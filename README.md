# Drive Sistemas de Informação
Drive de compartilhamento de estudos de Sistemas de Informação

# Ciclo Básico
Os estudos compartilhados do Ciclo Básico podem ser encontrados [aqui](https://gitlab.com/driveeach/drivecb)

# Grade Curricular

![](Grade.png)

## Contribuindo
Envie um Pull Request com a sua contribuição, ou verifique [esta página](https://gitlab.com/driveeach/driveeach/wikis/Contribuindo) para contribuir com facilidade!

### Outros Drives
Você pode encontrar uma coletânea com todos os Drives da EACH [aqui](https://gitlab.com/driveeach/driveeach)

## Listas no GitHub

* [Awesome awesomeness](https://github.com/bayandin/awesome-awesomeness) (Uma lista de repositórios sobre qualquer coisa)
* [Remote jobs Brazil](https://github.com/lerrua/remote-jobs-brazil) (Empresas que contratam remotamente)
* [Awesome Linux](https://github.com/aleksandar-todorovic/awesome-linux) (Listas de coisas relacionadas ao Linux)
* [Every programmer should know](https://github.com/mr-mig/every-programmer-should-know) (Uma coleção de coisas técnicas que todo desenvolvedor deve saber)
* [cosmos](https://github.com/OpenGenus/cosmos) (Coleção de todos algoritmos e estrutura de dados)

## Sub-reddits

* [Algorithms](https://www.reddit.com/r/algorithms/)
* [Computer science](https://www.reddit.com/r/compsci/)
* [Computer science videos](https://www.reddit.com/r/compscivideos/)
* [Hacking](https://www.reddit.com/r/hacking/)
* [Learn programming](https://www.reddit.com/r/learnprogramming/)
* [Programming](https://www.reddit.com/r/programming/)
* [RICK AND MORTY](https://www.reddit.com/r/rickandmorty/) (POR QUE NAO??)
* [Science](https://www.reddit.com/r/science/)
* [Ubuntu](https://www.reddit.com/r/Ubuntu/)
* [Unix porn](https://www.reddit.com/r/unixporn/)