#include <stdlib.h> 
#include <stdio.h>

#define TAM 500

typedef struct {
  int chave;//guarda a chave
  int valor; 
} INFO;

typedef struct NO {
  char simbolo; //simbolo a ser codificado  
  int freq; //frequencia   
  struct NO *fesq; //ponteiro para o filho da esquerda
  struct NO *fdir;//ponteiro para o filho da direita
} tNO;

typedef struct {
  tNO *raiz;//ponteiro para raiz
  char codigo [TAM][TAM];
} ARVORE;

//***********DefiniÃ§Ã£o das funÃ§oes********************************
void imprimi_msg(char *msg);
tNO *cria_no(char simbolo);
void salva_arvore(tNO *p,FILE *fp);
void carrega_arvore(tNO **p,FILE *fp);
void limpar_arvore(ARVORE *arv);
void limpar_arvore_aux(tNO *raiz);
void salva_cod(FILE *fp,char *cod);
int tam_cod(char *cod);
char *ler_cod(FILE *fp);
void inserir_arvore_abb(ARVORE *arv, int simbolo);
tNO *inserir_arvore_aux(tNO *p, int simbolo);
void inicializar_arvore(ARVORE *arv);
void in_ordem(tNO *no);
void grava(ARVORE *arv, char *name, char *msg);
void ler(ARVORE *arv, char *name, char **msg);

//***********Corpo das FunÃ§Ãµes*************************************
//imprime o arquivo codificado
void imprimi_msg(char *msg){
  int i;
  for (i=0;msg[i]!='\0';i++){
	printf("%c",msg[i]);
  }
}

//dado um simbolo, cria um no
tNO *cria_no(char simbolo){
  tNO *paux=(tNO *)malloc(sizeof(tNO));
  paux->fdir=NULL;
  paux->fesq=NULL;
  paux->simbolo = simbolo;
  return paux;
}

//salva uma arvore em disco
//? - usado para marcar um no sem filho
void salva_arvore(tNO *p,FILE *fp){
  if (p==NULL){
	char c='?';
	fwrite(&c,sizeof(char),1,fp);
  }else{
	//printf("%c",c);
	fwrite(&p->simbolo,sizeof(char),1,fp);
	salva_arvore(p->fesq,fp);
	salva_arvore(p->fdir,fp);
  }
}

//esta funÃ§Ã£o realiza a leiura de uma arvore em disco
void carrega_arvore(tNO **p,FILE *fp){
  char c;
  fread(&c,sizeof(char),1,fp); 

  if (feof(fp)) return;

  if (c=='@') return;

  if (c!='?'){
	//printf("%c",c);
	tNO *paux = cria_no(c);
	*p=paux;
	carrega_arvore(&((*p)->fesq),fp);
	carrega_arvore(&((*p)->fdir),fp);
  }
}

//limpa uma arvore
void limpar_arvore(ARVORE *arv) {
  limpar_arvore_aux(arv->raiz);
  arv->raiz = NULL;
}

void limpar_arvore_aux(tNO *raiz) {
  if (raiz != NULL) {
	limpar_arvore_aux(raiz->fesq);
	limpar_arvore_aux(raiz->fdir);
	free(raiz); 
	}
}

//Insere um dado numa arvore
void inserir_arvore_abb(ARVORE *arv, int simbolo) {
  arv->raiz = inserir_arvore_aux(arv->raiz, simbolo);
}

tNO *inserir_arvore_aux(tNO *p, int simbolo) {
  if (p == NULL) {//No folha
	p = (tNO *) malloc(sizeof(tNO));
	if (p==NULL) return NULL; //memoria insuficiente
	else{
	  printf("\nInserind chave %d",simbolo);   
	  p->fesq = p->fdir =  NULL;//inicializa
	  p->simbolo = simbolo;//guarda a informaÃ§Ã£o
	  return p;
	} //fecha else
  }else if(p->simbolo == simbolo) //No jÃ¡ existe 
	return NULL;
  else if (p->simbolo>simbolo) {
	p->fesq = inserir_arvore_aux(p->fesq,simbolo);
	return p;
  }
  else  {//descendo a direita
	p->fdir = inserir_arvore_aux(p->fdir,simbolo);  
	return p;
  }
} 

//salva o codigo
void salva_cod(FILE *fp,char *cod) {//salva o codigo no arquivo
	int i,tam, cont=0, cod_temp=-1;
	unsigned char a =(int)0; //8 bits todos com valores contendo zeros
	unsigned char b = (int) 1; //8 bits somente o ultimo com bit 1 
	unsigned char *cod_grav; //codigo convertido para caracter
	tam=tam_cod(cod);//calcula o tamanho do codigo de bits
	cod_grav=malloc((tam+1)*sizeof(char));//cria espaÃ§o para armazenar tam caracteres
	for (i=0;cod[i] != '\0';i++){
		cont++; //incrementa o contador
		if (cod[i]=='0') {//bit 0
			a = a<<1; //realiza um deslocamento a esquerda
		}
		else if (cod[i] == '1'){//bit 1
			a = a<<1; //realiza um deslocamento a esquerda
			a = a|b; //adiciona um bit a primeira posicao 
		}
		else
			printf("%s","CodificaÃ§Ã£o incorreta");
		if (cont==8) {/*completou 8 bits*/
			cod_temp++;
			cod_grav[cod_temp]=a; //armazena o 8 bits
			cont=0;//reseta contador
			a = (int) 0; //zera todos os bits
		}
	}
	/*O numero de bits pode nÃ£o ser multiplo de 8, logo temos que 
	verificar se o contador Ã© diferente de zero*/
	if (cont!=0){
		for (i=cont;i<8;i++){
			a = a<<1; //prenche com zeros os bits que nÃ£o sÃ£o necessÃ¡rios
		}
		cod_temp++;
		cod_grav[cod_temp]=a; //armazena o valor de a
		cod_grav[cod_temp+1]='\0'; //marca a posicao final
	}    
	fwrite(&tam,sizeof(int),1,fp); //escreve o numero de bits a serem gravados
	for (i=0;cod_grav[i] != '\0';i++){
		fwrite(&cod_grav[i],sizeof(char),1,fp);//grava os codigo 
	}
}

//calcula o tamanho do codigo
int tam_cod(char *cod){ 
	int i,tam=0;
	for (i=0;cod[i]!='\0';i++)  tam++;
	return tam;   
}

//ler o codigo do disco
char *ler_cod(FILE *fp) {
	int tam,i,val,cont=0;
	unsigned char a,b, *cod;
	fread(&tam,sizeof(int),1,fp);//le o numero de codigos
	cod=malloc((tam+1)*sizeof(char));//cria espaÃ§o para armazenar tam caracteres
	printf("\nForam encontrados %d codigos no arquivo\n", tam);
	fread(&a,sizeof(char),1,fp);//le o proximo caracter
	while (!feof(fp)) {
		b=(int)128; 
		for (i=0;i<8;i++){
			if (cont<tam){ //para quando atingiu o numero de bits
				val=(int)(a&b);
				if (val==0)	 cod[cont]='0';
				else cod[cont]='1';
				b=b>>1;	
				cont++;
			}
		}
		fread(&a,sizeof(char),1,fp);
	}
	cod[cont+1]='\0';
	return cod;
}

//inicializa uma arvore
void inicializar_arvore(ARVORE *arv){
	int i;
	for (i=0; i < TAM; i++) {
		arv->codigo[i][0] = '\0';
	}
	arv->raiz = NULL;
}

void in_ordem(tNO *no){     
	if (no!=NULL){  
		if(no->fesq!=NULL){
			in_ordem(no->fesq);
		}
		printf("[%d]\t",no->simbolo); 
		if(no->fdir!=NULL) {
			in_ordem(no->fdir); 
		}
	}else  
		printf("\n\tA arvore esta vazia!\n");   
}  

//grava os dados em disco
void grava(ARVORE *arv, char *name, char *msg){
	FILE *fp;    
	char marca ='@'; //marca o final da arvore
	if((fp = fopen(name,"wb")) ==NULL) {
		printf("\nNao consigo abrir o arquivo ! ");
		exit(1);
	}    
	else {
		printf("\nArquivo Criado com Sucesso");
	}
	printf("\nSalvando a arvore no arquivo");
	salva_arvore(arv->raiz,fp);

	printf("\nGravando uma marca de fim de arquivo\n");
	fwrite(&marca,sizeof(char),1,fp);

	printf("\nSalvando codigo no arquivo\n");
	salva_cod(fp,msg);

	printf("\nApagando a estrutura da arvore"); 
	limpar_arvore(arv); 

	printf("\nFechando Arquivo\n"); 
	fclose (fp);
}

//lendo os dados do disco
void ler(ARVORE *arv, char *name, char **msg){
	FILE *fp; 
	int cont=0;   
	char c;
	if((fp = fopen(name,"rb")) ==NULL) {
		printf("\nNao consigo abrir o arquivo ! ");
		exit(1);
	}    
	else {
		printf("\nArquivo aberto com Sucesso");
	}
	//procura @
	while(!feof(fp)){
		fread(&c,sizeof(char),1,fp); 
		if (c=='@') break;
	}
	//ate o final do arquivo
	while(!feof(fp)){
		fread(&c,sizeof(char),1,fp); 
		cont++;
	}
	fseek (fp,0,SEEK_SET ); //volta o ponteiro para inicio

	carrega_arvore(&arv->raiz,fp);//carrega a arvore

	while(!feof(fp)){
		fread(&c,sizeof(char),1,fp); 
		if (c=='@') break;
	}
	*msg=ler_cod(fp);
	fclose(fp);
}
