#include <stdlib.h> 
#include <stdio.h>
#include "ajuda3.h"


int main(){
	char *msg ="00001010101010"; //exemplo de arquivo codificado   
	char *msg_grav=NULL; //mensagem gravada
	ARVORE *arv = (ARVORE *)malloc(sizeof(ARVORE));
	inicializar_arvore(arv);
	inserir_arvore_abb(arv, 88);
	inserir_arvore_abb(arv, 89);
	inserir_arvore_abb(arv, 87);
	inserir_arvore_abb(arv, 83);
	printf("\nRealizando Percurso na Arvore\n");
	in_ordem(arv->raiz);

	//funÃ§Ã£o que grava em disco
	grava(arv,"arquivo.bin",msg);

	printf("\nImprimindo a arvore apÃ³s gravaÃ§Ã£o");
	in_ordem(arv->raiz); 

	//le arquivos do disco
	ler(arv, "arquivo.bin", &msg_grav);

	//imprimi mensagem
	printf("\nMensagem carregada\n");
	imprimi_msg(msg_grav);

	//imprime arvore
	printf("\nArvore Carregada\n");
	in_ordem(arv->raiz);

	return 1;
}
