#include <stdio.h>
#include <stdlib.h>
//calcula o tamanho do codigo
int tam_cod(char *cod){ 
	int i,tam=0;
	for (i=0;cod[i]!='\0';i++)  tam++;
	return tam;   
}
/*Le-se 8 bits, converte-se para caracter e armzena
Após a leitura de todos os bits, salva em arquivo*/
void salva_cod(FILE *fp,char *cod)//salva o codigo no arquivo
{
	int i,tam, cont=0, cod_temp=-1;
	unsigned char a =(int)0; //8 bits todos com valores contendo zeros
    unsigned char b = (int) 1; //8 bits somente o ultimo com bit 1 
    unsigned char *cod_grav; //codigo convertido para caracter
    tam=tam_cod(cod);//calcula o tamanho do codigo de bits
    cod_grav=malloc((tam+1)*sizeof(char));//cria espaço para armazenar tam caracteres
	for (i=0;cod[i] != '\0';i++){
    	cont++; //incrementa o contador
	    if (cod[i]=='0') {//bit 0
	       a = a<<1; //realiza um deslocamento a esquerda
		   }
	    else if (cod[i] == '1'){//bit 1
	       a = a<<1; //realiza um deslocamento a esquerda
	       a = a|b; //adiciona um bit a primeira posicao 
		   }
	   else
	     printf("%s","Codificação incorreta");
	    if (cont==8) {/*completou 8 bits*/
	       cod_temp++;
           cod_grav[cod_temp]=a; //armazena o 8 bits
	       cont=0;//reseta contador
	       a = (int) 0; //zera todos os bits
	    }
	 }
	 /*O numero de bits pode não ser multiplo de 8, logo temos que 
	 verificar se o contador é diferente de zero*/
	 if (cont!=0){
	    for (i=cont;i<8;i++){
	    a = a<<1; //prenche com zeros os bits que não são necessários
       }
       cod_temp++;
       cod_grav[cod_temp]=a; //armazena o valor de a
       cod_grav[cod_temp+1]='\0'; //marca a posicao final
    }    
    fwrite(&tam,sizeof(int),1,fp); //escreve o numero de bits a serem gravados
    for (i=0;cod_grav[i] != '\0';i++){
    	fwrite(&cod_grav[i],sizeof(char),1,fp);//grava os codigo 
	}
}

char *ler_cod(FILE *fp)
{
	int tam,i,val,cont=0;
	unsigned char a,b, *cod;
 	fread(&tam,sizeof(int),1,fp);//le o numero de codigos
    cod=malloc((tam+1)*sizeof(char));//cria espaço para armazenar tam caracteres
	printf("\nForam encontrados %d codigos no arquivo\n", tam);
    fread(&a,sizeof(char),1,fp);//le o proximo caracter
    while (!feof(fp))
     {
     	b=(int)128; 
	    for (i=0;i<8;i++){
	        	if (cont<tam){ //para quando atingiu o numero de bits
	        		val=(int)(a&b);
	    			if (val==0)	 cod[cont]='0';
	    			else cod[cont]='1';
	        		b=b>>1;	
	        		cont++;
	        	}
	    }
	        
        fread(&a,sizeof(char),1,fp);
	}
     cod[cont+1]='\0';

 	return cod;
}
void imprimir(char *cod)
{
	int i;
	printf("\nImprimindo os codigos\n");
	for (i=0;cod[i]!='\0';i++){
		printf("%c",cod[i]);
	}
	
}

int main()
{
	FILE *fp;
    unsigned char a =(int)0; //8 bits todos com valores contendo zeros
    unsigned char b = (int) 1; //8 bits somente o ultimo com bit 1 
    unsigned char *cod_grav;
    char *cod="010000010100100"; //codigo a ser escrito no arquivo
	int i, cont=0, tam;
    
	if((fp = fopen("arquivo.bin","wb")) ==NULL) //abre o arquivo para escrita em modo binario
    {
      printf("\nNao consigo abrir o arquivo ! ");
      exit(1);
    }
	salva_cod(fp,cod);//salva o codigo em arquivo
    fclose(fp); //fecha o arquivo   
    imprimir(cod);//imprime o codigo 

    if((fp = fopen("arquivo.bin","rb")) ==NULL)
    {
      printf("\nNao consigo abrir o arquivo ! ");
      exit(1);
    }    
	cod_grav=ler_cod(fp);
    fclose(fp);//fecha o arquivo
	imprimir(cod_grav);//imprime o codigo do arquivo
    
    return 1;
}