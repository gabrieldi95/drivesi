#include "aed1b.h"
#include <cstdlib>
#include <iostream>

using namespace std;

void imprimir(NO* p)
{
     if (p)
     {
        imprimir(p->esq);
        printf("-%d", p->chave);
        imprimir(p->dir);
     }
}

void inicializarArvore(NO* *raiz)
{
     *raiz = NULL;
}

int maior(int i, int j)
{
    if (i>j) return i;
    else return j;
}
int altura(NO *p)
{
    int r =0;
    if (p)
    {
       if(!p->esq && !p->dir) return(r);
       int auxEsq = altura(p->esq);
       int auxDir = altura(p->dir);
       r = maior(auxEsq, auxDir) + 1;    
    }
    return(r);           
}
void indentRec(int h, NO *p)
{
     int i;
     if (p)
     {
        indentRec(h+1, p->dir);
        for(i = 1; i<= h; i++) printf("   ");
        printf("%d\n", p->chave);
        indentRec(h+1, p->esq);
     }
}
void indent(NO *p)
{
     int h = altura(p);
     indentRec(h, p);
}

int main()
{
    int valor;
    NO* raiz = NULL;
    
    printf("Insira os valores...\n");
    
    while (valor != 0)
    {
          fflush(stdin);
          scanf("%d", &valor);
          if (valor < 1 || valor > MAX) break;
          manutencao(&raiz, valor);
    }
    
    indent(raiz);
    printf("Exibindo pre-ordem nao recursivo...\n");
    preOrdem(raiz);
    
    printf("\nExibindo em-ordem nao recursivo...\n");
    emOrdem(raiz);
    
    printf("\nExibindo em nivel (com fila)...\n");
    emNivel(raiz);
    
    printf("\nEspelhando...\n");
    espelhar(raiz);
    indent(raiz);
    
    if (valor == 300)
    {
       fflush(stdin);
       printf("Sucessor: insira valor de 'n'\n");
       scanf("%d", &valor);
       NO * s = NULL;
       s = sucessor(raiz, valor);
       if (s) printf("Resposta: sucessor de %d eh %d!\n", valor, s->chave);
       else printf("Sucessor nao encontrado.\n");
    }
    
    if (valor == 666)
    {
       valor = folhaMaisProxima(raiz);
       printf("Resposta: %d\n", valor);
    }
    
    if (valor == 777)
    {
       printf("Impressao em-ordem:\n");
       imprimir(raiz);
    }

    printf("Saiu...\n");


    //codigo do professor
    system("PAUSE");
    return EXIT_SUCCESS;
}

