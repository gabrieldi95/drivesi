# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <conio.h>
# include <malloc.h>

# define MAX 100

using namespace std;

typedef struct estrutura {
     	int chave;
     	estrutura *esq;
        estrutura *dir;
        estrutura *prox;
} NO;

typedef struct
{
        NO* top;
} PILHA;

typedef struct
{
        NO* inicio;
        NO* fim;
} FILA;

void manutencao(NO* *raiz, int ch);
NO *sucessor(NO *raiz, int n);
int folhaMaisProxima(NO *raiz);
void preOrdem(NO *p);
void emOrdem(NO *p);
void emNivel(NO *p);
NO* espelhar(NO *p);

char *aluno();
char *nrousp();
char *turma();
