#include stdin.h

typedef struct estrutura{
        int chave;
        estrutura *esq;
        estrutura *dir;
} NO;

//lado=1: esquerdo; lado=2: direito
//raiz passado como referencia, para poder mudar caso necessario (A.B. vazia)
bool inserir(NO* *raiz, int ch, NO* pai, int lado)
{
     if (pai && lado == 1 && pai->esq) return (false); //lado esquerdo ocupado
     if (pai && lado == 2 && pai->dir) return (false); //lado direito ocupado
     
     NO* novo = (NO*)malloc(sizeof(NO));
     novo->chave = ch;
     novo->esq = NULL;
     novo->dir = NULL;
     
     if (!pai)
     {
        if (*raiz) return (false); //raiz ocupada
        else *raiz = novo; //mudando o valor da raiz
     }
     else
     {
         if (lado == 1) pai->esq = novo;
         else pai->dir = novo;
     }
     return (true);
}

void espaco(int tam)
{
     while (tam)
     {
           printf(" ");
           tam--;
     }
}

void exibir(NO p, int *altura)
{
        printf("%d", p.chave);
        
        if (p->esq) printf("/");
        else printf(" ");
     
        if (p->dir) printf("\");
        else printf(" ");
     
     NO * ant = p;
     if (p->esq)
     {
        exibir(p->esq, altura++);
     }
     if (p->dir)
     {
        exibir(p->dir, altura++);
     }
}
 

int main()
{
    //inicialização
    NO *raiz;
    raiz = NULL;
    
    return(0);
}
          10
      /        \
     /          \
    10          10
   /  \       /    \
  10   10     10   10
 / \   / \   / \   / \
10 10 10 10 10 10 10 10
