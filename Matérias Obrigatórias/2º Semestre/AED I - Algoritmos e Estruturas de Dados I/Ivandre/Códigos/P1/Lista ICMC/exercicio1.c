#include <stdio.h>
#include <stdlib.h>

#define MAX 55

typedef struct {
	int chave;
}REGISTRO;

typedef struct {
	REGISTRO A[MAX];
	int nroElem;
}LISTA;

void preencheVazio(LISTA *l){
	int x = l->nroElem;
	if(l->nroElem == 0){
		printf("Vazia");
		return;
	}
	if(l->nroElem == MAX) printf("Cheia! \n");
	else{
		int j;
		while (l->nroElem < MAX){
			for( j = 0; j < x; j++){
				if(l->nroElem < MAX){
					l->A[l->nroElem].chave = l->A[j].chave;
					l->nroElem ++;
				}
				else{
					int y = MAX - l->nroElem;
					while (j < y){
						l->A[l->nroElem].chave = l->A[j].chave;
						l->nroElem ++;
					}
					break;
				}	
			}
		}
		printf("\nPreencheu\n");
	}
}

int main(){
	
	REGISTRO  a;
	REGISTRO  b;
	REGISTRO  c;
	REGISTRO  d;
	REGISTRO  e;
	REGISTRO  f;
	REGISTRO  g;
	REGISTRO  h;
	REGISTRO  i;
	REGISTRO  j;
	
	a.chave = 00;
	b.chave = 10;
	c.chave = 20;
	d.chave = 30;
	e.chave = 40;
	f.chave = 50;
	g.chave = 60;
	h.chave = 70;
	i.chave = 80;
	j.chave = 90;
	
	LISTA* lista = (LISTA*) malloc(sizeof(LISTA));
	
	lista->A[0] = a;
	lista->A[1] = b;
	lista->A[2] = c;
	lista->A[3] = d;
	lista->A[4] = e;
	lista->A[5] = f;
	lista->A[6] = g;
	lista->A[7] = h;
	lista->A[8] = i;
	lista->A[9] = j;
	
	lista->nroElem = 10;
	
	preencheVazio(lista);
	
	int k=0, s = 10;
	while(k < MAX){
		printf("%d  ", lista->A[k].chave);
		k++;
		if(k  == s){
			printf("\n");
			s +=10;
		}
	}
	printf("-> %d <- \n", lista->nroElem);
	system("Pause");
	return 0;
	
}
