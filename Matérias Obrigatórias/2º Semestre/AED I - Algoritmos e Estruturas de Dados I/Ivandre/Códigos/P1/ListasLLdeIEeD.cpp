/*
<> LISTAS LINEARES LIGADAS (ENCADEADAS) <>

*/
#include <stdio.h>
#include <stdlib.h>
#include<malloc.h>

#define MAX 50

typedef int TIPOCHAVE;

// Estatica

typedef struct {
    TIPOCHAVE chave;
    int prox;
} REGISTRO;
                             
typedef struct {
    REGISTRO A[MAX];
    int inicio;
    int dispo;
} LISTA;

// Dinamica

typedef struct estrutura {
	TIPOCHAVE chave;
	int info;
	estrutura *prox;
} NO;

typedef struct {
	NO* inicio;
} LISTAD;

// Sentinela

typedef struct {
	NO* inicio;
	NO* sentinela;
} LISTAT;

/* <> Fun��es <> */

//1. De IMPLEMENTA��O ESTATICA

// Inicializa��o
void inicializarListaLigadaEstatica(LISTA *l) {
    l->inicio = -1;
    l->dispo = 0;
    for(int i = 0; i < MAX - 1; i++) 
         l->A[i].prox = i + 1;
    l->A[MAX - 1].prox = -1;
}

// Exibi��o da lista
void exibirLista(LISTA l) {
    int i = l.inicio;
    while (i > -1) {
        printf("%d \n", l.A[i].chave); // TIPOCHAVE deve ser int
        i = l.A[i].prox;
    }
}

// Exibi��o da lista [Ponteiro]
void exibirListo(LISTA *l) {
    int i = l->inicio;
    while (i > -1) {
        printf("%d \n", l->A[i].chave); // TIPOCHAVE deve ser int
        i = l->A[i].prox;
    }
}

// Busca sequencial
int buscaSeqOrd(TIPOCHAVE ch, LISTA l, int *ant) {
    int i = l.inicio;
    *ant= -1;
    while (i != -1) {
        if(l.A[i].chave >= ch) break;
        *ant = i;
        i= l.A[i].prox;
    }
    if(i==-1) return -1;
    if(l.A[i].chave==ch) return(i);
    else return -1;
}

// Obter n� dispon�vel - a lista � alterada
int obterNo(LISTA *l) {
    int result = l->dispo;
    if(l->dispo > -1) 
         l->dispo = l->A[l->dispo].prox;
    return(result);
}

// Devolver n� p/ dispo � a lista � alterada
void devolverNo(LISTA *l, int j) {
    l->A[j].prox= l->dispo;
    l->dispo = j;
}

// Exclus�o do elemento de chave indicada
bool excluirElemListaEnc(TIPOCHAVE ch, LISTA *l) {
    int ant, i;
    i = buscaSeqOrd(ch, *l, &ant);
    if(i < 0) return(false);
    printf("------------------------------------- %d ----", ant);
    if(ant == -1) l->inicio = l->A[i].prox;
    else l->A[ant].prox = l->A[i].prox;
    devolverNo(l, i);
    return(true);
}

// inser��o em lista ordenada sem duplica��es
bool inserirElemListaEncOrd(TIPOCHAVE ch, LISTA *l) {
    int ant, i;
    i = buscaSeqOrd(ch, *l, &ant);
    if((l->dispo < 0) || (i != -1)) return(false);
    i = obterNo(l);
    l->A[i].chave = ch;
    if(l->inicio<0) {
        // inser��o do primeiro elemento de lista vazia
        l->inicio = i;
        l->A[i].prox = -1;
    }
    else {
        if(ant < 0) {
            // inser��o no in�cio de lista j� existente
            l->A[i].prox = l->inicio;
            l->inicio = i;
        }
        else {
            // inser��o entre dois elementos
            l->A[i].prox = l->A[ant].prox;
            l->A[ant].prox = i;
        }
    }
    return(true);
}

//2. De IMPLEMENTA��O DINAMICA

// Inicializa��o
void inicializarListaD(LISTAD *l) {
	l->inicio = NULL;
}

// Exibi��o da lista completa
void exibirListaD(LISTAD *l) {
	NO* p = l->inicio;
	while (p) {
		printf("%d \n",p->chave);
		p = p->prox;
	}
}

// Retornar o primeiro elemento da lista
NO* primeiroElemListaD(LISTAD *l) {
	return(l->inicio);
}

// Retornar o �ltimo elemento da lista
NO* ultimoElemListaD(LISTAD *l) {
	NO* p = l->inicio;
	if(p)
		while(p->prox) p = p->prox;
	return(p);
}

// Retornar o en�simo elemento da lista
NO* enesimoElemListaD(LISTAD *l, int n) {
	NO* p = l->inicio;
	int i = 1;
	if(p)
		while((p->prox)&&(i<n)){
			p = p->prox;
			i++;
		}
	if(i != n) return(NULL);
	else return(p);
}

// Quantos elementos existem na lista
int tamanhoListaD(LISTAD *l) {
	NO* p = l->inicio;
	int tam = 0;
	while (p) {
		tam++;
		p = p->prox;
	}
	return(tam);
}

// Busca pela chave ch na lista (ordem crescente) retornando p e ant
NO* buscaSeqOrdD(TIPOCHAVE ch, LISTAD *l, NO* *ant) {
	NO* p = l->inicio;
	*ant = NULL;
	while(p) {
		if(p->chave >= ch) break;
		*ant = p;
		p = p->prox;
	}
	if(p)
		if(p->chave == ch) return(p);
	return(NULL);
}

// Inser��o da chave ch na lista ordenada sem duplica��es
bool inserirElemListaOrdD(TIPOCHAVE ch , LISTAD *l) {
	NO* novo;
	NO* ant;
	novo = buscaSeqOrdD(ch, l, &ant);
	if(novo) return(false);
	novo = (NO*) malloc(sizeof(NO));
	novo->chave = ch;
	if(!l->inicio) {
		 // 1a. inser��o em lista vazia
		l->inicio = novo;
		novo->prox = NULL;
	}
	else {
		if(!ant) { 
			// inser��o no in�cio da lista
			novo->prox = l->inicio;
			l->inicio = novo;
		}
		else { 
			// inser��o ap�s um n� existente
			novo->prox = ant->prox;
			ant->prox = novo;
		}
	}
	return(true);
}

// Anexar novo elemento ao final da lista, duplicado ou n�o
void anexarElemLista(TIPOCHAVE ch, LISTAD *l) {
	NO* novo;
	NO* ant;
	ant = ultimoElemListaD(l);
	novo = (NO *) malloc(sizeof(NO));
	novo->chave = ch;
	novo->prox = NULL;
	if(!ant) l->inicio = novo;
	else ant->prox = novo;
}

// Exclus�o da chave dada
bool excluirElemListaD(TIPOCHAVE ch, LISTAD *l) {
	NO* ant;
	NO* elem;
	elem = buscaSeqOrdD(ch, l, &ant);
	if(!elem) return(false);		 	// nada a excluir
	if(!ant) l->inicio = elem->prox; 	// exclui 1o. elemento da lista
	else ant->prox = elem->prox; 		// exclui elemento que possui ant
	free(elem); 						// exclus�o �f�sica�
	return(true);
}

// Destrui��o da lista
void destruirListaD(LISTAD *l) {
	NO* atual;
	NO* prox;
	atual = l->inicio;
	while (atual) {
		prox = atual->prox; 	// guarda pr�xima posi��o
		free(atual); 			// libera mem�ria apontada por atual
		atual = prox;
	}
	l->inicio = NULL; 			// ajusta in�cio da lista (vazia)
}

//2.1 De IMPLEMENTA��O DINAMICA com SENTINELA

// Inicializa��o (lista com sentinela)
void inicializarListaT(LISTAT *l) {
	l->sentinela = (NO*) malloc(sizeof(NO));
	l->inicio = l->sentinela;
}

// Exibi��o (lista com sentinela)
void exibirListaT(LISTAT *l) {
	NO* p = l->inicio;
	while (p != l->sentinela) {
		printf("%d \n",p->chave); // chave deve ser int
		p = p->prox;
	}
}

// Primeiro elemento da lista com sentinela
NO* primeiroElemListaT(LISTAT *l) {
	if(l->inicio == l->sentinela)
		return(NULL);
	else
		return(l->inicio);
}

// �ltimo elemento da lista com sentinela
NO* ultimoElemListaT(LISTAT *l) {
	NO* p = l->inicio;
	if(p == l->sentinela) return(NULL);
	while(p->prox != l->sentinela)
		p = p->prox;
	return(p);
}

// N-�simo elemento da lista com sentinela
NO* enesimoElemListaT(LISTAT *l, int n) {
	NO* p = l->inicio;
	int i = 1;
	if(p == l->sentinela) return(NULL);
	while((p->prox != l->sentinela) && (i < n)) {
		p = p->prox;
		i++;
	}
	if(i != n) return(NULL);
	else return(p);
}

// Quantos elementos existem na lista com sentinela
int tamanhoListaT(LISTAT *l) {
	NO* p = l->inicio;
	int tam = 0;
	while (p != l->sentinela) {
		tam++;
		p = p->prox;
	}
	return(tam);
}

// Busca da chave em lista ordenada e com sentinela
NO* buscaSeqOrdT(TIPOCHAVE ch, LISTAT *l, NO* *ant) {
	NO* p = l->inicio;
	*ant = NULL;
	l->sentinela->chave = ch;
	while(p->chave < ch) {
		*ant = p;
		p = p->prox;
	}
	if((p != l->sentinela) && (p->chave == ch)) return(p);
	else return(NULL);
}

// Inser��o em lista ordenada sem repeti��o com sentinela
bool inserirElemListaOrdT(TIPOCHAVE ch , LISTAT *l) {
	NO* novo;
	NO* ant;
	novo = buscaSeqOrdT(ch, l, &ant);
	if(novo) return(false);
	novo = (NO*) malloc(sizeof(NO));
	novo->chave = ch;
	if(l->inicio == l->sentinela) {
		l->inicio = novo;
		novo->prox = l->sentinela;
	}
	else {
		if(ant==NULL) {
			novo->prox = l->inicio;
			l->inicio = novo;
		}
		else {
			novo->prox = ant->prox;
			ant->prox = novo;
		}
	}
	return(true);
}

// Anexar um novo elemento � lista com sentinela
void anexarElemListaT(TIPOCHAVE ch, LISTAT *l) {
	NO* novo;
	NO* ant;
	ant = ultimoElemListaT(l);
	novo = (NO *) malloc(sizeof(NO));
	novo->chave = ch;
	novo->prox = l->sentinela;
	if(ant == NULL) l->inicio = novo;
	else ant->prox = novo;
}

// Destrui��o da lista com sentinela
void destruirListaT(LISTAT *l) {
	NO* atual;
	NO* prox;
	atual = l->inicio;
	while (atual != l->sentinela ) {
		prox = atual->prox;
		free(atual);
		atual = prox;
	}
	l->inicio = l->sentinela;
}

int main(){  
     
     //1. De IMPLEMENTA��O ESTATICA
     
     REGISTRO x;
     x.chave = 200;
     x.prox = 1;
     REGISTRO y;
     y.chave = 300;
     y.prox = 2;
     REGISTRO z;
     z.chave = 500;
     z.prox = -1;
     
	 LISTA* listo= (LISTA*) malloc(sizeof(LISTA));
     listo->inicio = 0;
     listo->dispo = 2; // Qual o papel do 'listo->dispo' ?
     listo->A[0] = x;
     listo->A[1] = y;
     listo->A[2] = z;

     LISTA list;
     list.inicio = 0;
     list.dispo = 3;
     list.A[0] = x;
     list.A[1] = y;
     list.A[2] = z;
     
     printf(" -> 1. De IMPLEMENTA��O ESTATICA\n");
     //inicializarListaLigadaEstatica(listo); // -> ????? <- || Tem q usar em conjunto com o inserir 
     exibirListo(listo);
     //printf("--->> %i <<--- Busca Seq. Ordenada \n", buscaSeqOrd( 300, list, ?)); //-> Terceiro argumento tipo desconhecido <-
     printf("--->> %i <<--- N� disponivel \n", obterNo(listo));// -> ????? <-
     //devolverNo(listo, -1); ??????
     printf("%s \n", inserirElemListaEncOrd(400, listo)? "--->> Inseriu <<---" : "--->> Nao inseriu <<---");
     exibirListo(listo);
     printf("%s \n", excluirElemListaEnc(300, listo)? "--->> Excluiu <<---" : "--->> Nao excluiu <<---");
     //printf("%s \n", inserirElemListaEncOrd(300, listo)? "--->> Inseriu <<---" : "--->> Nao inseriu <<---");
     exibirListo(listo);
     
     //2. De IMPLEMENTA��O DINAMICA
     
     NO* a = (NO*) malloc(sizeof(NO));
     a->chave = 200;
     NO* b = (NO*) malloc(sizeof(NO));
     b->chave = 300;
     NO* c = (NO*) malloc(sizeof(NO));
     c->chave = 500;
     a->prox = b;
     b->prox = c;
     c->prox = NULL;
     
     LISTAD* lista = (LISTAD*) malloc(sizeof(LISTAD));
     lista->inicio = a;
     
     printf(" -> 2. De IMPLEMENTA��O DINAMICA\n");
     //inicializarListaD(lista); 
     exibirListaD(lista);
	 printf("--->> %i <<--- Primeiro elemento \n", primeiroElemListaD(lista)); // Tipo de retorno?? Retorna um ponteiro...
	 printf("--->> %i <<--- Ultimo elemento \n", ultimoElemListaD(lista)); 	// printf serve s� pra saber q esta retornando alguma coisa, nao sei se a coisa certa
	 printf("--->> %i <<--- Enesimo elemento \n", enesimoElemListaD(lista, 2));
	 printf("--->> %i <<--- Tamanho \n", tamanhoListaD(lista));
	 printf("%s \n", inserirElemListaOrdD(400, lista)? "--->> Inseriu <<---" : "--->> Nao inseriu <<---"); 
	 exibirListaD(lista);
	 printf("--->> Anexou ultimo elemento '550' <<---\n");
	 anexarElemLista(550, lista);
	 exibirListaD(lista);
	 printf("%s \n", excluirElemListaD(300, lista)? "--->> Excluiu <<---" :"--->> Nao excluiu <<---");
	 exibirListaD(lista);
	 //destruirListaD(lista);
	 
	 //2.1 Com SENTINELA
	 
	 NO* d = (NO*) malloc(sizeof(NO));
     d->chave = 200;
     NO* e = (NO*) malloc(sizeof(NO));
     e->chave = 300;
     NO* f = (NO*) malloc(sizeof(NO));
     f->chave = 500;
	 
	 LISTAT* listat = (LISTAT*) malloc(sizeof(LISTAT));
	 listat->sentinela = (NO*) malloc(sizeof(NO));
	 listat->inicio = d;
	 
	 d->prox = e;
     e->prox = f;
     f->prox = listat->sentinela;
	 
	 printf(" -> 2.1 Com SENTINELA \n");
	 //inicializarListaT(listat);
	 exibirListaT(listat);
	     
     system("Pause");
     return 0;
}
