/* <> LISTAS LINEARES SEQUENCIAS <> */
#include <stdio.h>
#include <stdlib.h>

#define MAX 50

typedef int TIPOCHAVE;
 
 typedef struct {
         TIPOCHAVE chave;         
 } REGISTRO;
 
 typedef  struct{
          REGISTRO A[MAX];
          int nElem;
 } LISTA;
 
// Inicializa��o da lista sequencial
void inicializarListaSequencial(LISTA *l) {
     l->nElem = 0;
}
 
// Exibi��o da lista sequencial
   void exibirLista(LISTA l)
     {
          int i;
          for (i=0; i < l.nElem; i++)
              printf("%d \n", l.A[i].chave); // TIPOCHAVE deve ser = int
     }

// Retornar o tamanho da lista sequencial
     int tamanho(LISTA l) { 
         return(l.nElem);
     }
     
 // Retornar a chave do primeiro elemento da lista sequencial
    TIPOCHAVE primeiroElem(LISTA l) {
              if(l.nElem > 0) return(l.A[0].chave);
              else return(-1); // lista vazia
    }
 // Retornar a chave do ultimo elemento da lista sequencial
    TIPOCHAVE ultimoElem(LISTA l) {
              if(l.nElem > 0) return(l.A[l.nElem-1].chave);
              else return(-1); // lista vazia
              }
  // Retornar a chave do n-�simo elemento da lista sequencial 
    TIPOCHAVE enesimoElem(LISTA l, int n) {
              if(n <= l.nElem) return(l.A[n-1].chave);
              else return(-1);
    }
 // Destrui��o da lista sequencial 
   void destruirLista(LISTA *l) { //passar pra ponteiro
        l->nElem = 0;
   }
     
// Inser��o "direta" na posi��o i
   bool inserirElemListaSeq(TIPOCHAVE ch, int i, LISTA *l) {
        int j;
        if((l->nElem >= MAX) || (i < 0) || (i > (l->nElem)))
             return(false); // lista cheia ou �ndice inv�lido
        if((l->nElem > 0) && (i < l->nElem))
             for (j = l->nElem; j >= (i+1); j--)
                 l->A[j] = l->A[j-1];
        l->A[i].chave=ch;
        l->nElem++;
        return(true);
   }
   
// Exibi��o da lista sequencial [ponteiro]
   void exibirListo(LISTA *l)
     {
          int i;
          for (i=0; i < l->nElem; i++)
              printf("%d \n", l->A[i].chave); // TIPOCHAVE deve ser = int
     }
     
// Busca sequencial em lista ordenada ou n�o 
   int buscaSeq(TIPOCHAVE ch, LISTA l) {
       int i = 0;
       while (i < l.nElem) {
             if(ch == l.A[i].chave) return(i); // achou
             else i++;
       }
       return(-1); // n�o achou
   }

// Busca sequencial em lista COM SENTINELA (vetor criado com MAX+1 posi��es)
   int buscaSent(TIPOCHAVE ch, LISTA l) {
       int i = 0;
       l.A[l.nElem].chave = ch; // sentinela
       while(l.A[i].chave < ch) i++;
       if((i > (l.nElem -1)) || (l.A[i].chave != ch)) return (-1); // n�o achou
       else return (i);
   } 
   
// Busca binaria em lista ordenada
    int buscaBin(TIPOCHAVE ch, LISTA l) {
        int inf, sup, meio;
        inf = 0;
        sup = l.nElem - 1;
        while(inf <= sup) {
                  meio = ((inf + sup) / 2);
                  if(l.A[meio].chave == ch) return(meio); // achou
                  else {
                       if(l.A[meio].chave < ch) inf = meio + 1;
                       else sup = meio - 1;
                  }
        }
        return(-1);
    }
    
// Inser��o em lista ordenada COM SENTINELA sem duplica��o
    bool inserirElemListaOrd(TIPOCHAVE ch, LISTA *l) {
        int i = 0;
        if(l->nElem >= MAX) return(false); // lista cheia
        l->A[l->nElem].chave = ch; // sentinela
        while(l->A[i].chave < ch) i++;
        if((l->A[i].chave == ch) && (i < l->nElem))
                return(false); //Elemento de mesma chave
        else return(inserirElemListaSeq(ch, i, l));
    }
    
// Exclus�o
    bool excluirElemLista(TIPOCHAVE ch, LISTA *l) {
        int pos, j;
        pos = buscaSeq(ch, *l);
        if(pos == -1) return(false); // n�o existe
        for(j = pos; j < l->nElem - 1; j++)
              l->A[j] = l->A[j+1];
        l->nElem--;
        return(true);
    }
    
int main(){   
    
     //Criando chaves primarias e atribuindo aos registros
     REGISTRO x;
     x.chave = 500;
     REGISTRO y;
     y.chave = 200;
     REGISTRO z;
     z.chave = 300;
     
     //Criando uma lista e colocando elementos na lista
     LISTA list; //por valor
     list.nElem = 3;
     list.A[0] = y; 
     list.A[1] = z;
     list.A[2] = x;
     
     LISTA* listo= (LISTA*) malloc(sizeof(LISTA)); 
     listo->nElem = 3; //por referencia/ponteiro
     listo->A[0] = y;
     listo->A[1] = z;
     listo->A[2] = x;
     
     printf("++ Listas SEM ponteiro! ++\n");
     exibirLista(list);
     printf("--->> %i <<--- Tamanho \n", tamanho(list));
     printf("--->> %i <<--- Primeiro elemento \n", primeiroElem(list));
     printf("--->> %i <<--- Ultimo Elemento\n", ultimoElem(list));     
     printf("--->> %i <<--- Elemento na posi�ao\n", enesimoElem(list, 2));
     printf("--->> %i <<--- Busca em lista\n", buscaSeq(300, list));
     printf("--->> %i <<--- Busca com Sentinela\n", buscaSent(300, list));
     printf("--->> %i <<--- Busca Binaria\n", buscaBin(500, list));
     
     printf("\n++ Listas COM ponteiro! ++\n");
     printf("Inseriu elemento de chave 400 no indice 2 <<--- Inser��o direta\n");
     inserirElemListaSeq(400,2, listo);
     //destruirLista(listo); 
     exibirListo(listo);
     printf("%s", inserirElemListaOrd(450, listo)? "--->> Inseriu [Sentinela] <<---\n" : "--->> Duplicado ou Max+1 \n<<---");
     exibirListo(listo);
     printf("%s", excluirElemLista(300, listo)? "--->> Excluiu <<---\n" : "--->> Nao existe na lista! <<---\n");
     exibirListo(listo);
     
     system("Pause");   
     return 0;  
}
