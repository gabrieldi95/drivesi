#include <stdlib.h>
#include <stdio.h>

#define MAX 50
#define MAXI 15 // tamanho do vetor A
#define NP 5 // nro. de pilhas compartilhando o vetor (numeradas de 0..NP-1)

typedef struct {
	int topo1;
	int topo2;
	int A[MAX];
} PILHADUPLA;

// Inicializacao da pilha dupla
void inicializarPilhaDupla(PILHADUPLA *p) {
	p->topo1 = -1;
	p->topo2 = MAX;
}

// Quantos elementos existem na pilha k (1 ou 2)
int tamanhoPilhaDupla(PILHADUPLA *p, int k) {
	if( k == 1 ) return(p->topo1 + 1);
	else return(MAX - p->topo2);
}

void exibir(PILHADUPLA *p, int k){
	int i = tamanhoPilhaDupla(p, k);
	if( k == 1){
		i--;
		while( i > -1){
			printf("%d ", p->A[i]);
			i--;
		}
		printf("\n");
	}
	else{
		int j = MAX - i;
		while(j < MAX){
			printf("%d ", p->A[j]);
			j++;
		}
		printf("\n");
	}
}

// O vetor est� cheio ?
bool pilhaCheia(PILHADUPLA *p) {
	if(p->topo1 == (p->topo2 - 1) ) return(true);
	else return(false);
}

// Inserir no topo da pilha k
bool pushK(int ch, PILHADUPLA *p, int k) {
	if(pilhaCheia(p)) return(false);
	if(k == 1) {
		p->topo1++;
		p->A[p->topo1] = ch;
	}
	else {
		p->topo2--;
		p->A[p->topo2] = ch;
	}
	return(true);
}

// Retirar do topo k, ou retornar -1
int popK(PILHADUPLA *p, int k) {
	int ch = -1;
	if(k == 1) {
		if(p->topo1 > -1) {
			ch = p->A[p->topo1];
			p->topo1--;
		}
	}
	else {
		if(p->topo2 < MAX) {
			ch = p->A[p->topo2];
			p->topo2++;
		}
	}
	return(ch);
}

// NP Pilhas

typedef struct {
    int base[NP+1]; // pilhas [0..NP-1] + pilha[NP] auxiliar
    int topo[NP+1];
    int A[MAX];
} PILHAS;

// Inicializacao da pilha m�ltipla
void inicializarPilhas(PILHAS *p) {
    int i;
    for(i = 0; i <= NP ; i++) {
        p->base[i] = ( i * (MAX / NP) ) - 1;
        p->topo[i] = p->base[i];
    }
}

// Quantos elementos existem na pilha k
int tamanhoPilhaK(PILHAS p, int k) {
    return(p.topo[k] - p.base[k]);
}

// A pilha k esta cheia ?
bool pilhaKcheia(PILHAS p, int k) {
    if(p.topo[k] == p.base[k + 1])
         return(true);
    else
         return(false);
}

// Desloca pilha k uma posi��o para a direita, se possivel
bool paraDireita(PILHAS *p, int k) {
    int i;
    if( (k < 1) || (k > NP-1) ) return (false); // �ndice inv�lido
    if( (p->topo[k] < p->base[k + 1])) {
        for(i = p->topo[k] + 1; i > p->base[k]; i--) p->A[i] = p->A[i-1];
        p->topo[k]++;
        p->base[k]++;
        return(true);
    }
    return(false);
}

bool paraEsquerda(PILHAS *p, int k){
    int i;
    if( (k < 1) || (k > NP-1) ) return (false); // �ndice inv�lido
    if( (p->topo[k-1] < p->base[k])) {
        for(i = p->base[k] - 1; i < p->topo[k]; i++) p->A[i] = p->A[i+1];
        p->topo[k]--;
        p->base[k]--;
        return(true);
    }
    return(false);
}

// Inserir um novo item no topo da pilha k
bool pushK(int ch, PILHAS *p, int k) {
    int j;
    if( (pilhaKcheia(*p, k)) && (k < NP-1) )
    // desloca p/direita todas as pilhas de [k+1..NP-1] em ordem reversa
       for( j = NP-1; j > k; j--) paraDireita(p, j);
    if( (pilhaKcheia(*p, k)) && (k > 0))
    // desloca p/esquerda todas as pilhas de [1..k] (mas n�o a pilha 0)
       for( j = 1; j <= k; j++) paraEsquerda(p, j);
    if(pilhaKcheia(*p, k)) return(false);
    p->topo[k]++;
    p->A[p->topo[k]] = ch;
    return(true);
}

// Retirar um item da pilha k, ou -1
int popK(PILHAS *p, int k) {
    int resp = -1;
    if( (p->topo[k] > p->base[k]) ) {
        resp = p->A[p->topo[k]];
        p->topo[k]--;
    }
    return(resp);
}

int main(){
	
	int a = 0;
	int b = 1;
	int c = 2;
	int d = 7;
	int e = 8;
	int f = 9;
	
	PILHADUPLA* pd = (PILHADUPLA*) malloc(sizeof(PILHADUPLA));
//	pd->topo1 = -1;
//	pd->topo2 = MAX;
	pd->A[0] = a;
	pd->A[1] = b;
	pd->A[2] = c;
	pd->A[47] = d;
	pd->A[48] = e;
	pd->A[49] = f;
	
	pd->topo1 = 2;
	pd->topo2 = 47;
	
	printf("--> %d \n", tamanhoPilhaDupla(pd, 2));
	exibir(pd,2);
	//pushK(600, pd, 1);
	//pushK(500, pd, 2);
	exibir(pd,1);
	
	popK(pd, 1);
	popK(pd, 2);
	exibir(pd,1);
	exibir(pd,2);
	
	printf("--> %d \n", tamanhoPilhaDupla(pd, 1));
	printf("--> %d \n", tamanhoPilhaDupla(pd, 2));
	
	system("Pause");
	return 0;
}
