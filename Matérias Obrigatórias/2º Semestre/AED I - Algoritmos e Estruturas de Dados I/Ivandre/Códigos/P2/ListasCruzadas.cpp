#include <stdlib.h>
#include <stdio.h>

#define MAXLIN 5
#define MAXCOL 5
#define MAX 5

typedef struct estrutura {
    int lin;
    int col;
    int chave;
    estrutura *proxL;
    estrutura *proxC;
} NO;

typedef struct {
    NO* lin[MAXLIN+1]; // para indexar at� MAXLIN
    NO* col[MAXCOL+1]; // para indexar at� MAXCOL
} MATRIZ;

// Inicializa��o
void inicializarMatriz(MATRIZ *m) {
    int i;
    for(i=0; i <= MAXLIN; i++) m->lin[i] = NULL;
    for(i=0; i <= MAXCOL; i++) m->col[i] = NULL;
    
}

// Conta elementos da estrutura
int contaElementos(MATRIZ m) {
    int i, t;
    NO* p;
    t = 0;
    for(i=1; i<= MAX; i++) {
        p = m.lin[i];
        while (p) {
            t++;
            p = p->proxC;
        }
    }
    return(t);
}

//Exclui elemento p das listas apontadas por *linha e *coluna
void excluir(NO* *linha, NO* *coluna, NO* esq, NO* acima, NO* p) {
    if(p) {
        // desliga do vetor de linhas
        if(esq) esq->proxC = p->proxC;
        else *linha = p->proxC;
        // desliga do vetor de colunas
        if(acima) acima->proxL = p->proxL;
        else *coluna = p->proxL;
        free(p);
    }
}

// Exibe os produtos dos elementos das colunas c1 e c2
void exibirProdutos(MATRIZ m, int c1, int c2) {
    NO* p1 = m.col[c1];
    NO* p2 = m.col[c2];
    int atual = 0; // linha a ser processada
    int v1, v2;
    while ((p1)||(p2)) {
          v1 = 0;
          if(p1) {
                 if(p1->lin < atual) {
                            p1 = p1->proxL;
                            continue;
                 }
                 if(p1->lin == atual) v1 = p1->chave;
          }
          v2 = 0;
          if(p2){
                 if(p2->lin < atual) {
                            p2 = p2->proxL;
                            continue;
                 }
                 if(p2->lin == atual) v2 = p2->chave;
          }
          printf("Linha %d*%d=%d\n", v1, v2, v1*v2);
           atual++;
    }
    if(atual < MAXLIN+1) 
             for(; atual <= MAXLIN; atual++) printf("Linha %d*%d=%d\n", atual,0,0);
}

NO* busca(MATRIZ *m, int x, int y, NO* *esq, NO* *acima){
    *acima =NULL;
    *esq = NULL;
    printf("w");
    NO* l = m->lin[y];
    NO* c = m->col[x];
    printf("r");
    while (c){
          printf("x");
          if(c->lin == y) return c;
          if(c->lin > y) break;
          *acima = c;
          c = c->proxL;
    }
     while (l){
          printf("y");
          if(l->col == x) return l;
          if(l->col > x) break;
          *esq = l;
          l = l->proxC;
    }
    printf("1");
    return NULL;
}

void exibir (MATRIZ *m){
    int i;
    for(i = 0; i < MAXLIN; i++ ){
    	NO* p = m->lin[i];
    	int j, n=0;
    	for(j = 0; j < MAXCOL; j++){
    		if(p && n == p->col) {
    			printf(" %d ", p->chave);
    			p = p->proxC;
    		}
    		else printf(" 0 ");	
    		n++;
    	}
    	printf("\n ");
    }
}

int main(){
    
    MATRIZ* mm = (MATRIZ*) malloc(sizeof(MATRIZ));
    inicializarMatriz(mm);
    
    NO* mt = (NO*) malloc(sizeof(NO));
    NO* mt1 = (NO*) malloc(sizeof(NO));
    NO* mt2 = (NO*) malloc(sizeof(NO));
    NO* mt3 = (NO*) malloc(sizeof(NO));
    NO* mt4 = (NO*) malloc(sizeof(NO));
    NO* mt5 = (NO*) malloc(sizeof(NO));
    NO* mt6 = (NO*) malloc(sizeof(NO));
    NO* mt7 = (NO*) malloc(sizeof(NO));
   // NO* mt8 = (NO*) malloc(sizeof(NO));
    
    mt->lin = 0;
    mt->col = 0;
    mt->chave = 10;
    mt1->lin = 1;
    mt1->col = 0;
    mt1->chave = 20;
    mt2->lin = 2;
    mt2->col = 0;
    mt2->chave = 30;
    mt3->lin = 0;
    mt3->col = 1;
    mt3->chave = 40;
    mt4->lin = 1;
    mt4->col = 1;
    mt4->chave = 50;
    mt5->lin = 2;
    mt5->col = 1;
    mt5->chave = 60;
    mt6->lin = 0;
    mt6->col = 2;
    mt6->chave = 70;
    mt7->lin = 2;
    mt7->col = 2;
    mt7->chave = 80;
    
   mt->proxL = mt1;
   mt1->proxL = mt2;
   mt2->proxL = NULL;
   mt3->proxL = mt4;
   mt4->proxL = mt5;
   mt5->proxL = NULL;
   mt6->proxL = mt7;
   mt7->proxL = NULL;
  // mt8->proxL = NULL;
   
   mt->proxC = mt3;
   mt1->proxC = mt4;
   mt2->proxC = mt5;
   mt3->proxC = mt6;
   mt4->proxC = NULL;
   mt5->proxC = mt7;
   mt6->proxC = NULL;
   mt7->proxC = NULL;
   //mt8->proxC = ;

   mm->col[0] = mt;
   mm->lin[0] = mt;
   mm->col[1] = mt3;
   mm->lin[0] = mt3;
   mm->col[2] = mt6;
   mm->lin[0] = mt6;
   mm->col[0] = mt1;
   mm->lin[1] = mt1;
   mm->col[0] = mt2;
   mm->lin[2] = mt2;


   NO* esq = NULL;
   NO* acima = NULL;
   
//  NO* p = busca( mm, 1, 0, &esq, &acima);
  // printf("-- > %d \n", p->chave );
   exibir(mm);
    system("Pause");
    return 0;
}
