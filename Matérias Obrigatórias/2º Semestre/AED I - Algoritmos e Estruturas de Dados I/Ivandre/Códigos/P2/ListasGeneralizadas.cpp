#include <stdlib.h>
#include <stdio.h>

typedef enum{elemLista, inicioLista} IDENT;

typedef struct estrutura {
    IDENT tag;
    union {
        int chave;
        struct estrutura *sublista;
    };
    estrutura *prox;
} NO;

// Inicializa��o
void inicializarLista(NO* *p) {
     *p = NULL;
}

// Quantidade de chaves na lista
int contarChaves(NO* p) {
    int chaves = 0;
    while (p) {
        if( p->tag == elemLista) chaves++;
        else chaves = chaves + contarChaves(p->sublista);
        p = p->prox;
    }
    return(chaves);
}

// Quantidade de nos na lista
int contarNos(NO* p) {
    int nos = 0;
    while (p) {
        nos++;
        if( p->tag == inicioLista) nos = nos + contarNos(p->sublista);
        p = p->prox;
    }
    return(nos);
}

// Profundidade maxima da lista
int profundidade(NO* p) {
    int maximo = 0;
    int resp;
    if(!p) return(maximo);
    while(p) {
        if( p->tag == elemLista) resp = 0;
        else resp = profundidade(p->sublista);
        if(resp > maximo) maximo = resp;
        p = p->prox;
    }
    return(maximo + 1);
}

// copia uma lista inteira
NO* copiarListaGen(NO* p) {
    NO* novo;
    NO* abaixo;
    NO* dir;
    IDENT tipo;
    novo = NULL;
    if (p) {
        tipo = p->tag;
        if( tipo == inicioLista) abaixo = copiarListaGen(p->sublista);
        dir = copiarListaGen(p->prox);
        novo = (NO *) malloc(sizeof(NO));
        novo->tag = tipo;
        novo->prox = dir;
        if( tipo == elemLista) novo->chave = p->chave;
        else novo->sublista = abaixo;
    }
    return(novo);
}

// verifica se duas listas s�o identicas
bool listasIguais(NO* a, NO* b) {
    bool resp = false;
    if((!a) && (!b)) return(true);
    if((a) && (b)) {
        if( a->tag == b->tag) {
            if( a->tag == elemLista) resp = (a->chave == b->chave);
            else resp = listasIguais(a->sublista, b->sublista);
            if(resp) resp = listasIguais(a->prox, b->prox);
        }
    }
    return(resp);
}

int main(){
    NO* n = (NO*) malloc(sizeof(NO));
    NO* n1 = (NO*) malloc(sizeof(NO));
    NO* n2 = (NO*) malloc(sizeof(NO));
    
    NO* s = (NO*) malloc(sizeof(NO));
    NO* s1 = (NO*) malloc(sizeof(NO));
    NO* s2 = (NO*) malloc(sizeof(NO));
    
    n->tag = elemLista;
    n1->tag = inicioLista;
    n2->tag = elemLista;
    
    s->tag = inicioLista;
    s1->tag = inicioLista;
    s2->tag = elemLista;
    
    n1->sublista = s;
    s->sublista = s1;
    
    n->chave =10;
    n2->chave = 20;
    s2->chave = 30;
    
    s->prox = NULL;
    s1->prox = s2;
    s2->prox = NULL;
    
    n->prox = n1;
    n1->prox = n2;
    n2->prox = NULL;
    
    //printf("--> %d ", contarChaves(n));
    printf("--> %d ", profundidade(n));
    
    system("Pause");
    return 0;
}
