#include <stdlib.h>
#include <stdio.h>

#define MAX 5

// DINAMICA

typedef struct estrutura {
    int chave;
    estrutura *prox;
} NO;

typedef struct {
    NO* inicio;
    NO* fim;
} Fdinam;

// Inicializa��o da fila dinamica
void inicializarFdinam(Fdinam *f) {
    f->inicio = NULL;
    f->fim = NULL;
}

// quantos elementos existem
int tamanhoFdinam(Fdinam *f) {
    NO* p;
    int tam = 0;
    p = f->inicio;
    while (p) {
        tam++;
        p = p->prox;
    }
    return(tam);
}

// inserir item ao final da fila dinamica
void entrarFdinam(int ch, Fdinam *f) {
    NO* novo;
    novo = (NO*) malloc(sizeof(NO));
    novo->chave = ch;
    novo->prox = NULL;
    if(f->fim) f->fim->prox = novo; // fila n�o � vazia
    else f->inicio = novo; // 1a. inser��o em fila vazia
    f->fim = novo;
}

// retirar a chave da frente ou -1
int sairFdinam(Fdinam *f) {
    NO* aux;
    int ch;
    if(!f->inicio) return(-1);
    ch = f->inicio->chave;
    aux = f->inicio;
    f->inicio = f->inicio->prox;
    free(aux);
    if(!f->inicio) f->fim = NULL; // fila ficou vazia
    return(ch);
}

void exibir(Fdinam *f){
     NO* p = f->inicio;
     while(p){
         printf("%d ", p->chave);
         p = p->prox;
     } 
     printf("\n");
}

// ESTATICA

typedef struct {
    int chave;
} RegistroEstat;

typedef struct {
    int inicio;
    int fim;
    RegistroEstat A[MAX];
} Festat;

// Inicializacao da fila est�tica
void inicializarFestat(Festat *f) {
    f->inicio = -1;
    f->fim = -1;
}

int tamanhoFestat(Festat *f){
    if(f->inicio == -1) return 0;
    if(f->inicio < f->fim ) return (f->fim - f->inicio + 1);
    else return (MAX - f->inicio + f->fim + 1);
}

// Inserir novo item ao final
bool entrarFestat(int ch, Festat *f) {
    if(tamanhoFestat(f) >= MAX) return(false);
    f->fim = (f->fim + 1) % MAX;
    f->A[f->fim].chave = ch;
    if(f->inicio < 0 ) f->inicio = 0;
    return(true);
}

// Retirar um item da frente ou retornar -1 se vazia
int sairFestat(Festat *f) {
    if(f->inicio < 0) return(-1);
    int ch = f->A[f->inicio].chave;
    if(f->inicio != f->fim)
        f->inicio = (f->inicio + 1) % MAX;
    else {
        f->inicio = -1;
        f->fim = -1;
    }
    return(ch);
}

void exibe(Festat *f){
     if(f->inicio < f->fim){
         int i = f->inicio;
         while(i <= f->fim){
              printf("%d ", f->A[i].chave );
              i++;
         }
         printf("\n");
     }
     else{
          int j = f->inicio;
          while(j < MAX){
              printf("%d ", f->A[j].chave );
             // printf("-+%d ", j);
              j++;
          }
          int k = 0;
          while( k <= f->fim){
              printf("%d ", f->A[k].chave );
              //printf("--%d ", k);
              k++;
          }
     }
     printf("\n");
}

int main(){
    
    //  DINAMICA
	NO* nf = (NO*) malloc(sizeof(NO));
	NO* nf1 = (NO*) malloc(sizeof(NO));
	NO* nf2 = (NO*) malloc(sizeof(NO));
	NO* nf3 = (NO*) malloc(sizeof(NO));
	NO* nf4 = (NO*) malloc(sizeof(NO));
	NO* nf5 = (NO*) malloc(sizeof(NO));
	
	nf->chave = 0;
	nf1->chave = 1;
	nf2->chave = 2;
	nf3->chave = 3;
	nf4->chave = 4;
	nf5->chave = 5;
	
	nf->prox = nf1;
	nf1->prox = nf2;
	nf2->prox = nf3;
	nf3->prox = nf4;
	nf4->prox = nf5;
	nf5->prox = NULL;
	
	Fdinam* fl = (Fdinam*) malloc(sizeof(Fdinam));
	//fl->inicio =  NULL;
    //fl->fim = NULL;
	
    fl->inicio =  nf;
    fl->fim = nf5;
    
    printf("---- Dinamico ----\n");
    
    printf("--> %d \n", tamanhoFdinam(fl));
    entrarFdinam(600, fl);
    exibir(fl);
    printf("--> %d \n", sairFdinam(fl));
    exibir(fl);
    
    //  ESTATICA
	printf("---- Estatico ----\n");
	RegistroEstat a;
	RegistroEstat b;
	RegistroEstat c;
	RegistroEstat d;
	RegistroEstat e;
	
	a.chave = 0;
	b.chave = 1;
	c.chave = 2;
	d.chave = 7;
	e.chave = 9;

	Festat* ft = (Festat*) malloc(sizeof(Festat));
	//ft->inicio = -1;
	//ft->fim = -1;
	
	ft->A[0] = a;
	ft->A[1] = b;
	ft->A[2] = c;
	ft->A[3] = d;
	ft->A[4] = e;
	
	ft->inicio = 3;
	ft->fim = 2;

    printf("--> %d \n", tamanhoFestat(ft));
    exibe(ft);
    
    system("Pause");
    return 0;
}
