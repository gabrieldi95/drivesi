#include <stdlib.h>
#include <stdio.h>

#define MAX 50

// DINAMICA

typedef struct estrutura {
	int chave;
	estrutura *prox;
} NO;

typedef struct {
	NO* topo;
} Pdinam;

// Inicializa��o da pilha din�mica
void inicializarPdinam(Pdinam *p) {
	p->topo = NULL;
}

// Quantos elementos existem
int tamanhoPdinam(Pdinam *p) {
	NO* p1 = p->topo;
	int tam = 0;
	while(p1) {
		tam++;
		p1 = p1->prox;
	}
	return(tam);
}

// Inserir item no topo
void push(int ch, Pdinam *p) {
	NO* novo = (NO*) malloc(sizeof(NO));
	novo->chave = ch;
	novo->prox = p->topo;
	p->topo = novo;
}

// Retirar a chave do topo ou -1
int pop(Pdinam *p) {
	NO* aux;
	int ch;
	if(!p->topo) return(-1);
	aux = p->topo;
	ch = aux->chave;
	p->topo = p->topo->prox;
	free(aux);
	return(ch);
}
void exibe(Pdinam *pl){
	NO* p = pl->topo;
	while(p){
		printf("%d ", p->chave);
		p = p->prox;
	}
	printf("\n");
}

// ESTATICA

typedef struct {
	int chave;
} RegistroEstat;

typedef struct {
	int topo;
	RegistroEstat A[MAX];
} PESTAT;

// Inicializa��o da pilha est�tica
void inicializarPestat(PESTAT *p) {
	p->topo = -1;
}

// A pilha est�tica est� cheia ?
bool pilhaCheia(PESTAT p) {
	if( p.topo >= MAX - 1 ) return(true);
	else return(false);
}

// Quantos elementos existem
int tamanhoPestat(PESTAT *p) {
	return(p->topo+1);
}

// Inserir no topo da pilha est�tica
bool push(int ch, PESTAT *p) {
	if( tamanhoPestat(p) >= MAX ) return(false);
	p->topo++;
	p->A[p->topo].chave = ch;
	return(true);
}

// Retirar do topo ou retornar -1 se vazia
int pop(PESTAT *p) {
	if(p->topo < 0) return (-1);
	int ch = p->A[p->topo].chave;
	p->topo--;
	return(ch);
}

void exibir(PESTAT *p){
	int i = p->topo;
	while(i > -1){
		printf("%d ", p->A[i].chave);
		i--;
	}
	printf("\n");
}

int main(){
	//  DINAMICA
	NO* np = (NO*) malloc(sizeof(NO));
	NO* np1 = (NO*) malloc(sizeof(NO));
	NO* np2 = (NO*) malloc(sizeof(NO));
	NO* np3 = (NO*) malloc(sizeof(NO));
	NO* np4 = (NO*) malloc(sizeof(NO));
	NO* np5 = (NO*) malloc(sizeof(NO));
	
	np->chave = 0;
	np1->chave = 1;
	np2->chave = 2;
	np3->chave = 3;
	np4->chave = 4;
	np5->chave = 5;
	
	np->prox = NULL;
	np1->prox = np;
	np2->prox = np1;
	np3->prox = np2;
	np4->prox = np3;
	np5->prox = np4;
	
	Pdinam* pl = (Pdinam*) malloc(sizeof(Pdinam));
	//pl->topo = NULL;
	pl->topo = np5;
	
	NO* p = pl->topo;

	printf("%d ", tamanhoPdinam(pl));
	
	printf("\n");
//	push(600, pl);
	exibe(pl);
	
	printf("--> %d ",pop(pl));
	printf("\n");
	exibe(pl);
	
	//  ESTATICA
	printf("Estatico\n");
	RegistroEstat a;
	RegistroEstat b;
	RegistroEstat c;
	RegistroEstat d;
	RegistroEstat e;
	
	a.chave = 0;
	b.chave = 1;
	c.chave = 2;
	d.chave = 3;
	e.chave = 4;

	PESTAT* pt = (PESTAT*) malloc(sizeof(PESTAT));
	//pt.topo = -1;
	
	pt->A[0] = a;
	pt->A[1] = b;
	pt->A[2] = c;
	pt->A[3] = d;
	pt->A[4] = e;
	pt->topo = 4;
	
	printf("%d ", tamanhoPestat(pt));
	printf("\n");
//	push(600, pt);
	exibir(pt);
	printf("--> %d ", pop(pt));
	printf("\n");
	exibir(pt);
	
	system("Pause");
	return 0;
}
