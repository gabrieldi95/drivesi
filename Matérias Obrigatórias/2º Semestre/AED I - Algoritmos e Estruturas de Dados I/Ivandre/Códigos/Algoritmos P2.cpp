# include stdin.h

/* FILAS: listas lineares com disciplina de acesso FIFO
 * armazena-se as extremidades da estrutura - inicio e fim
 * entradas em fim e saidas em inicio
 * implementacao estatica: vetor circular
 * implementacao dinamica: ponteiros
 * eficiencia identica
 */

//DINAMICA *********************************************************************
typedef struct estrutura
{
        TIPOCHAVE chave;
        estrutura *prox;
} NO;

typedef struct
{
        NO *inicio;
        NO *fim;
} Fdinam;

//Inicialização
void inicializarFdinam(Fdinam *f)
{
     f->inicio = NULL;
     f->fim = NULL;
}

//size contar elementos
int tamanhoFdinam(Fdinam f)
{
    NO *p;
    int tam = 0;
    p = f.inicio;
    while (p)
    {
          tam++;
          p = p->prox;
    }
    return(tam);
}

//enqueue(e) inserir
void inserir(TIPOCHAVE ch, Fdinam* f)
{
    NO *novo;                            //cria um novo no
    novo = (NO*)malloc(sizeof(NO))       //aloca a memoria
    novo->chave = ch;                    //insere a chave
    novo->prox = NULL                    //define o prox como NULL (ultimo elemento)
    if (f->fim) f->fim->prox = novo;     //se houver fim, liga esse elemento com o novo
    else f->inicio = novo;               //se nao, liga com o inicio
    f->fim = novo;                       //define o novo como fim da fila
}

//dequeue retirar
TIPOCHAVE retirar(Fdinam *f)
{
    NO *p;                               //ponteiro para o no a ser retirado
    TIPOCHAVE ch;                        //armazenara a chave
    if(!f->inicio) return (-1);          //erro, fila nao inicializada
    else
    {
        ch = f->inicio->chave;           //copia o valor do elemento
        f->inicio = p;                   //aponta para o no que sera retirado
        f->inicio = f->inicio->prox;     //define o inicio como o proximo elemento
        free(p);                         //remove o no
        if(!f->inicio) f->fim = NULL;    //fila vazia
    }
    return(ch);                          //devolve o valor
}

//isEmpty: true se inicio = NULL && fim = NULL
//head: return inicio->chave sem remover no, return(-1) se estiver vazia

//ESTATICA *********************************************************************
typedef struct
{
        TIPOCHAVE chave;
} Registro;

typedef struct
{
        int inicio;
        int fim;
        Registro A[MAX];
} FILA_est;

void inicializar(FILA_est *f)
{
     f->inicio = -1;
     f->fim = -1;
}

int tamanho(FILA_est f)
{
    if (f->inicio > f->fim) return ((f->fim + 1 + (f->inicio % 7));
    else return ((f->fim - f->inicio) + 1);
}

bool enqueue(FILA_est *f, TIPOCHAVE ch)
{
     if (tamanho(f) >= MAX) return (false);
     f->fim = (f->fim+1) % MAX;               //para garantir a circularidade: lembrar 9%7=2, vai inserindo em movendo...
     f->A[fim].chave = ch;                    //acessar e modificar a chave
     if (f->inicio < 0) f->inicio = 0;        //caso seja o primeiro elemento
     return (true);
}

TIPOCHAVE dequeue(FILA_est *f)
{
          int ch;                             
          f->A[inicio].chave = ch;            //guarda a resposta
          if (f->inicio != fim)
             f->inicio = (f->inicio+1) % MAX; //para garantir a circularidade: lembrar 9%7=2, vai inserindo em movendo...
          else
              f->inicio = f->fim = -1;        //fila vazia, reinicia a contagem
          return (ch);
}


// *****************************************************************************

/* DEQUES: FILAS que permitem entrada e retirada das duas extremidades
 * ha inicio1 e inicio2 (ou inicioESQ, inicioDIR)
 * implementacao dinamica: encadeamento duplo com acesso em tempo O(1);
 */
 
//DINAMICA COM ENCADEAMENTO DUPLO

typedef struct estrutura
{
        TIPOCHAVE ch;
        estrutura *prox;
        estrutura *ant;
} NO;

typedef struct
{
        NO *inicioEsq;
        NO *inicioDir;
} DEQUE;

//inicializar
void inicializarDeque(DEQUE *d)
{
     d->inicioEsq = NULL;
     d->inicioDir = NULL;
}

//tamanho do double-ended queue
int tamanho(DEQUE d)
{
    NO *p = d->inicioEsq;
    int tam = 0;
    
    while (p)
    {
          tam++;
          p = p->prox;
    }
    return (tam);
}

void inserirEsq(DEQUE d, TIPOCHAVE ch)
{
    NO* novo = (NO*) malloc(sizeof(NO));
    novo->chave = ch;
    novo->ant = NULL;                             //pois esta inserindo da esquerda
    novo->prox = d->inicioEsq;                       //ajusta o ponteiro do elemento inserido
    if (d->inicioEsq) d->inicioEsq->ant = novo;   //ja ha elemento, ajusta ponteiro desse elemento
    else d->inicioDir = novo;                     //1a. insercao
    d->inicioEsq = novo;
}

void inserirDir(DEQUE d, TIPOCHAVE ch)
{
    NO* novo = (NO*) malloc(sizeof(NO));
    novo->chave = ch;
    novo->ant = d->inicioDir;                             //pois esta inserindo da esquerda
    novo->prox = NULL;                       //ajusta o ponteiro do elemento inserido
    if (d->inicioDir) d->inicioDir->prox = novo;   //ja ha elemento, ajusta ponteiro desse elemento
    else d->inicioEsq = novo;                     //1a. insercao
    d->inicioDir = novo;
}

TIPOCHAVE sairEsq(DEQUE *d)
{
          NO* aux;
          if (!d->inicioEsq) return (-1);
          aux = d->inicioEsq;
          d->inicioEsq = d->inicioEsq->prox;
          free(aux);
          if (!d->inicioEsq) d->inicioDir = NULL;
          else d->inicioEsq->ant = NULL;
          return (ch);
}

TIPOCHAVE sairDir(DEQUE *d)
{
          NO * aux;
          if (!d->inicioDir) return (-1);
          aux = d->inicioDir;
          d->inicioDir = d->inicioDir->ant;
          free(aux);
          if (!d->inicioDir) d->inicioEsq = NULL;
          else d->inicioDir->prox = NULL;
}

void destruir(DEQUE *d)
{
     while (d->inicioEsq) sairEsq(d);
}

// *****************************************************************************

/* PILHAS: listas lineares com acesso LIFO (Last In First Out) ou FILO
 * soh ha o topo, insercao e remocao (push e pop) sao realizadas no top
 * implementacao dinamica e estatica da STACK apresentam a mesma eficiencia
 */
 
//DINAMICA 
typedef struct estrutura
{
        TIPOCHAVE ch;
        estrutura *prox;
} NO;

typedef struct
{
        NO * topo;
} PILHA_d;
 
void inicializar(PILHA_d *p)
{
     p->topo = NULL;
}

int size(PILHA_d p)
{
    NO *p1;
    int tam = 0;
    //if (!p->topo) return  (-1); //pilha vazia
    p1 = p.topo;
    while (p1)
    {
          tam++;
          p1 = p1->prox;
    }
    return (tam);
}

int push(PILHA_d *p, TIPOCHAVE ch)
{
    NO *novo = (NO*) malloc(sizeof(NO));
    novo->chave = ch;
    novo->prox = p->topo;
    p->topo = novo;
}

TIPOCHAVE pop(PILHA_d *p) //retorna a chave
{
    NO* aux = p->topo;
    TIPOCHAVE ch;
    if (!p->topo) return (-1); //pilha vazia
    aux = topo;
    ch = topo->chave;
    p->topo = p->topo->prox;
    free(aux);
    return(ch);
}

//ESTATICA

typedef struct
{
        TIPOCHAVE ch;
} Registro;

typedef struct
{
        int topo;
        Registro A[MAX];
} PILHA_e;

void start(PILHA_e *p)
{
     p->topo = -1;
}

void pilhaCheia(PILHA_e p)
{
     if (p.topo >= MAX-1) return (true);
     else return (false); 
}

bool push(PILHA_e *p, TIPOCHAVE ch)
{
     if (pilhaCheia(p)) return (false); //testar a pilha
     p->topo++;
     p->A[p->topo].chave = ch;
     return (true);
}

TIPOCHAVE pop(PILHA_e p)
{
          TIPOCHAVE ch; //guarda o valor
          if (p->topo < 0) return (-1); //pilha vazia
          p->A[p->topo]->chave = ch; //nao esquecer que topo eh um int!!!
          p->topo--;
          return (ch);
}

/* DUAS PILHAS em um vetor
 * topo1, inicializa com -1, posicoes de 0 a MAX-1
 * topo2, inicializa com MAX, posicoes de MAX-1 a 0
 * Pilha cheia: topo2 - topo1 == 1
 */
 
typedef struct
{
        TIPOCHAVE ch;
} Registro;

typedef struct
{
        int topo1;
        int topo2;
        Registro A[MAX];
} PILHA_dupla;

void inicializar(PILHA_dupla *pd)
{
     pd->topo1 = -1;
     pd->topo2 = MAX;
}

bool pilhaCheia(PILHA_dupla pd)
{
     if (pd.topo1 == (pd.topo2-1)) return (true);
     else return (false);
}

//retorna o tamanho da pilha escolhida
int tamanhoPilhaDupla(PILHA_dupla pd, int opcao)
{
    if (opcao == 1) return (pd.topo1 + 1);
    else return (MAX - pd.topo2);
}

int push(PILHA_dupla *pd, TIPOCHAVE ch, int opcao)
{
    if (pilhaCheia(pd)) return (false);
    if (opcao == 1)
    {
       pd->topo1++;
       pd->A[p->topo1] = ch;
    }
    else
    {
        pd->topo2--;
        pd->A[p->topo2] = ch;
    }
    return (true);
}

//retira o elemento ou retorna erro(-1)
TIPOCHAVE pop(PILHA_dupla *pd, int opcao)
{
    TIPOCHAVE ch = -1; //usado em caso de erro
    if (opcao == 1)
    {
              if (pd->topo1 > -1)
              {
                 ch = pd->A[pd->topo].chave;
                 topo--;
              }
    }
    else
    {
        if (pd->topo2 < MAX)
        {
           ch = pd->A[pd->topo2].chave;
           topo++;
        }
    }
    return (ch);
}
