# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h> //o booleano so funciona se incluir isso 
# include <conio.h>
# include <string.h>
# define MAX 100  //ATENCAO, nao eh atribuicao eh MACRO

//Roteiro das aulas
//1. Tipo de E.D.
//2. Operacoes basicas
//3. Eficiencia

//Listas lineares
//Colecao de elementos que possui um inicio. Para se chegar a qualquer elemento 
//precisamos partir do primeiro.

// LISTA LINEAR SEQUENCIAL


typedef int TIPOCHAVE;

typedef struct
{
        TIPOCHAVE chave;
} REGISTRO;


typedef struct
{
        REGISTRO A[MAX];
        int nroElem;
} LISTA;

// LISTA LINEAR SEQUENCIAL - inicializacao
void inicializarLista(LISTA *l)
{
     l->nroElem = 0;
}
// LISTA LINEAR SEQUENCIAL - tamanho
int tamanho(LISTA l)
{
     return(l.nroElem);
}
// LISTA LINEAR SEQUENCIAL - lista cheia
bool listaCheia(LISTA l)
{
     if (l.nroElem == MAX) return (true);
     else return (false);
}
// LISTA LINEAR SEQUENCIAL - apagar lista
bool apagaLista(LISTA *l)
{
     if (l->nroElem > 0) 
     {
        l->nroElem = 0;
        return (true);
     }
     return (false);
}
// LISTA LINEAR SEQUENCIAL - inserir elemento
bool inserir(LISTA *l, int i, TIPOCHAVE ch)
{
     int j; //variavel de deslocamento
     
     if (i < 1 || i > (l->nroElem+1)) return false; //testa se a posicao "i" eh valida ou se estah alem do tamanho da lista.
     if (l->nroElem > MAX) return false; //testa se a lista nao esta cheia
     if (i < l->nroElem) //testa se i nao eh a ultima posicao
     {
           for (j = l->nroElem+1; j >= i; j--) //desloca os elementos para a frente
               l->A[j] = l->A[j-1];            //desloca os elementos para a frente
           l->A[i].chave = ch; //insere a chave
           l->nroElem++; //atualiza o numero de elementos
     }
     return true;
}
// LISTA LINEAR SEQUENCIAL - buscar na lista e retornar a posicao
int buscaSeq(LISTA l, TIPOCHAVE ch)
{
    int resp = -1; //convencao
    int j;
    for (j = 0; j <= l.nroElem; j++)
        if (l.A[j].chave == ch) return (j);
    return (resp);
}
// LISTA LINEAR SEQUENCIAL - excluir elemento pesquisando pela chave
int excluir(LISTA *l, TIPOCHAVE ch)
{
    int j; //variavel de deslocamento
    int i = buscaSeq(*l, ch); //obtem o indice relativo a chave passada
    if (i == -1) return (false); //a chave nao existe
    for (j = i ; j < l->nroElem-1; j++) //desloca os elementos apartir da posicao encontrada
        l->A[j] = l->A[j+1];
    l->nroElem--; //atualiza o numero de elementos
    return (true);
}

// Definicao de tipo com sentinela. Cria uma lista artificialmente com um elemento a mais.
// Facilita os algoritmos de busca, pois o elemento SEMPRE sera encontrado.
// Reduz o tempo de busca.
typedef struct
{
        TIPOCHAVE A[MAX+1];
        int nroElem;
} LISTA_SENTINELA;

// LISTA LINEAR SEQUENCIAL COM SENTINELA - pesquisar elemento pela chave e retorna a posicao
int buscaSentinela(LISTA_SENTINELA l, TIPOCHAVE ch)
{
    l.A[MAX] = ch; //coloca a chave procurada na posicao de sentinela
    int i = 0;  
    while (l.A[i] != ch) //testar pra ver se funciona mesmo
    {
          i++;
    }
    if (i<MAX) return (i);
    else return (-1);
}

// DISCUSSAO
// A busca em lista sequencial eh O(lg n). O acesso (em caso de lista ordenada)
// eh direto, O(1). Porem INS/DEL serao sempre O(n), o que eh terrivel para
// aplicacoes que manipulam mapas de bits, como por exemplo Jogos.


//LISTA DE EXERCICIOS 1:
//6 - Dada uma lista sequencial ordenada e um valor de chave qualquer, eliminar 
//    as ocorrencias repetidas desta chave (mantendo apenas a 1a. ocorrencia).
int main ()
{
    LISTA L;
    L.nroElem = 10;
    int i;
    int tamanho;
    int apagar; //chave a excluir
    int sair = 1;
    while(sair != 0)
    {
        printf("\nInsira o tamanho desejado:");
        fflush(stdin);
        scanf("%i", &tamanho);  //atencao para a passagem por referencia
        L.nroElem = tamanho;
        printf("\nTamanho: %d\n", L.nroElem);
        for (i = 0; i < L.nroElem; i++)
        {
            if (i%3 == 0 || i%2 == 0) L.A[i].chave = 0;
            else L.A[i].chave = 1;
        }
    
        for (i = 0; i < L.nroElem; i++) printf("%d-", L.A[i].chave);
        
        printf("\n");
        printf("\nInsira a chave a ser apagada:");
        fflush(stdin);
        scanf("%i", &apagar);  //atencao para a passagem por referencia  
        
        int i = 0;
        while (i < L.nroElem)
        {
               if (apagar == L.A[i].chave)
               {
                  int j = i+1;
                  while (L.A[i].chave == L.A[i+1].chave)
                  {
                      for (j; j < L.nroElem; j++)
                      {
                          L.A[j].chave = L.A[j+1].chave;
                          L.nroElem--;
                      }
                  }
               }
               i++;
        }
    
        printf("\nTamanho: %i \n", L.nroElem);
        for (i = 0; i < L.nroElem; i++) printf("%d-", L.A[i].chave);
        fflush(stdin);
        scanf("\nDeseja sair?%i", &sair);
    }
}
