# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>
# include <conio.h>
# include <string.h>
# include <time.h>
# define MAX 50

typedef int TIPOCHAVE;

typedef struct
{
        TIPOCHAVE chave;
} REGISTRO;


typedef struct
{
        REGISTRO A[MAX];
        int nroElem;
} LISTA;

//Funcao do exercicio 02
int excluir(LISTA *l, int pos)
{
    int j = pos+1;
    for (j; j < l->nroElem; j++)
        l->A[j] = l->A[j+1];
    l->nroElem--;
    return (true);
}

//Funcao do exercicio 03
int comparar(LISTA l1, LISTA l2, int *pos)
{
    int i = 0;
    if (l1.nroElem < l2.nroElem) return -1;
    if (l1.nroElem > l2.nroElem) return -2;
    while (l1.A[i].chave == l2.A[i].chave)
    {
          i++;
    }
    if (l1.nroElem != i)
    {
       *pos = i;
       return -3;
    }
    else return 0;
}

//Funcao do exercicio 04
int inverter(LISTA *l, int i1, int i2)
{
    if (i1 >= 0 && i1 < i2 && i2 < l->nroElem)
    {
       int aux = 0;
       while (i1 < i2)
       {
             aux = l->A[i1].chave;
             l->A[i1].chave = l->A[i2].chave;
             l->A[i2].chave = aux;
             i1++;
             i2--;
       }
       return 0;
    }
    else
    {
        printf("Indices invalidos.");
        return -1;
    }
}

//Funcao do exercicio 05
LISTA somar(LISTA l1, LISTA l2)
{
      LISTA l3;
      if (l1.nroElem >= l2.nroElem) l3.nroElem = l1.nroElem;
      else l3.nroElem = l2.nroElem;
      
      for (int i = 0; i < l3.nroElem; i++)
      {
          if (i > l1.nroElem-1) l3.A[i].chave = l2.A[i].chave;
          else if (i > l2.nroElem-1) l3.A[i].chave = l1.A[i].chave;
          else l3.A[i].chave = l1.A[i].chave + l2.A[i].chave;
      }
      return l3;
}

int main()
{
    srand(time(NULL));
    
    LISTA l1;
    LISTA l2;
    LISTA l3;
    
    l1.nroElem = rand()%20;
    l2.nroElem = rand()%20;
    
    for (int i = 0; i < l1.nroElem; i++)
        l1.A[i].chave = rand()%20;
    for (int i = 0; i < l2.nroElem; i++)
        l2.A[i].chave = rand()%20;
    
    printf("Listas originais:\n");
    
    for (int i = 0; i < l1.nroElem; i++)
    {
        if (l1.A[i].chave<10) printf("0%d-", l1.A[i].chave);
        else printf("%d-", l1.A[i].chave);
    }
    printf("\n");
    for (int i = 0; i < l2.nroElem; i++)
    {
        if (l2.A[i].chave<10) printf("0%d-", l2.A[i].chave);
        else printf("%d-", l2.A[i].chave);
    }
    
    l3 = somar(l1, l2);
    
    printf("\nListas somadas:\n");
    for (int i = 0; i < l3.nroElem; i++)
    {
        if (l3.A[i].chave<10) printf("0%d-", l3.A[i].chave);
        else printf("%d-", l3.A[i].chave);
    }

    getch();
}
