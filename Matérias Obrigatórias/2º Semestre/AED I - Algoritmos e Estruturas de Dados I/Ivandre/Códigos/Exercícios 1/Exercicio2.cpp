# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>
# include <conio.h>
# include <string.h>
# include <time.h>
# define MAX 50

typedef char TIPOCHAVE;

typedef struct
{
        TIPOCHAVE chave;
} REGISTRO;


typedef struct
{
        REGISTRO A[MAX];
        int nroElem;
} LISTA;

int excluir(LISTA *l, int pos)
{
    int j = pos+1;
    for (j; j < l->nroElem; j++)
        l->A[j] = l->A[j+1];
    l->nroElem--;
    return (true);
}

int main()
{
    LISTA l;
    l.A[0].chave = 'B';
    l.A[1].chave = 'B';
    l.A[2].chave = 'B';
    l.A[3].chave = 'B';
    l.A[4].chave = 'A';
    l.A[5].chave = 'B';
    l.A[6].chave = 'A';
    l.A[7].chave = 'A';
    
    int tam = 0;
    while(l.A[tam].chave != NULL)
    {
        tam++;
    }
    l.nroElem = tam;
    
    printf("\nLista Inicial: \n");
    for (int i = 0; i < l.nroElem; i++)
    {
        printf("%c-", l.A[i].chave);
    }
    getch();
    
    int i = 0;
    while (i < l.nroElem)
    {
          if (l.A[i].chave == l.A[i+1].chave) excluir(&l, i);
          else i++;
    }    
        
    printf("\n\nResultado: \n");
    for (int i = 0; i < l.nroElem; i++)
    {
        printf("%c-", l.A[i].chave);
    }
    getch();
}
