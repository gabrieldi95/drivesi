# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>
# include <conio.h>
# include <string.h>
# include <time.h>
# define MAX 50

typedef int TIPOCHAVE;

typedef struct
{
        TIPOCHAVE chave;
} REGISTRO;


typedef struct
{
        REGISTRO A[MAX];
        int nroElem;
} LISTA;

int excluir(LISTA *l, int pos)
{
    int j = pos+1;
    for (j; j < l->nroElem; j++)
        l->A[j] = l->A[j+1];
    l->nroElem--;
    return (true);
}

int comparar(LISTA l1, LISTA l2, int *pos)
{
    int i = 0;
    if (l1.nroElem < l2.nroElem) return -1;
    if (l1.nroElem > l2.nroElem) return -2;
    while (l1.A[i].chave == l2.A[i].chave)
    {
          i++;
    }
    if (l1.nroElem != i)
    {
       *pos = i;
       return -3;
    }
    else return 0;
}

int main()
{
    srand(time(NULL));
    LISTA l1;
    LISTA l2;
    int tam = 0;
    int opcao = 1+rand()%3;

    printf("Gerando listas...");
    getch();
    
    if (opcao == 1)
    {
       l1.nroElem = 10;
       l2.nroElem = l1.nroElem;
       for (int i = 0; i < l1.nroElem; i++)
       {
           l1.A[i].chave = rand()%10;
           l2.A[i] = l1.A[i];
       }
    }
    else if (opcao == 2)
    {
         l1.nroElem = rand()%20;
         l2.nroElem = l1.nroElem;
         for (int i = 0; i < l1.nroElem; i++)
         {
             if (i < rand()%(l1.nroElem-5))
             {
                l1.A[i].chave = rand()%10;
                l2.A[i].chave = l1.A[i].chave;
             }
             else
             {
                 l1.A[i].chave = rand()%10;
                 l2.A[i].chave = rand()%10;
                 }
         }
    }
    else if (opcao == 3)
    {
         l1.nroElem = 20+rand()%10;
         l2.nroElem = 10+rand()%9;
         for (int i = 0; i < l1.nroElem; i++)
             l1.A[i].chave = rand()%10;
         for (int i = 0; i < l2.nroElem; i++)
             l2.A[i].chave = rand()%10;
    }
    else if (opcao == 4)
    {
         l1.nroElem = 10+rand()%9;
         l2.nroElem = 20+rand()%10;
         for (int i = 0; i < l1.nroElem; i++)
             l1.A[i].chave = rand()%10;
         for (int i = 0; i < l2.nroElem; i++)
             l2.A[i].chave = rand()%10;
    }
    else
    {
        printf("\nN�o foi digitada uma opcao valida!\n");
        getch();
        return -1;
    }
    
    printf("\nLista 1: \n");
    for (int i = 0; i < l1.nroElem; i++)
    {
        printf("%d-", l1.A[i].chave);
    }
    getch();
    printf("\nLista 2: \n");
    for (int i = 0; i < l2.nroElem; i++)
    {
        printf("%d-", l2.A[i].chave);
    }
    getch();

    int pos = 0;    
    int resultado = comparar(l1, l2, &pos);

    
    printf("\n\nResultado: ");
    if (resultado == 0) printf("listas identicas.");
    else if (resultado == -1) printf("lista 1 menor que lista 2.");   
    else if (resultado == -2) printf("lista 2 menor que lista 1.");
    else printf("Primeira chave diferente na posicao %d", pos);
    getch();
}
