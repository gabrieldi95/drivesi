# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>
# include <conio.h>
# include <string.h>
# include <time.h>
# define MAX 50

typedef int TIPOCHAVE;

typedef struct
{
        TIPOCHAVE chave;
} REGISTRO;

typedef struct
{
        REGISTRO A[MAX];
        int nroElem;
} LISTA;


//Funcao util
void exibir(LISTA l, int opcao, int comZero)
{
     if (opcao == 0) printf("\nLista original:\n");
     else if (opcao == 1) printf("\nResultado:\n");
     else printf("\n");
     if (comZero == 0)
     {
        for (int i = 0; i < l.nroElem; i++)
            printf("%d-", l.A[i].chave);
     }
     else
     {
         for (int i = 0; i < l.nroElem; i++)
         {
             if (l.A[i].chave<10) printf("0%d-", l.A[i].chave);
             else printf("%d-", l.A[i].chave);
         }
     }
}


//Funcao do exercicio 02
int excluir(LISTA *l, int pos)
{
    for (int j = pos+1; j < l->nroElem; j++)
        l->A[j] = l->A[j+1];
    l->nroElem--;
    return (true);
}

//Funcao do exercicio 03
int comparar(LISTA l1, LISTA l2, int *pos)
{
    int i = 0;
    if (l1.nroElem < l2.nroElem) return -1;
    if (l1.nroElem > l2.nroElem) return -2;
    while (l1.A[i].chave == l2.A[i].chave)
    {
          i++;
    }
    if (l1.nroElem != i)
    {
       *pos = i;
       return -3;
    }
    else return 0;
}

//Funcao do exercicio 04
int inverter(LISTA *l, int i1, int i2)
{
    if (i1 >= 0 && i1 < i2 && i2 < l->nroElem)
    {
       int aux = 0;
       while (i1 < i2)
       {
             aux = l->A[i1].chave;
             l->A[i1].chave = l->A[i2].chave;
             l->A[i2].chave = aux;
             i1++;
             i2--;
       }
       return 0;
    }
    else
    {
        printf("Indices invalidos.");
        return -1;
    }
}

//Funcao do exercicio 05
LISTA somar(LISTA l1, LISTA l2)
{
      LISTA l3;
      if (l1.nroElem >= l2.nroElem) l3.nroElem = l1.nroElem;
      else l3.nroElem = l2.nroElem;
      
      for (int i = 0; i < l3.nroElem; i++)
      {
          if (i > l1.nroElem-1) l3.A[i].chave = l2.A[i].chave;
          else if (i > l2.nroElem-1) l3.A[i].chave = l1.A[i].chave;
          else l3.A[i].chave = l1.A[i].chave + l2.A[i].chave;
      }
      return l3;
}

//funcao do exercicio 06
void eliminarElem(LISTA *l, int chave)
{
      int i = 0;
      while (i < l->nroElem)
      {
            while (l->A[i].chave == chave && l->A[i+1].chave == chave)
            {
                  for (int j = i+1; j < l->nroElem; j++)
                      l->A[j].chave = l->A[j+1].chave;
                  l->nroElem--;
                  //excluir(&l, i);
            }
            i++;
      }
}

//Estrutura do exercicio 07
typedef struct
{
        REGISTRO elem[MAX];
        int quant;
        int nroElem;
} LISTA2;

//funcoes do exercicio 07
LISTA ordenar(LISTA *l, int begin, int end)
{
      if (end - begin > 0) 
      {
	     int aux;
	     int pivot = l->A[0].chave;
	     int left = begin + 1;
	     int right = end;
	     while (left < right) 
         {
               if (l->A[left].chave <= pivot) left++;
               else 
               {
                    aux = l->A[left].chave;
                    l->A[left].chave = l->A[right].chave;
			        l->A[right].chave;
                    right--;
               }
         }
	     if(l->A[left].chave > pivot) left--;
         aux = l->A[begin].chave;
	     l->A[begin].chave = l->A[left].chave;
	     l->A[left].chave = aux;
         //faz as chamadas recursivas
	     ordenar(l, begin, left-1);
	     ordenar(l, right, end);
     }
}

LISTA2 criar(LISTA l1) //para funcionar adequadamente precisa receber uma lista ordenada
{
      int i = 0;
      int j = 0;
      LISTA2 L2;
      while (i < l1.nroElem)
      {
            int qtd = 1; //quantidade de elementos
            if (l1.A[i].chave == l1.A[i+1].chave)
            {
               while (l1.A[i].chave = l1.A[i+1].chave)
               {
                     L2.elem[j].chave = l1.A[i].chave;
                     L2.quant = qtd++;
                     i++;
               }
            }
            else
            {
                L2.elem[j].chave = l1.A[i].chave;
                L2.quant = 1;
                i++;
            }
            j++;
      }
      L2.nroElem = j;
}

int main()
{
    srand(time(NULL));
    
    LISTA l;
    
    l.nroElem = rand()%26;
    
    for (int i = 0; i < l.nroElem; i++)
         l.A[i].chave = rand()%9;
       
    exibir(l, 0, 0);
    ordenar(&l, 0, l.nroElem);
    exibir(l, 1, 0);
    getch();
}
