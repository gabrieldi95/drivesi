# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h> //o booleano so funciona se incluir isso 
# include <conio.h>
# include <time.h>
# include <string.h>
# define MAX 50

typedef int TIPOCHAVE;

typedef struct
{
        TIPOCHAVE chave;
} REGISTRO;


typedef struct
{
        REGISTRO A[MAX];
        int nroElem;
} LISTA;

int main()
{
     LISTA l;
     int nro;
     printf("\nDigite o quantidade de  inicial: ");
     fflush(stdin);
     scanf("%d", &nro);
     fflush(stdin);
     printf("\nDigite o tamanho total do vetor: ");
     scanf("%d", &l.nroElem);
     for (int i = 0; i < nro; i++)
         l.A[i].chave = rand()%100;
     
     for (int i = nro; i < l.nroElem; i++)
         l.A[i].chave = 0;
     
     for (int i = 0; i < (l.nroElem-nro); i++)
         l.A[i+nro].chave = l.A[i].chave;
     
     printf("\nResultado: \n\n");
     for (int i = 0; i < l.nroElem; i++)
         printf("%d-", l.A[i].chave);
     
     getch();
}
