# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>
# include <conio.h>
# include <string.h>
# include <time.h>
# define MAX 50

typedef int TIPOCHAVE;

typedef struct
{
        TIPOCHAVE chave;
} REGISTRO;


typedef struct
{
        REGISTRO A[MAX];
        int nroElem;
} LISTA;

//Funcao util
void exibir(LISTA l, int opcao, int comZero)
{
     if (opcao == 0) printf("\nLista original:\n");
     else if (opcao == 1) printf("\nResultado:\n");
     else printf("\n");
     if (comZero == 0)
     {
        for (int i = 0; i < l.nroElem; i++)
            printf("%d-", l.A[i].chave);
     }
     else
     {
         for (int i = 0; i < l.nroElem; i++)
         {
             if (l.A[i].chave<10) printf("0%d-", l.A[i].chave);
             else printf("%d-", l.A[i].chave);
         }
     }
}


//Funcao do exercicio 02
int excluir(LISTA *l, int pos)
{
    for (int j = pos+1; j < l->nroElem; j++)
        l->A[j] = l->A[j+1];
    l->nroElem--;
    return (true);
}

//Funcao do exercicio 03
int comparar(LISTA l1, LISTA l2, int *pos)
{
    int i = 0;
    if (l1.nroElem < l2.nroElem) return -1;
    if (l1.nroElem > l2.nroElem) return -2;
    while (l1.A[i].chave == l2.A[i].chave)
    {
          i++;
    }
    if (l1.nroElem != i)
    {
       *pos = i;
       return -3;
    }
    else return 0;
}

//Funcao do exercicio 04
int inverter(LISTA *l, int i1, int i2)
{
    if (i1 >= 0 && i1 < i2 && i2 < l->nroElem)
    {
       int aux = 0;
       while (i1 < i2)
       {
             aux = l->A[i1].chave;
             l->A[i1].chave = l->A[i2].chave;
             l->A[i2].chave = aux;
             i1++;
             i2--;
       }
       return 0;
    }
    else
    {
        printf("Indices invalidos.");
        return -1;
    }
}

//Funcao do exercicio 05
LISTA somar(LISTA l1, LISTA l2)
{
      LISTA l3;
      if (l1.nroElem >= l2.nroElem) l3.nroElem = l1.nroElem;
      else l3.nroElem = l2.nroElem;
      
      for (int i = 0; i < l3.nroElem; i++)
      {
          if (i > l1.nroElem-1) l3.A[i].chave = l2.A[i].chave;
          else if (i > l2.nroElem-1) l3.A[i].chave = l1.A[i].chave;
          else l3.A[i].chave = l1.A[i].chave + l2.A[i].chave;
      }
      return l3;
}

//funcao do exercicio 06
void eliminarElem(LISTA *l, int chave)
{
      int i = 0;
      while (i < l->nroElem)
      {
            while (l->A[i].chave == chave && l->A[i+1].chave == chave)
            {
                  for (int j = i+1; j < l->nroElem; j++)
                      l->A[j].chave = l->A[j+1].chave;
                  l->nroElem--;
                  //excluir(&l, i);
            }
            i++;
      }
}

//recebe lista sequencial ordenada, valor de chave

int main()
{
    srand(time(NULL));
    
    LISTA l;
    
    l.nroElem = 20;
    int seq = 0;
    int j = 1;
    int i = 0;
    
    while(i < l.nroElem)
    {
         seq = rand()%5;
         if (i+seq > l.nroElem) seq = l.nroElem;
         else seq += i;
         for (i; i < seq; i++)
             l.A[i].chave = j;
         j++;
    }
       
    exibir(l, 0, 0);
    getch();
    
    int elem;
    printf("\nDigite o elemento a ser eliminado: ");
    fflush(stdin);
    scanf("%d", &elem);
    
    eliminarElem(&l, elem);
    
    exibir(l, 1, 0);
    getch();
}
