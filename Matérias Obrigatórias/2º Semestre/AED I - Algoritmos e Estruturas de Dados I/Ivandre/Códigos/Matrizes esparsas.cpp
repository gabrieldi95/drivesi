//Representa��o por lista encadeada simples

/*
typedef struct s{
        int lin;
        int col;
        char info/
        *s prox;
} NO;

void exibirLinhaN(LISTA *m, int n)
{
     int cont = 0;
     NO *p = m->inicio;
     
     while(p)
     {
             if(p->lin < n)
             {
                        p = p->prox;
                        continue;
             }
             if(p->lin == n)
             {
                        printf("%c", p->info);
                        p = p->prox;
                        cont++;
             }
     }
}
*/

// Como imprimir os zeros que faltam?
// Como exibir a diagonal principal?
// Como inserir um elemento (x,y) na posi��o certa da lista?
// Como copiar uma coluna i1 para i2?

/*******************************************************************************
Representa��o por listas cruzadas**********************************************/

typedef struct s
{
        int lin;
        int col;
        char info; //chave, valor, etc
        s* proxC; //para a direita
        s* proxL; //para baixo
} NO;

typedef struct
{
        NO* LIN[MAXLIN+1]
        NO* COL[MAXCOL+1]
} MATRIZ;

void inicializar (MATRIZ *m)
{
     int i;
     for (i=1; i <= MAXLIN; i++)
         m->lin[i] = NULL;
     for (i=1; i <= MAXCOL; i++)
         m->col[i] = NULL;
}

void exibirLinha (MATRIZ *m, int l)
{
     NO *p = m->LIN[l];
     while (p)
     {
           printf("%c", p->info)
           p = p->proxC;
     } 
}

int contarZeros (MATRIZ *m)
{
    int i;
    int cont = 0;
    for (i=1; 1<=MAXCOL; i++)
    {
        NO *p = m->col[i];
        while (p)
        {
              cont++;
              p=p->proxL;
        }
    }
    return (MAXLIN*MAXCOL-cont);
}

/* COMO SABER SE VALE A PENA A IMPLEMENTA��O
/  MAXLIN*sizeof(NO*)+MAXCOL*sizeof(NO*)+N*sizeof(NO*)
*/

// Exerc�cio: void exibirProdutoColunas(MATRIZ *m, int c1, int c2)
// Quando o resultado � zero?
// Resultado - lista de n�meros


/* Cuidados: nunca inserir chave 0, se o usu�rio fornecer "0" deve apagar o conte�do.
/  NO**linha, NO**coluna s�o as entradas das listas
/  NO* esq, NO* acima demandam busca
*/
void excluir (NO **linha, NO **coluna, NO* esq, NO* acima, NO* p)
{
     if (p) //se passar zero n�o faz nada
     {
        if (esq) esq->proxC = p->proxC;
        else *linha = p->proxC; //caso seja o 1� elemento
        if (acima) acima->proxL = p->proxL;
        else *coluna = p->proxL;
     }
     //2 inser��es em lista ligada simples
     free(p);
}

/*******************************************************************************
/ Representa��o por linhas
*******************************************************************************/
int valor (MATRIZ *m, int lin, int col)
{
    NO *p = m->inicio;
    while (p)
    {
          if ((p->lin == lin)&&(p=>col == col)) return (p->info);
          if (p->lin > lin) return(0);
          p = p->prox;
    }
}
