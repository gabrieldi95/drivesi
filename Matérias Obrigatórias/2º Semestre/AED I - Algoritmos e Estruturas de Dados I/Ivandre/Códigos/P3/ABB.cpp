#include <stdlib.h>
#include <stdio.h>

typedef struct estrutura {
	int chave;
	estrutura *esq;
	estrutura *dir;
} NO;

typedef struct estruture {
    NO* no;
    estruture *prox;
} NOF;

typedef struct {
    NOF* inicio;
    NOF* fim;
} FILA;

//-----------------------------------------------------------------------------------------------------------------------------------

//FILAS

// Inicializa��o da fila dinamica
void inicializarFila(FILA *f) {
    f->inicio = NULL;
    f->fim = NULL;
}

// inserir item ao final da fila dinamica
void entrarFila(NO* ch, FILA *f) {
    NOF* novo;
    novo = (NOF*) malloc(sizeof(NOF));
    novo->no = ch;
    novo->prox = NULL;
    if(f->fim) f->fim->prox = novo; // fila n�o � vazia
    else f->inicio = novo; // 1a. inser��o em fila vazia
    f->fim = novo;
}

// retirar a chave da frente ou -1
NO* sairFila(FILA *f) {
    NOF* aux;
    NO* ch;
    if(!f->inicio) return(NULL);
    ch = f->inicio->no;
    aux = f->inicio;
    f->inicio = f->inicio->prox;
    free(aux);
    if(!f->inicio) f->fim = NULL; // fila ficou vazia
    return(ch);
}

// quantos elementos existem
int tamanhoFila(FILA *f) {
    NOF* p;
    int tam = 0;
    p = f->inicio;
    while (p) {
        tam++;
        p = p->prox;
    }
    return(tam);
}

//-----------------------------------------------------------------------------------------------------------------------------------


// Busca bin�ria n�o recursiva devolvendo o n� pai
NO* buscaNo(NO* raiz, int ch, NO* *pai) {
	NO* atual = raiz;
	*pai = NULL;
	while (atual) {
		if(atual->chave == ch) return(atual);
		*pai = atual;
		if(ch < atual->chave) atual = atual->esq;
		else atual = atual->dir;
	}
	return(NULL);
}

// Inicializacao - sentinela
void inicializar(NO* *raiz, NO* sentinela) {
	*raiz = sentinela;
}

// Testa se �rvore com sentinela � vazia
bool vazia(NO* raiz, NO* sentinela) {
	if (raiz == sentinela) return(true);
	else return(false);
}

// Busca bin�ria com sentinela devolvendo o n� pai
NO* buscaComSentinela(NO* raiz, NO* sentinela, int ch, NO* *pai) {
	NO* atual = raiz;
	*pai = NULL;
	sentinela->chave = ch;
	while (atual->chave != ch) {
		*pai = atual;
		if(ch < atual->chave) atual = atual->esq;
		else atual = atual->dir;
	}
	if(atual == sentinela) return(NULL);
	else return(atual);
}

//Inserir
bool inserir(NO* *raiz, int ch){
	NO* pai;
	NO* aux = buscaNo(*raiz, ch, &pai);
	if(aux) return false;
	NO* novo = (NO*) malloc(sizeof(NO));
	novo->chave = ch;
	novo->esq = NULL;
	novo->dir = NULL;
	if(!pai){
		*raiz = novo;
		return true;
	}
	else{
		if(ch < pai->chave) pai->esq = novo;
		else pai->dir = novo;
		return true;
	}
}

//Altura de um n�
int altura(NO* p){
	if(!p) return 0;
	else{
		int he = altura(p->esq);
		int hd = altura(p->dir);
		if(he > hd) return (1+he);
		else return (1+hd);
	}
}

//Exibir arvore em formato parentisado
void parent(NO* p){
	if(p){
		printf("%d ", p->chave);
		if(p->esq || p->dir){
			printf(" ( ");
			parent(p->esq);
			printf(" , ");
			parent(p->dir);
			printf(" ) ");
		}
	}
}

//Criar ABB a partir de um vetor
void vetorABB(NO* *raiz, int ini, int fim, int a[]){
	if(ini <= fim){
		int i = ini + (fim - ini) / 2;
		inserir( raiz, a[i]);
		vetorABB( raiz, ini, i-1, a);
		vetorABB( raiz, i+1, fim, a);
	}
}

//Copiar Arvore
NO* copiar(NO* p){
	NO* resp = NULL;
	if(p){
		resp = (NO*) malloc(sizeof(NO));
		resp->chave = p->chave;
		resp->esq = copiar(p->esq);
		resp->esq = copiar(p->dir);
	}
	return resp;
}

//Retornar ocorrencia mais proxima da raiz de uma chave ch
NO* maisProxRaiz (NO* p, int ch){
	FILA f;
	inicializarFila(&f);
	if(p){
		entrarFila( p, &f);
		while(tamanhoFila(&f) > 0){
			p = sairFila(&f);
			if(p->chave == ch){
				//if(p) sairFila(&f);
				while(tamanhoFila(&f) > 0) sairFila(&f);
				return p;
			}
			if(p->esq) entrarFila( p->esq, &f);
			if(p->dir) entrarFila( p->dir, &f );
		}
		return NULL;
	}
}

//Achar menor chave em ABB
NO* menor(NO* p){
	if(!p) return NULL;
	while(p->esq) p = p->esq;
	return p;
}

//Exibi��o horizontal
void horiz(NO* p, int i){
	if(p){
		horiz(p->dir, i+1);
		int j;
		for(j = 1; j <= i; j++) printf("--- ");
		printf("%d \n", p->chave);
		horiz( p->esq, i+1);
	}
}

//Achar antecessor
void antecessor (NO* p, int ch, NO* *ant, bool* achou){
	if(p){
		antecessor(p->esq, ch, ant, achou);
		if(*achou) return;
		if(p->chave == ch){
			*achou = true;
			return;
		}
		*ant = p;
		antecessor( p->dir, ch, ant, achou);
	}
}

/*
Exercicios:
	1. Contar quantidade de elementos de um nivel
	2. Trocar as chaves de todos os elementos com as chaves de seus filhos esquerdos, se houver
	3. Contar a quantidade de ancetrais de um no
	4. Quantidade de descendentes
*/

void contando(NO* p, int niv, int ch, int *e){
	if(p){
		niv += 1;
		if(niv == ch) {
			 niv -= 1;	
			 *e += 1;
			 return ;
		}
		else {
			contando(p->esq, niv, ch, e);
			contando(p->dir, niv, ch, e);
		}
	}
}
int elemNivel(NO* raiz, int ch){
	int e = 0;
	contando(raiz, 0, ch, &e);
	return e;
}

int main(){
	NO* n1 = (NO*) malloc(sizeof(NO));
	NO* n2 = (NO*) malloc(sizeof(NO));
	NO* n3 = (NO*) malloc(sizeof(NO));
	NO* n4 = (NO*) malloc(sizeof(NO));
	NO* n5 = (NO*) malloc(sizeof(NO));
	NO* n6 = (NO*) malloc(sizeof(NO));
	NO* n7 = (NO*) malloc(sizeof(NO));
	NO* n8 = (NO*) malloc(sizeof(NO));
	NO* n9 = (NO*) malloc(sizeof(NO));
//	NO* n10 = (NO*) malloc(sizeof(NO));
	NO* n11 = (NO*) malloc(sizeof(NO));
	NO* n12 = (NO*) malloc(sizeof(NO));
	NO* n13 = (NO*) malloc(sizeof(NO));
	NO* n14 = (NO*) malloc(sizeof(NO));
	NO* n15 = (NO*) malloc(sizeof(NO));

		
	n1->chave = 60;
	n2->chave = 40;
	n3->chave = 80;
	n4->chave = 30;
	n5->chave = 50;
	n6->chave = 70;
	n7->chave = 90;
	n8->chave = 20;
	n9->chave = 35;
//	n10->chave = 45;
	n11->chave = 55;
	n12->chave = 65;
	n13->chave = 75;
	n14->chave = 85;	
	n15->chave = 95;
/*
    n1->esq = n2;
	n1->dir = n3;
	n2->esq = NULL;
	n2->dir = NULL;
	n3->esq = NULL;
	n3->dir = NULL;
/*	*/
	n1->esq = n2;
	n1->dir = n3;
	n2->esq = n4;
	n2->dir = n5;
	n3->esq = n6;
	n3->dir = n7;
	n4->esq = n8;
	n4->dir = n9;
	n5->esq = NULL;
//	n5->esq = n10;
	n5->dir = n11;
	n6->esq = n12;
	n6->dir = n13;
	n7->esq = n14;
	n7->dir = n15;
	n8->esq = NULL;
	n8->dir = NULL;
	n9->esq = NULL;
	n9->dir = NULL;
//	n10->esq = NULL;
//	n10->dir = NULL;
	n11->esq = NULL;
	n11->dir = NULL;
	n12->esq = NULL;
	n12->dir = NULL;
	n13->esq = NULL;
	n13->dir = NULL;
	n14->esq = NULL;
	n14->dir = NULL;
	n15->esq = NULL;
	n15->dir = NULL;

	NO* raiz = n1; 
	NO* x = (NO*) malloc(sizeof(NO));
	x=NULL;
	
	printf("%s \n", inserir(&raiz, 75)? "-- Inseriu --":"-- False --");
	horiz(raiz,0);
	printf("%d \n", altura(raiz));
	
/*	Descobrir como tornar viavel a visualiza�ao dos dados
	int a[2047];
	int i;
	for(i=0; i < 2047; i++){
		a[i] = i;
	}
	vetorABB(&x, 0, 2046, a);
	horiz(x, 0);
	printf("%d \n", altura(x));*/
	printf("%d \n", elemNivel(raiz, 4));
	printf("\n");
	//excluirNo(&raiz, 40);
	printf("\n");
	horiz(raiz,0);
	
	system("Pause");
	return 0;
}
