#include <stdlib.h>
#include <stdio.h>

/*
 
 --> O n�mero m�ximo de n�s poss�veis no n�vel i � 2^(i-1)

 --> Uma �rvore bin�ria de altura h tem no m�ximo 2^h � 1 n�s
 
 --> A altura m�nima de uma �rvore bin�ria com n > 0 n�s � 1 + ch�o( log 2 n )
 
 //ESTATICA
 - pai(i) = ch�o (i / 2) se i == 0, n�o h� pai
 - filho_esq(i) = 2 * i se i > n, n�o h� filho esquerdo
 - filho_dir(i) = (2 * i) + 1 se i > n, n�o h� filho direito
 
 //DINAMICA
 - Pr�-ordem: visita a raiz, esquerda e direita.
 - Em ordem: esquerda, visita a raiz e direita.
 - P�s-ordem: esquerda, direita e visita raiz.

*/

//DINAMICA

typedef struct estrutura {
	int chave;
	estrutura *esq;
	estrutura *dir;
} NO;

typedef struct estrutur {
	NO* no;
	estrutur *prox;
} NOP;

typedef struct {
	NOP* topo;
} PILHA;

typedef struct estruture {
    NO* no;
    estruture *prox;
} NOF;

typedef struct {
    NOF* inicio;
    NOF* fim;
} FILA;

//-----------------------------------------------------------------------------------------------------------------------------------

//PILHAS E FILAS

// Inicializa��o da pilha din�mica
void inicializarPilha(PILHA *p) {
	p->topo = NULL;
}

// Quantos elementos existem
int tamanhoPilha(PILHA *p) {
	NOP* p1 = p->topo;
	int tam = 0;
	while(p1) {
		tam++;
		p1 = p1->prox;
	}
	return(tam);
}

// Inserir item no topo
void push(NO* ch, PILHA *p) {
	NOP* novo = (NOP*) malloc(sizeof(NOP));
	novo->no = ch;
	novo->prox = p->topo;
	p->topo = novo;
}

// Retirar a chave do topo ou -1
NO* pop(PILHA *p) {
	NOP* aux;
	NO* ch;
	if(!p->topo) return(NULL);
	aux = p->topo;
	ch = aux->no;
	p->topo = p->topo->prox;
	free(aux);
	return(ch);
}

// Inicializa��o da fila dinamica
void inicializarFila(FILA *f) {
    f->inicio = NULL;
    f->fim = NULL;
}

// inserir item ao final da fila dinamica
void entrarFila(NO* ch, FILA *f) {
    NOF* novo;
    novo = (NOF*) malloc(sizeof(NOF));
    novo->no = ch;
    novo->prox = NULL;
    if(f->fim) f->fim->prox = novo; // fila n�o � vazia
    else f->inicio = novo; // 1a. inser��o em fila vazia
    f->fim = novo;
}

// retirar a chave da frente ou -1
NO* sairFila(FILA *f) {
    NOF* aux;
    NO* ch;
    if(!f->inicio) return(NULL);
    ch = f->inicio->no;
    aux = f->inicio;
    f->inicio = f->inicio->prox;
    free(aux);
    if(!f->inicio) f->fim = NULL; // fila ficou vazia
    return(ch);
}

//-----------------------------------------------------------------------------------------------------------------------------------

// Inicializa��o da �rvore vazia
void inicializarArvore(NO* *raiz) {
	*raiz = NULL;
}

// Verificar se �rvore � vazia
bool arvoreVazia(NO* raiz) {
	if(!raiz) return(true);
	else return(false);
}

// Inser��o de um n� em �rvore comum (sem ordem)
bool inserirNo(NO* *raiz, NO* pai, int ch, int pos) {
	NO* novo;
	if(pai) {
		if( ((pos==1) && (pai->esq!=NULL))|| ((pos!=1) && (pai->dir!=NULL))) {
			return(false);
		}
	}
	novo = (NO *) malloc(sizeof(NO));
	novo->chave = ch;
	novo->esq = NULL;
	novo->dir = NULL;
	if(!pai) *raiz = novo;
	else {
		if(pos==1) pai->esq = novo;
		else pai->dir = novo;
	}
	return true;
}

// Percurso pr�-ordem n�o-recursivo
void preOrdemNaoRecurs(NO* p) {
	PILHA pi;
	inicializarPilha(&pi); // deve ser uma pilha de ponteiros de n�s da �rvore
	while (true) {
		while (p) {
			printf("%d ", p->chave);
			if(p->dir) push(p, &pi); // memoriza caminho � direita
			p = p->esq;
		}
		if(tamanhoPilha(&pi) > 0) {
			p = pop(&pi);
			p = p->dir;
		}
		else break;
	}
}

void preOrdem(NO* p){
	if(p) {
		printf("%d ", p->chave);
		preOrdem(p->esq);
		preOrdem(p->dir);
	}
}

void emOrdem(NO* p){
	if(p) {
		emOrdem(p->esq);
		printf("%d ", p->chave);
		emOrdem(p->dir);
	}
}
	
void posOrdem(NO* p){
	if(p) {
		posOrdem(p->esq);
		posOrdem(p->dir);
		printf("%d ", p->chave);
	}
}

// Percorre a �rvore em n�vel e exibe
void exibirArvoreEmNivel(NO* raiz){
	FILA f;
	NO* p = raiz;
	inicializarFila(&f);
	while( (p) || (f.inicio) ) {
		if(p->esq) entrarFila(p->esq, &f);
		if(p->dir) entrarFila(p->dir, &f);
		printf("%d ", p->chave);
		p = NULL;
		if(f.inicio) p = sairFila(&f);
	}
}

// Retorna o n�vel de uma chave (que deve ser encontrada)
void travessia(NO* p, int *niv, int ch, bool *achou){
	if(p) {
		*niv = *niv + 1;
		if(p->chave == ch) *achou = true;
		else {
			travessia(p->esq, niv, ch, achou);
			if((!*achou)) travessia(p->dir, niv, ch, achou);
			if((!achou)) *niv = *niv - 1;
		}
	}
}

int nivel (NO* raiz, int ch) {
	int n = 0;
	bool achou = false;
	travessia(raiz, &n, ch, &achou);
	return(n);
}

// Retorna o endere�o do pai da chave procurada
void travessiap(NO* p, int ch, NO* *no_pai, bool *achou) {
	if(p) {
		if(p->esq)
			if(p->esq->chave == ch) {
				*achou = true;
				*no_pai = p;
			}
		if(!*achou)
			if(p->dir)
				if(p->dir->chave == ch) {
					*achou = true;
					*no_pai = p;
				}
		if(!*achou) travessiap(p->esq, ch, no_pai, achou);
		if(!*achou) travessiap(p->dir, ch, no_pai, achou);
	}
}

NO* pai(NO* raiz, int ch) {
	bool achou = false;
	NO* no_pai;
	if(raiz) {
		if(raiz->chave == ch) return(NULL);
		else {
			travessiap(raiz, ch, &no_pai, &achou);
			return(no_pai);
		}
	}
}

// Procura a chave na arvore inteira usando pr�-ordem
NO* busca_local(NO* p, int ch, bool *achou) {
	NO* aux;
	*achou = false;
	if(!p) return(NULL);
	if(p->chave == ch) {
		*achou = true;
		return(p);
	}
	aux = busca_local(p->esq, ch, achou);
	if(*achou) return(aux);
	else return(busca_local(p->dir, ch, achou));
}

NO* buscaCompleta(NO* raiz, int ch) {
	bool achou;
	return(busca_local(raiz, ch, &achou));
}

// Destruir uma �rvore usando p�s-ordem
void destruirArvore(NO* *p) {
	if(*p) {
		destruirArvore(&(*p)->esq);
		destruirArvore(&(*p)->dir);
		free(*p);
	}
	*p = NULL; // evita que a raiz aponte para endere�o inv�lido
}

//Contar elementos da arvore
int contar(NO* p){
	int resp = 0;
	if(p){
		resp++;
		resp += contar(p->esq);
		resp += contar(p->dir);
	}
	return resp;
}

//Exibir arvore em formato parentisado
void parent(NO* p){
	if(p){
		printf("%d ", p->chave);
		if(p->esq || p->dir){
			printf(" ( ");
			parent(p->esq);
			printf(" , ");
			parent(p->dir);
			printf(" ) ");
		}
	}
}

//Exibi��o horizontal
void horiz(NO* p, int i){
	if(p){
		horiz(p->dir, i+1);
		int j;
		for(j = 1; j <= i; j++) printf("--- ");
		printf("%d \n", p->chave);
		horiz( p->esq, i+1);
	}
}

int main(){
	
	NO* n1 = (NO*) malloc(sizeof(NO));
	NO* n2 = (NO*) malloc(sizeof(NO));
	NO* n3 = (NO*) malloc(sizeof(NO));
	NO* n4 = (NO*) malloc(sizeof(NO));
	NO* n5 = (NO*) malloc(sizeof(NO));
	NO* n6 = (NO*) malloc(sizeof(NO));
//	NO* n7 = (NO*) malloc(sizeof(NO));
	
	n1->chave = 1;
	n2->chave = 2;
	n3->chave = 3;
	n4->chave = 4;
	n5->chave = 5;
	n6->chave = 6;
//	n7->chave = 7;
	
	n1->esq = n2;
	n1->dir = n3;
	n2->esq = n4;
	n2->dir = n5;
	n3->esq = n6;
	n3->dir = NULL;
//	n3->dir = n7;
	n4->esq = NULL;
	n4->dir = NULL;
	n5->esq = NULL;
	n5->dir = NULL;
	n6->esq = NULL;
	n6->dir = NULL;
//	n7->esq = NULL;
//	n7->dir = NULL;
	
	NO* raiz = n1; 
	
	preOrdem(raiz);
	printf("  Pre Ordem");
	printf("\n");
	emOrdem(raiz);
	printf("  Em Ordem");
	printf("\n");
	posOrdem(raiz);
	printf("  Pos Ordem");
	printf("\n");
	

	NO* aux = buscaCompleta(raiz, 3);
	printf("%s", inserirNo(&raiz, aux, 10, 2)? "-- Inseriu --":"-- FALSE --");
    printf("\n");
	preOrdem(raiz);
	printf("  Pre Ordem Recursivo");
	printf("\n");
	preOrdemNaoRecurs(raiz);
	printf("  Pre Ordem Nao Recursivo");
	printf("\n");
	exibirArvoreEmNivel(raiz);
	printf("  Em nivel Apostila");
	printf("\n");
	printf("%d ERRO", nivel(raiz, 5 ));
	printf("\n");
	horiz(raiz,0);
	printf("# de elem %d \n", contar(raiz));
	
	system("Pause");
	return 0;
}
