class Aula10 {

	static final int ALVENARIA = 0;
	static final int VINIL = 1;
	static final int FIBRA = 2;
	static final int PLASTICO = 3;
	static double[] precos = {1500, 1100, 750, 500};

	static char[] nAlvenaria = {'A','l','v','e','n','a','r','i','a'};
	static char[] nVinil = {'V','i','n','i','l'};
	static char[] nFibra = {'F','i','b','r','a'};
	static char[] nPlastico = {'P','l','á','s','t','i','c','o'};

	static double valorPiscina(double area, int material) {
		if (material<ALVENARIA || material>PLASTICO || area<0) return(-1);
		
		return(area*precos[material]);
	}


	static boolean minuscula(char c) {
		return(c >= 'a' && c <= 'z');
	}

	static char paraMin(char c) {
		int aux;
		if (c >= 'A' && c <= 'Z') {
			aux = c - 'A' + 'a';
			return((char)aux);
		}
		return(c);
	}


	public static void main(String[] args) {
		char c = 'ö';
		char x = '\u00F6';
		int y = 246;

		System.out.println(c);
		System.out.println(x);
		System.out.println((char)y);
		
		for (int i = 32; i <= 126; i++) {
            System.out.println(i + " : " + (char)i);
        }
        
        for (char i = 32; i <= 126; i++) {
            System.out.println((int)i + " : " + i);
        }
        
        System.out.println(minuscula('A'));
        System.out.println(paraMin('a'));
        
        System.out.print("Piscina de ");
        System.out.print(nFibra);
        System.out.println(": "+ valorPiscina(100,FIBRA)+" (100m2)");
	}
}