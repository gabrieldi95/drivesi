import lista.ListaDepositos;
import depositos.Deposito;
import lista.No;
/*
	VOC� PODE IMPORTAR AS CLASSES QUE DESEJAR AQUI
*/

class Juros {
	
	/*
		VOC� PODE INCLUIR OS M�TODOS E ATRIBUTOS QUE QUISER, DESDE QUE OS JUROS SEJAM CALCULADOS COM UMA �NICA CHAMADA A "juros".
	*/
	
	/**
		Calcula os juros pagos, a partir de uma seq��ncia de dep�sitos e um saldo final, com a precis�o indicada. Usa o m�todo de Newton-Raphson para tal.
		
		@param depositos Lista de dep�sitos (n�o incluindo o saldo final)
		@param saldo Saldo final da aplica��o
		@param epsilon Precis�o do c�lculo (0 < epsilon < 1)
		
		@return Valor dos juros, ou NAN em caso de erro (epsilon fora dos limites, lista de dep�sitos sem dep�sitos, saldo null)
	*/
	public static double juros(ListaDepositos depositos, Deposito saldo, double epsilon) {
		
		//Confere o epsilon, o saldo e a lista
		if(epsilon > 0 || epsilon < 1 || saldo != null || depositos.elementos() != 0){ 
		
		//Declara��o das vari�veis locais
		double flinha = 0;
		double f = 0;
		double jk = 0.5;
		double j = 0;
		No no;
		
		//M�todo de Newton-Raphson que tem como objetivo calcular o 0 das fun��es
		do{
		flinha = 0;
			no = depositos.getInicio();
			while(no != null){
				flinha = flinha + saldo.getData().mesesEntre(no.getDeposito().getData())*no.getDeposito().getValor()*Math.pow((1+jk),(saldo.getData().mesesEntre(no.getDeposito().getData())-1));
				no = no.getProx();
			}
		
			f = 0;
			no = depositos.getInicio();
			while(no != null){
				f = f + no.getDeposito().getValor()*Math.pow((1+jk),saldo.getData().mesesEntre(no.getDeposito().getData()));
				no = no.getProx();
			}
			f = f - saldo.getValor();
			
			j = jk - f / flinha;
			
			if(Math.abs(j - jk) < epsilon){
				break;
			}else{
				jk = j;
			}
		}while(true);
		
		return j;
		
		}else{
			return Double.NaN;
		}
		
	}
	
	public static void main(String[] args) {
		// para seus testes
	}
}

