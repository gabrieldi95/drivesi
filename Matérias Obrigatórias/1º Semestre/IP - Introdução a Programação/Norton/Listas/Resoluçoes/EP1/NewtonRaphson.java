/*********************************************************************/
/** ACH2001 - Introdu��o � Ci�ncia da Computa��o I **/
/** EACH-USP - Primeiro Semestre de 2011 **/
/** 42 - Nakano **/
/** **/
/** Primeiro Exerc�cio-Programa **/
/** Arquivo: NewtonRaphson.java **/
/** **/
/** Vin�cius Schiavetto Linhares 7972032 **/
/** **/
/** 15/04/2012 **/
/*********************************************************************/

public class NewtonRaphson{
	public static void main(String[] args){
		System.out.println(newton(0.001));
	}
	
	
	//Declarar e inicializar vari�veis globais
		static double D_0 = 0;
		static double D_1 = 0;
		static double D_2 = 0;
		static double D_3 = 0;
		static double D_4 = 0;
		static double D_5 = 0;
		static double D_6 = 0;
		static double D_7 = 0;
		static double D_8 = 0;
		static double D_9 = 0;
		static double Saldo = 0;
		static int t_0 = 0;
		static int t_1 = 0;
		static int t_2 = 0;
		static int t_3 = 0;
		static int t_4 = 0;
		static int t_5 = 0;
		static int t_6 = 0;
		static int t_7 = 0;
		static int t_8 = 0;
		static int t_9 = 0;
		static int t_S = 0;
		
		
		
	static double newton(double epsilon){
		//Verifica se o par�metro est� v�lido, se n�o estiver retorna -1
		if (epsilon < 0 || epsilon > 1) return -1;
		
		//Declara��o das vari�veis locais
		double jk = 0.5;
		double j;
		double func;
		double derivada;
		
		//M�todo de Newton-Raphson para calcular 0 de fun��es
		while(true){
			func = (D_0*Math.pow((1+jk),(t_S-t_0)) + D_1*Math.pow((1+jk),(t_S-t_1)) + D_2*Math.pow((1+jk),(t_S-t_2)) + D_3*Math.pow((1+jk),(t_S-t_3)) + D_4*Math.pow((1+jk),(t_S-t_4)) + D_5*Math.pow((1+jk),(t_S-t_5)) + D_6*Math.pow((1+jk),(t_S-t_6)) + D_7*Math.pow((1+jk),(t_S-t_7)) + D_8*Math.pow((1+jk),(t_S-t_8)) + D_9*Math.pow((1+jk),(t_S-t_9))	- Saldo);
			derivada = ((t_S-t_0)*D_0*Math.pow((1+jk),(t_S-t_0-1)) + (t_S-t_1)*D_1*Math.pow((1+jk),(t_S-t_1-1)) + (t_S-t_2)*D_2*Math.pow((1+jk),(t_S-t_2-1)) + (t_S-t_3)*D_3*Math.pow((1+jk),(t_S-t_3-1)) + (t_S-t_4)*D_4*Math.pow((1+jk),(t_S-t_4-1)) + (t_S-t_5)*D_5*Math.pow((1+jk),(t_S-t_5-1)) + (t_S-t_6)*D_6*Math.pow((1+jk),(t_S-t_6-1)) + (t_S-t_7)*D_7*Math.pow((1+jk),(t_S-t_7-1)) + (t_S-t_8)*D_8*Math.pow((1+jk),(t_S-t_8-1)) + (t_S-t_9)*D_9*Math.pow((1+jk),(t_S-t_9-1)));
			j = jk - func / derivada;
			
			if(Math.abs(j - jk) < epsilon){
				break;
			}
			else{
				jk = j;
			}
		}
		
		//Sa�da
		return j;
	}
}

