class AdrianaCamargo extends Pato implements Jogador {
	int[] CartasNaMao=new int[19]; // certamente n�o passa de 2 cartas na mesa e 19 na m�o sem estourar.
	int NCartas=0;
	public int fazAposta() {
		NCartas=0;   // fazer aposta equivale a iniciar novo jogo.
		setDinheiro(getDinheiro()-1); // d� para trapacear na hora de apostar ou receber.
		return 1;
	}
	public int[] mostraCartas() {  // um jogador pode trapacear, trocando cartas
		int[] R = new int[NCartas];
		for (int i=0;i<NCartas;i++) R[i]=CartasNaMao[i];
		return R;
	}
	public int recebeGanhos(int Ganhos) {
		setDinheiro(getDinheiro()+Ganhos);
		return 1;
	}
	public int joga() {
		if (calculaPontos()<16) return 1;
		return 0;
	}
	public int recebeCarta (int Carta) {
		CartasNaMao[NCartas]=Carta;
		NCartas++;
		setDinheiro(getDinheiro()-1);
		return 1;
	}
	int calculaPontos () {  // o certo seria ter uma classe que cont�m as regras do jogo, e um m�todo �nico para calcular os pontos.
		int Pontos=0;
		int[] CartasAbertas=Mesa.getCartas(this);  // ainda bem que tem o this - � uma certa forma de reflex�o.
		if (CartasAbertas!=null)
			for (int i=0;i<CartasAbertas.length;i++) Pontos+=CartasAbertas[i];
		if (NCartas>0)
			for (int i=0;i<NCartas;i++) Pontos+=CartasNaMao[i];
		return Pontos;
	}
	public int getNCartas() {
		return NCartas;
	}
}
