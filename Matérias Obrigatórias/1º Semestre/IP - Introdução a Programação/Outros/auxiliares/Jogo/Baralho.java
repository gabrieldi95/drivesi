import java.util.Random;

class Baralho {
	private int NCARTAS=26;
	/** cte que indica que nao ha mais cartas no baralho. */
	public static final int VAZIO=0;   
    protected Random RNG=new Random();
	/** array que contem todas as cartas que ainda nao sairam. */
	protected int[] Cartas;
	/** Quantidade de cartas restantes no baralho */
	int CartasRestantes;
	Baralho () {
		this.troca();
	}
	/** sorteia uma carta do baralho, removendo a carta. */
	int getCarta () {
		if (CartasRestantes<=0) return VAZIO;
		int i= (int) Math.abs(RNG.nextInt()%CartasRestantes);
		int Sorteada=Cartas[i];
		Cartas[i]=Cartas[--CartasRestantes];
		return Sorteada;
	}
	void imprimeCartasRestantes() {
		for (int i=0;i<CartasRestantes;i++)
			System.out.println (Cartas[i]);
	}
	/** troca o baralho */
	void troca () {
		Cartas=new int[NCARTAS];
		for (int i=0;i<NCARTAS;i++) {
			Cartas[i]=i%13+1;
			if (Cartas[i]>10) Cartas[i]=1;   // as letras
		}
		CartasRestantes=NCARTAS;
	}
	int getCartasRestantes () {
		return CartasRestantes;
	}
}
