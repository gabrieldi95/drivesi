interface Jogador {
	int fazAposta();
	int[] mostraCartas();  // um jogador pode trapacear, trocando cartas
	int recebeGanhos(int Ganhos);
	int recebeCarta (int Carta);
	int joga();
	void imprimeQuantoTem ();
	int getNCartas();
	// em uma interface todos os m�todos s�o p�blicos.
}
