class Mesa {
	protected static ListaLigada Cartas=new ListaLigada();    // cartas de cada jogador
	protected static int ApostaTotal;
	protected static int[] Lixo=new int[0];
	public static void insereJogador (Jogador J) {
		Cartas.ins (J, null);  // insere sem cartas
	}
	public static int[] getCartas(Jogador J) {
	// retorna copias das cartas. retornar a referencia para a estrutura interna permitiria "trocar cartas".
		int [] C = (int[]) Cartas.rcl(J);
		int[] R=new int[C.length];
		for (int i=0;i<C.length;i++) {
			R[i]=C[i];
		}
		return R;
	}
	public static void setCartas(Jogador J, int[] NovasCartas) {
	// copia as cartas (para evitar trapa�as)
		int[] R=new int[NovasCartas.length];
		for (int i=0;i<NovasCartas.length;i++) {
			R[i]=NovasCartas[i];
		}
		Cartas.sto(J, R);
	}
	public static int getApostaTotal () {
		return (ApostaTotal);
	}
	public static void setApostaTotal (int ApostaTotal) {
		Mesa.ApostaTotal=ApostaTotal;
	}
	public static void imprime () {
		System.out.println ("Total Apostado:" + ApostaTotal);
		Jogador J;
		int[] C;
		J= (Jogador) Cartas.first();
		// Da cartas iniciais
		while (J!=null) {
			C= (int[]) Cartas.rcl(J);
			System.out.print ("Jogador: " + J + " " + ((Pato)J).getDinheiro() + " Mesa: ");
			for (int i=0;i<C.length;i++) {
				System.out.print (C[i]+" ");
			}
			System.out.println("Quantidade na mao: " + J.getNCartas());
			J=(Jogador)Cartas.next(J);
		}
		System.out.print ("Lixo: ");
		for (int i=0;i<Lixo.length;i++) {
			System.out.print (Lixo[i] + " ");
		}
		System.out.println();
	}
	public static void poeNoLixo (int[] Descarte) {
		int [] NovoLixo=new int [Lixo.length+Descarte.length];
		for (int i=0;i<Lixo.length;i++)
			NovoLixo[i]=Lixo[i];
		for (int i=0;i<Descarte.length;i++)
			NovoLixo[i+Lixo.length]=Descarte[i];
		Lixo=NovoLixo;
	}
	public static int[] mostraLixo () {
		int [] RLixo=new int [Lixo.length];
		for (int i=0;i<Lixo.length;i++)
			RLixo[i]=Lixo[i];
		return RLixo;
	}
	public static void limpaLixo () {
		Lixo=new int[0];
	}
}
