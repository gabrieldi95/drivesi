import java.util.Scanner;

class Manual extends Pato implements Jogador {
	int[] CartasNaMao=new int[19]; // certamente n�o passa de 2 cartas na mesa e 19 na m�o sem estourar.
	int NCartas=0;
	public int fazAposta() {
		NCartas=0;   // fazer aposta equivale a iniciar novo jogo.
		setDinheiro(getDinheiro()-1); // d� para trapacear na hora de apostar ou receber.
		return 1;
	}
	public int[] mostraCartas() {  // um jogador pode trapacear, trocando cartas
		int[] R = new int[NCartas];
		for (int i=0;i<NCartas;i++) R[i]=CartasNaMao[i];
		return R;
	}
	public int recebeGanhos(int Ganhos) {
		setDinheiro(getDinheiro()+Ganhos);
		return 1;
	}
	public int joga() {
		System.out.print ("Meus Pontos: " + calculaPontos() + " Minha mao: ");
		for (int i=0;i<NCartas;i++) {
			System.out.print (CartasNaMao[i] + " ");
		}
		System.out.println ();
		Mesa.imprime();
		Scanner Key = new Scanner (System.in);
		if (Key.next().equals("s")) return 1;  // pergunta se quer comprar ou n�o
		return 0;
	}
	public int recebeCarta (int Carta) {
		CartasNaMao[NCartas]=Carta;
		NCartas++;
		setDinheiro(getDinheiro()-1);
		return 1;
	}
	int calculaPontos () {  // o certo seria ter uma classe que cont�m as regras do jogo, e um m�todo �nico para calcular os pontos.
		int Pontos=0;
		int[] CartasAbertas=Mesa.getCartas(this);  // ainda bem que tem o this - � uma certa forma de reflex�o.
		if (CartasAbertas!=null)
			for (int i=0;i<CartasAbertas.length;i++) Pontos+=CartasAbertas[i];
		if (NCartas>0)
			for (int i=0;i<NCartas;i++) Pontos+=CartasNaMao[i];
		return Pontos;
	}
	public int getNCartas() {
		return NCartas;
	}
}
