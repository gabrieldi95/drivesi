class Croupier extends Mesa {
	Baralho B;
	Croupier (Baralho B) {
		this.B=B;
	}
	void troca () {
		B.troca();
		Mesa.limpaLixo ();
	}
	int calculaPontos (int [] CartasAbertas, int [] CartasFechadas) {
		int Pontos=0;
		if (CartasAbertas!=null)
			for (int i=0;i<CartasAbertas.length;i++) Pontos+=CartasAbertas[i];
		if (CartasFechadas!=null)
			for (int i=0;i<CartasFechadas.length;i++) Pontos+=CartasFechadas[i];
		if (Pontos>21) Pontos=0; // estourou
		return Pontos;
	}
	void registraJogador (Jogador J) {
		Cartas.ins (J, null);
	}
	boolean fazRodadaInicial () {
		Jogador J;
		System.out.println ("NOVA RODADA, Jogadores apostam!!!");
		J= (Jogador) Cartas.first();
		if (B.getCartasRestantes()>4) {
			// Da cartas iniciais
			while (J!=null) {
				if (((Pato)J).getDinheiro()>0) {  // se o jogador tem dinheiro
					setApostaTotal(J.fazAposta()+getApostaTotal()); // jogador aposta
					Cartas.sto(J, new int[] {B.getCarta(), B.getCarta()}); // jogador recebe cartas
				}
				J=(Jogador)Cartas.next(J);
			}
			return true;
		}
		return false;
	}
	boolean fazRodada () {
		Jogador J;
		Boolean PediuCarta=false;
		J= (Jogador) Cartas.first();
		while (J!=null) {
			if ((J.joga()>0)&&(((Pato)J).getDinheiro()>0)&&(B.getCartasRestantes()>0)) {  // se o jogador tem dinheiro e quer comprar carta e tem carta para comprar
				setApostaTotal(getApostaTotal()+J.recebeCarta(B.getCarta()));
				PediuCarta=true;
			}
			J= (Jogador) Cartas.next(J);
		}
		return PediuCarta;
	}
	int terminaJogo () {
		Jogador J, Vencedor=null;
		int maxPontos=0;
		System.out.println ("FIM DE JOGO!!");
		J= (Jogador) Cartas.first();
		while (J!=null) {
			imprimeFinal (J, (int []) Cartas.rcl(J),  J.mostraCartas());
			if (maxPontos<calculaPontos ((int []) Cartas.rcl(J), J.mostraCartas())) {
				maxPontos=calculaPontos ((int []) Cartas.rcl(J), J.mostraCartas());
				Vencedor=J;
			}
			J= (Jogador) Cartas.next(J);
		}
		if (Vencedor!=null) {
			System.out.println ("Ganhador: " + Vencedor + "\n\n\n");
			Vencedor.recebeGanhos (getApostaTotal());
		}

		J= (Jogador) Cartas.first();
		while (J!=null) {
			Mesa.poeNoLixo ((int []) Cartas.rcl(J));
			Mesa.poeNoLixo (J.mostraCartas());
			J= (Jogador) Cartas.next(J);
		}

		setApostaTotal(0);
		return 1;
	}
	int imprimeFinal (Jogador J, int [] CartasAbertas, int [] CartasFechadas) {
		System.out.print ("Jogador " + J + " Mesa: ");
		if (CartasAbertas!=null)
			for (int i=0;i<CartasAbertas.length;i++) System.out.print (CartasAbertas[i] + " ");
		System.out.print ("Mao: ");
		if (CartasFechadas!=null)
			for (int i=0;i<CartasFechadas.length;i++) System.out.print (CartasFechadas[i] + " ");
		System.out.println();
		return 1;
	}

}
