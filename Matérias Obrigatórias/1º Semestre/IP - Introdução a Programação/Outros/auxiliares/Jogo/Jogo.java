class Jogo {
	public static void main (String[] args) {
		Baralho B=new Baralho();
		Croupier Pierre = new Croupier(B);
		Jogador Drica=new AdrianaCamargo();
		Jogador Dea=new AdrianaCamargo();
		Jogador Console=new Manual();
		Pierre.registraJogador(Drica);
		Pierre.registraJogador(Dea);
		Pierre.registraJogador(Console);
		for (int i=0;i<5;i++) {
			System.out.println ("Baralho: " + i);
			Pierre.troca();
			while (Pierre.fazRodadaInicial()) {  // inicia enquanto tiver cartas.
				do {
					System.out.println ("Cartas?");
				} while (Pierre.fazRodada());
				Pierre.terminaJogo();
			}
		}
		System.out.print ("Drica ("+Drica+") terminou com: ");
		Drica.imprimeQuantoTem();
		System.out.print ("Dea ("+Dea+") terminou com: ");
		Dea.imprimeQuantoTem();
		System.out.print ("Console ("+Console+") terminou com: ");
		Console.imprimeQuantoTem();
		//B.imprimeCartasRestantes();
	}
}
