class ListaLigada {
	private Registro Cabeca;
	void ins (Object Chave, Object O) {
		Registro R = new Registro(Chave, O);
		R.Prox=Cabeca;
		Cabeca=R;
	}
	Object rcl (Object Chave) {
		Registro Sc=Cabeca;
		if (Chave!=null)
			while ((Sc!=null) && (Sc.Chave!=Chave)) Sc=Sc.Prox;
		return (Sc.O);  // objeto armazenado tem que garantir sua pr�pria integridade.
	}
	void sto (Object Chave, Object O) {
		Registro Sc=Cabeca;
		if (Chave!=null)
			while ((Sc!=null) && (Sc.Chave!=Chave)) Sc=Sc.Prox;
		if (Sc!=null) Sc.O=O;  // objeto armazenado tem que garantir sua pr�pria integridade.
		else System.out.println ("N�o encontrei chave:" + Chave);
	}
	Object first () {
		if (Cabeca!=null) return Cabeca.Chave;
		return null;
	}
	
	Object next (Object Chave) {
		Registro Sc=Cabeca;
		if (Chave!=null)
			while ((Sc!=null) && (Sc.Chave!=Chave)) Sc=Sc.Prox;
		if (Sc!=null) 
			if (Sc.Prox!=null) return Sc.Prox.Chave;
		return null;
	}
	
	Object del (Object Chave) {
		Registro Ante=null;
		Registro Sc=Cabeca;
		if (Chave!=null) {
			while ((Sc!=null) && (Sc.Chave!=Chave)) {
				Ante=Sc;
				Sc=Sc.Prox;
			}
			if ((Ante==null) && (Sc!=null)) {
				Cabeca=Sc.Prox;
				Sc.Prox=null;
				return Sc.O;
			}
			if ((Ante!=null) && (Sc!=null)) {
				Ante.Prox=Sc.Prox;
				Sc.Prox=null;
				return Sc.O;
			}
		}
		return null;
	}
}
