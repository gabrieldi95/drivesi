import java.util.Scanner; // pacote que contém a classe Scanner.

/** Classe para leitura de teclado. 
  * O objetivo é ocultar ("esconder") elementos de
  * orientação a objetos que serão apresentados mais tarde.
*/
public class Teclado {
	public static int proxInt () {
		Scanner sc=new Scanner (System.in); // instancia
		return sc.nextInt(); // retorna o retorno da chamada.
	}
}

