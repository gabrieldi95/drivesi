class EstouroDePilhaException extends Exception {
}

class Pilha {
	int [] arr;
	private int pos;
	Pilha (int newSize) {
		pos=0;
		arr=new int [newSize];
	}
	void push (int v) {
		try {
			arr[pos++]=v;
		}
		catch (ArrayIndexOutOfBoundsException e) {
			EstouroDePilhaException exc = new EstouroDePilhaException ();
			exc.initCause (e);
			throw exc;
			System.out.println ("Estourou a pilha.");
		}
	}
	int pop () {
		return arr[--pos];
	}
	int getsize () {
		return arr.length;
	}
	boolean isFull () {
		return pos>=arr.length;
	}
	boolean isEmpty () {
		return pos==0;
	}
	void printE () {
		for (int i=0;i<pos;i++) {
			System.out.print (arr[i] + " ");
		}
		System.out.println("");
	}
	void printC () {
		System.out.print (this + ": {");
		printE();
		System.out.println("}");
	}

	public static void main (String[] args) throws Exception {
		Pilha p1=new Pilha(3);
		p1.push (1);
		p1.push (2);
		p1.push (3);
		p1.push (4);
	}
}
