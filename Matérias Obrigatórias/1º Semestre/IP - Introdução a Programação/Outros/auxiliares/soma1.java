import java.util.Scanner;

public class soma1 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		long soma = 0;
		int valor;
		while (sc.hasNextInt()){
			valor = sc.nextInt();
			if (valor != 0) soma+=valor;
			else{
				System.out.println("Soma: " + soma);
				break;
			}
		}
	}
}

/*

Exemplo de Entrada
1
2
3
4
5
0

Exemplo de Saida
Soma: 15

*/