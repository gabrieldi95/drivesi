import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

class LinePanel extends JPanel {
	Triangulo t;
	public void paintComponent (Graphics g) {
		super.paintComponent(g);

		g.drawLine (t.x[0],t.y[0],t.x[1],t.y[1]);
		g.drawLine (t.x[0],t.y[0],t.x[2],t.y[2]);
		g.drawLine (t.x[1],t.y[1],t.x[2],t.y[2]);
	}
	public Dimension getPreferredSize () {
		return new Dimension (200,200);
	}
	public Dimension getMinimumSize () {
		return getPreferredSize();
	}
}

public class Tela {
	public static void desenha(Triangulo tr) {
		JFrame frame = new JFrame("Ola");
		final JLabel label = new JLabel("Olá Mundo!");
		LinePanel lp=new LinePanel();
		frame.getContentPane().add(label, BorderLayout.SOUTH);
		lp.t=tr;
		frame.getContentPane().add(lp, BorderLayout.NORTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
  }
}

