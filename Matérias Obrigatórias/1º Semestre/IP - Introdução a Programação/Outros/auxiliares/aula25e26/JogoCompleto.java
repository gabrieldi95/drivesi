import java.util.Random;

class JogoCompleto {
	static int njogadores;
	static int ncartas;
	static int[] cartas;
	static void inicializa () {
		ncartas=52;
		cartas=new int[ncartas];
		for (int i=0;i<ncartas;i++) cartas[i]=i+1;
		njogadores=0;
	}
	static void registra () { njogadores++; }
	/* Sorteia sem reposicao */
	static int sorteiaCarta() {
		Random rng=new Random ();
		int posicaoSorteada=Math.abs(rng.nextInt())%ncartas;   // sorteia de onde tirar a carta
		int cartaSorteada=cartas[posicaoSorteada];             // "separa" a carta que sorteou
		ncartas--;                                             // remove a carta do "monte"
		cartas[posicaoSorteada]=cartas[ncartas];               // coloca a última carta no lugar da sorteada
		return cartaSorteada;
	}

	/* Sorteia todas as cartas e já retorna quem ganhou. */
	static int fazRodada () {
		int cartaMaior=0, vencedor=-1;
		for (int i=0;i<njogadores;i++) {
			int cartaSorteada=sorteiaCarta();
			if (cartaSorteada>cartaMaior) {
				cartaMaior=cartaSorteada;
				vencedor=i;
			}
		}
		return vencedor+1;
	}
	public static void main (String[] args) {
		inicializa();
		registra();
		registra();
		registra();
		System.out.println (fazRodada() + " é o vencedor");
	}
}

