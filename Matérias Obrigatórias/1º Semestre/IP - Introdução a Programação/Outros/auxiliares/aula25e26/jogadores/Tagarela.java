package jogadores;
import suporte.*;

public class Tagarela implements Jogador {
	Carta mao;
	String nome;
	public Tagarela (String nome) { this.nome=nome; }
	public Carta mostra () { return mao;}
	public void recebe (Carta c) { 
		mao=c.clone();
		System.out.println (nome + ": peguei um " + mao.getNum() + " de " + mao.getNaipe()); 
	}
	public void venci () {
		System.out.println (nome + " é o maioral!!!");
	}
}

