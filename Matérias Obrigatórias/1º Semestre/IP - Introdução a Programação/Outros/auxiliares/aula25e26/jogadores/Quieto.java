package jogadores;
import suporte.*;

public class Quieto implements Jogador {
	Carta mao;
	String nome;
	public Quieto (String nome) { this.nome=nome; }
	public Carta mostra () { return mao;}
	public void recebe (Carta c) { 
		mao=c.clone();
	}
	public void venci () {
		System.out.println (" ganhei...");
	}
}

