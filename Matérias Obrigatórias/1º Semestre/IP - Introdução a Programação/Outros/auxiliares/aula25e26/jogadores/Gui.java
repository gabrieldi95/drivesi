package jogadores;
import suporte.*;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Gui extends JPanel implements Jogador {
	Carta mao;
	String nome;
	Tela t=null;
	public Gui (String nome) { this.nome=nome; }
	public Carta mostra () { return mao;}
	public void recebe (Carta c) { 
		mao=c.clone();
		if (t==null) t=new Tela();
		t.desenha (nome, mao, "");
	}
	public void venci () {
		t.desenha (nome, mao, "GANHEI!!!");
	}


	class LinePanel extends JPanel {
		Carta c;
		public Color corNaipe() {
			switch (c.getNaipe()) {
				case paus:
				case espadas: return Color.black;
			}
			return Color.red;
		}
		public String converteNaipe () {
			switch (c.getNaipe()) {
				case paus: return "\u2663";
				case copas: return "\u2665";
				case espadas: return "\u2660";
			}
			return "\u2666";
		}
		public void paintComponent (Graphics g) {
			super.paintComponent(g);

			g.drawLine (0,0,200,200);
			this.setFont(this.getFont().deriveFont(100f));
			g.setColor (corNaipe());
			g.drawString (converteNaipe(), 100,100);
			g.drawString (""+c.getNum(), 10,190);
		}
		public Dimension getPreferredSize () {
			return new Dimension (200,200);
		}
		public Dimension getMinimumSize () {
			return getPreferredSize();
		}
	}

	public class Tela {
		JLabel label;
		public void desenha(String nome, Carta c, String mensagem) {
			JFrame frame = new JFrame(nome);
			label = new JLabel(mensagem);
			LinePanel lp=new LinePanel();
			lp.c=c;
			frame.getContentPane().add(label, BorderLayout.SOUTH);
			frame.getContentPane().add(lp, BorderLayout.NORTH);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.pack();
			frame.setVisible(true);
	  }
	}

}

