package jogadores;
import suporte.*;

public interface Jogador {
	Carta mostra ();
	void recebe (Carta c);
	void venci ();
}

