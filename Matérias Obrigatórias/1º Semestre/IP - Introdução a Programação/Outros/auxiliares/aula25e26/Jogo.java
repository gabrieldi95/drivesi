import suporte.*;
import jogadores.*;

class Jogo {
	public static void main (String[] args) {
		Jogador j1 = new Tagarela ("Alemao");
		Jogador j2 = new Quieto ("Coxinha");
		Jogador j3 = new Gui ("Chaveiro");
		Administrador A = new Administrador();
		A.registra (j1);
		A.registra (j2);
		A.registra (j3);
		for (int i=0;i<10;i++) {
			A.fazRodada();
			A.apresentaVencedor().venci();
		}
	}
}

