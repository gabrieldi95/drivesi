package suporte;

import java.util.Random;
import java.util.Iterator;

public class Baralho implements Iterator {
	int ncartas;
	Carta[] cartas;
	public Baralho (int ncartaspornaipe) {
		int i, k=0;
		this.ncartas=ncartaspornaipe*Naipe.values().length;
		cartas=new Carta[ncartas];
		for (Naipe n : Naipe.values()) {
			for (i=0;i<ncartaspornaipe;i++, k++) {
				cartas[k]=new Carta(i+1, n);
			}
		}
	}
	public void print () {
		for (int i=0;i<ncartas;i++) {
			System.out.println (cartas[i] + " ");
		}
	}
	/* Sorteia sem reposicao */
	public Carta next() {
		Random rng=new Random ();
		int posicaoSorteada=Math.abs(rng.nextInt())%ncartas;   // sorteia de onde tirar a carta
		Carta cartaSorteada=cartas[posicaoSorteada];           // "separa" a carta que sorteou
		ncartas--;                                             // remove a carta do "monte"
		cartas[posicaoSorteada]=cartas[ncartas];               // coloca a última carta no lugar da sorteada
		return cartaSorteada;
	}
	public boolean hasNext () { return (ncartas>0); }
	public void remove () { throw new UnsupportedOperationException (); };

	public static void main (String[] args) {
		Baralho B = new Baralho (13);
		B.print();
		while (B.hasNext()) {
			Carta c = B.next();
			System.out.println (c.getNum() + " " + c.getNaipe());
			System.out.println (c.compareTo (new Carta (2, Naipe.copas)));
		}
	}
}

