package suporte;

public class Carta {
	protected int num;
	protected Naipe naipe;
	public Carta (int num, Naipe n) {
		this.num=num;
		naipe=n;
	}
	public Carta clone () {
		return new Carta (num, naipe);
	}
	public Naipe getNaipe () { return naipe; }
	public int getNum () { return num; }
	protected int toNum (Naipe n) {
		switch (n) {
			case paus: return 0;
			case copas: return 1;
			case espadas: return 2;
		}
		return 3; // ouros
	}
	public int compareTo (Carta c) {
		int dif = num-c.num;
		if (dif==0) {
			int n1=toNum(naipe);
			int n2=toNum( c.getNaipe());
			return n1-n2;
		}
		return dif;
	}
}
