import suporte.*;
import jogadores.*;

class Administrador {
	class No {
		Jogador j;
		No prox;
	}
	class Lista {
		No inicio;
		Lista () {
			inicio=null;
		}
		void insere (Jogador j) {
			No novo = new No ();
			novo.j=j;
			novo.prox=inicio;
			inicio=novo;
		}
		No getInicio () {
			return inicio;
		}
	}
	Lista jogadores;
	Baralho b;
	Administrador () {
		jogadores=new Lista ();
		b=new Baralho (13);
	}
	void registra (Jogador j) {
		jogadores.insere (j);
	}

	/* Sorteia todas as cartas e já retorna quem ganhou. */
	void fazRodada () {
		//int cartaMaior=0, vencedor=-1;
		No vez = jogadores.getInicio();
		while (vez!=null) {
			Carta cartaSorteada=b.next();
			vez.j.recebe (cartaSorteada);
			vez=vez.prox;
		}
	}
	Jogador apresentaVencedor() {
		Carta cartaMaior=null;
		Jogador vencedor=null;
		No vez = jogadores.getInicio();
		while (vez!=null) {
			if ((cartaMaior==null)||(cartaMaior.compareTo (vez.j.mostra())<0)) {
				cartaMaior=vez.j.mostra();
				vencedor=vez.j;
			}
			vez=vez.prox;
		}
		return vencedor;
	}
}

