import java.util.Scanner;

public class soma2 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		long soma = 0;
		while (sc.hasNextInt()){
			soma += sc.nextInt();
		}
		System.out.println("Soma: " + soma);
	}
}

/*

Exemplo de Entrada
1
2
3
4
5

Exemplo de Saida
Soma: 15

*/