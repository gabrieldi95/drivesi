/* Agora usando o pacote Math.
 */

// informa ao compilador para carregar o pacote Math.
import java.lang.Math.*;  

public class Casa2 {
	public static double pontua (double credito, 
		double renda, int pessoas) {
		return 1.0/(1.0+Math.pow (renda/(2000*pessoas)-1.0,2)+
		                Math.pow (credito/(60000*pessoas)-1.0,2));
	}
}

