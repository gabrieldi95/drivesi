/* calcula pontuação de um candidato a empréstimo.
 * 5000 é a renda familiar, 3 é o número de pessoas, 
 * 180000 é o crédito pedido. 
 * Show de horror!
 * Como é que dá para calcular para outro caso e 
 * não errar na hora de trocar os valores?
 */
public class CasaZero {
	public static void main (String[] args) {
		System.out.println (
		  1.0/
		   (1.0+
		    ((5000/(2000*3)-1.0)*(5000/(2000*3)-1.0)) +
		    ((180000/(60000*3)-1.0)*
		     (180000/(60000*3)-1.0)
			)
		   )
		);
	}
}

