/* calcula pontuação de um candidato a empréstimo.
 * Agora tem método, então dá para rodar vários casos com um main!!
 * já não é mais Show de horror!
 */
public class CasaMetodo {
	public static double pontua (double credito, 
		double renda, int pessoas) {
		return 1.0/(1.0+((renda/(2000*pessoas)-1.0)*
		                 (renda/(2000*pessoas)-1.0))+
		                ((credito/(60000*pessoas)-1.0)*
		                 (credito/(60000*pessoas)-1.0)));
	}
	public static void main (String[] args) {
		System.out.println (pontua(1.8e5, 5e3, 3));
		System.out.println (pontua(3.0e5, 5e3, 3));
		System.out.println (pontua(2.5e5, 6e3, 2));
		System.out.println (pontua(1.8e5, 7e3, 3));
	}
}

