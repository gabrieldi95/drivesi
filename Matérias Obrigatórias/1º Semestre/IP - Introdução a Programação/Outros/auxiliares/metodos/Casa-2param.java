public class Casa {
	static final int SOLTEIRO=0, CASADO=1, DIVORCIADO=2;
	static final double CUSTO=2000;  // custo per capita
	static final double ALAVANCAGEM=30;  // 
	public static double potencia (double v, int p) {
		double r=1;
		for (int i=1;i<=p;i++) {
			r*=v;
		}
		return r;
	}
	public static int contaPessoas (int estado, int filhos) {
		int n=filhos;
		switch (estado) {
			case CASADO: return 2+n;
			case SOLTEIRO:
			case DIVORCIADO: return 1+n;
			default: return -1000; // erro
		}
	}
	public static double pontua (double credito, 
			double renda, int estado, int filhos) {
		return -(potencia (1-renda/(CUSTO*contaPessoas (estado, filhos)), 2) + potencia (1-credito/(ALAVANCAGEM*renda), 2));
	}
	public static void main (String[] args) {
		System.out.println (pontua(ALAVANCAGEM*CUSTO, 1*CUSTO, SOLTEIRO, 1));
	}
}

