import javax.swing.JOptionPane;

/** Este é popando uma caixa de entrada. */
public class TecladoG {
	public static int proxInt () {
		/* Mostra a caixa e armazena a 
		 * string digitada em resp. */
		String resp=JOptionPane.showInputDialog (null, "Entre com um valor"); 
		/* Converte a string em int e retorna. */
		return Integer.parseInt (resp);
	}
}

