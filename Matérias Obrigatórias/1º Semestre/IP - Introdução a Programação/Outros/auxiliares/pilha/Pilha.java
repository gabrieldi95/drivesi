class Pilha {
	int [] arr;
	int pos;
	Pilha (int newSize) {
		pos=0;
		arr=new int [newSize];
	}
	void push (int v) {
		arr[pos++]=v;
	}
	int pop () {
		return arr[--pos];
	}
	int getsize () {
		return arr.length;
	}
	boolean isFull () {
		return pos>=arr.length;
	}
	boolean isEmpty () {
		return pos==0;
	}
	void printE () {
		for (int i=0;i<pos;i++) {
			System.out.print (arr[i] + " ");
		}
		System.out.println("");
	}
	void printC () {
		System.out.print (this + ": {");
		printE();
		System.out.println("}");
	}

	static void atrapalha (Pilha p) {
		p.push (-55);
	}

	public static void main (String[] args) {
		Pilha p1=new Pilha(5);
		Pilha p2=new Pilha(7);
		p1.push (1);
		p1.push (3);
		p2.push (1);
		p2.push (5);
		p1.printE();
		p2.printE();
	}
}
