/** Segunda solução para a simulação do cavalo.
  * Nesta, o cavalo tem como atributo a sua posição. */

class Cavalo {
	private int[] posicao=new int[2];  // private!!!
	private int[] dx={1,-1, 1, -1, 2,  2, -2, -2};
	private int[] dy={2, 2,-2, -2, 1, -1,  1, -1};
	private int n=8;
	void posiciona (int x, int y) {
		posicao[0]=x;
		posicao[1]=y;
	}
	int[] posicao () {
		int[] r=new int[2];
		r[0]=posicao[0];
		r[1]=posicao[1];
		return r;
	}
	int[] proxima (int i) {
		// retorna a i-esima das 8 próximas posições possiveis.
		// i tem que estar dentro do intervalo válido!
		int[] r=new int[2];
		r[0]=posicao[0]+dx[i];
		r[1]=posicao[1]+dy[i];
		return r;
	}

	int[] move (int i) {
		// move para a i-esima das 8 próximas posições possiveis.
		// retorna a nova posicao
		// i tem que estar dentro do intervalo válido!
		int[] r=new int[2];
		posicao[0]+=dx[i];
		posicao[1]+=dy[i];
		r[0]=posicao[0];
		r[1]=posicao[1];
		return r;
	}

}


