/** Primeira solução para a simulação do cavalo.
  * Nesta, dizemos que o cavalo "passa" pela casa e deixa uma marca 
  * pelas casas por onde passou.
  */
import java.util.Scanner;

class ResolveCavalo2 {
	public static void main (String[] args) {
		int VISITADO=1;
		Cavalo c = new Cavalo();
		c.posiciona (0,0);
		Tabuleiro T = new Tabuleiro();
		T.cria (8,8);            // poderia fazer isso no construtor.
		T.limpa ();
		T.marca (c.posicao(),0); // marcar a ordem de visitação
		for (int i=1, k=0;i<10;i++) { 
			int j=0;
			while ((j<8)&&(T.testa(c.proxima(j))!=T.VAZIO)) j++;
			if (j>=8) System.out.println ("Não foi possível fazer o movimento");
			else T.marca (c.move(j),i);
		}
		Tela.desenha(T); // ocultamento, modularização 
	}
}


