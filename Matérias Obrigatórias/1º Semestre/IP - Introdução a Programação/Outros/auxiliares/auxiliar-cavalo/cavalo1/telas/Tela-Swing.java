import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

class LinePanel extends JPanel {
	Tabuleiro t;
	public void paintComponent (Graphics g) {
		super.paintComponent(g);
		int sx=getWidth ();
		int sy=getHeight();
		int dy=sy/t.T.length, dx=sx/t.T[0].length;
		for (int i=0;i<=t.T.length;i++) {
			g.drawLine (0,i*dy,sx,i*dy);
		}
		for (int i=0;i<=t.T[0].length;i++) {
			g.drawLine (i*dx,0, i*dx, sy);
		}
		for (int i=0;i<t.T.length;i++) {
			for (int j=0;j<t.T[i].length;j++) {
				if (t.T[i][j]!=t.VAZIO) {
					//g.drawOval (i*dx, j*dy, dx, dy);
					g.drawString (""+t.T[i][j], i*dx+dx/2, j*dy+dy/2);
				}
			}
		}
	}
	public Dimension getPreferredSize () {
		return new Dimension (200,200);
	}
	public Dimension getMinimumSize () {
		return getPreferredSize();
	}
}

public class Tela {
	public static void desenha(Tabuleiro t) {
		JFrame frame = new JFrame("Ola");
		final JLabel label = new JLabel("Olá Mundo!");
		LinePanel lp=new LinePanel();
		frame.getContentPane().add(label, BorderLayout.SOUTH);
		lp.t=t;
		frame.getContentPane().add(lp, BorderLayout.NORTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
  }
}

