/** Primeira solução para a simulação do cavalo.
  * Nesta, dizemos que o cavalo "passa" pela casa e deixa uma marca 
  * pelas casas por onde passou.
  * nesta solução o cavalo não é modelado como um objeto, embora
  * as coordenadas da casa são x e y. */

class ResolveCavalo1 {
	public static void main (String[] args) {
		int x=0, y=0, VISITADO=1;
		Tabuleiro T = new Tabuleiro();
		T.cria (8,8);            // poderia fazer isso no construtor.
		T.limpa ();
		//T.marca (x,y,VISITADO);  // cavalo na posiçao 0,0
		T.marca (x,y,0);         // poderia marcar a ordem de visitação
		for (int i=1;i<10;i++) { 
			// este bloco está "feio" mas é fácil de entender.
			// como você ajustaria o código?
			if (T.testa (x+1,y+2)==T.VAZIO) {
				x++; y+=2; T.marca(x,y,VISITADO);
			} else if (T.testa (x-1,y+2)==T.VAZIO) {
				x--; y+=2; T.marca (x,y,VISITADO);
			} else if (T.testa (x+1,y-2)==T.VAZIO) {
				x++; y-=2; T.marca (x,y,VISITADO);
			} else if (T.testa (x-1,y-2)==T.VAZIO) {
				x--; y-=2; T.marca (x,y,VISITADO);
			} else if (T.testa (x+2,y+1)==T.VAZIO) {
				y++; x+=2; T.marca (x,y,VISITADO);
			} else if (T.testa (x-2,y+1)==T.VAZIO) {
				y++; x-=2; T.marca (x,y,VISITADO);
			} else if (T.testa (x+2,y-1)==T.VAZIO) {
				y--; x+=2; T.marca (x,y,VISITADO);
			} else if (T.testa (x-2,y-1)==T.VAZIO) {
				y--; x-=2; T.marca (x,y,VISITADO);
			} else {
				System.out.println ("Não foi possível realizar o " + 
				                     i + "ésimo movimento.");
			}
			T.marca (x,y,i); // poderia marcar a ordem de visitação.
		}
		Tela.desenha(T); // ocultamento, modularização 
	}
}


