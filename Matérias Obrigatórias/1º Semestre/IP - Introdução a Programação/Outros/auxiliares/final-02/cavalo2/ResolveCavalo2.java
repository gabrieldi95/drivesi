/** Primeira solução para a simulação do cavalo.
  * Nesta, dizemos que o cavalo "passa" pela casa e deixa uma marca 
  * pelas casas por onde passou.
  */
import java.util.Scanner;

class ResolveCavalo2 {
	public static void main (String[] args) {
		int VISITADO=1;
		Scanner sc = new Scanner (System.in);
		Cavalo c = new Cavalo();
		c.posiciona (sc.nextInt(),sc.nextInt());
		Tabuleiro T = new Tabuleiro();
		T.cria (8,8);            // poderia fazer isso no construtor.
		T.limpa ();
		T.marca (c.posicao(),0); // poderia marcar a ordem de visitação
		int[] r= new int [2]; 
		for (int i=0, k=0;i<4;i++) { 
			int m=2*i;
			for (int j=0;j<8;j++,k++) {
				r[0]=m; r[1]=j;
				T.marca(r,k);
			}
			for (int j=7;j>=0;j--,k++) {
				r[0]=m+1; r[1]=j;
				T.marca(r,k);
			}
		}
		Tela.desenha(T); // ocultamento, modularização 
	}
}


