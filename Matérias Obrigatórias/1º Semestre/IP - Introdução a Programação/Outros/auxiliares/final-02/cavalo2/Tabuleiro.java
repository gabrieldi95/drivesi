/* Tabuleiro do jogo */
class Tabuleiro {
	static final int VAZIO=-1, INVALIDO=-100; // constantes
	// note que não tem mais o static!
	// tem uma questão de encapsulamento e aqui, pois o atributo 
	// pode ser acessado diretamente.

	int[][] T;
	void cria (int nlinhas, int ncolunas) {
		T=new int[nlinhas][];
		for (int i=0;i<nlinhas;i++) {
			T[i]=new int[ncolunas];
		}
	}
	void limpa () {
		for (int i=0;i<T.length;i++) {
			for (int j=0;j<T[i].length;j++) {
				T[i][j]=VAZIO;
			}
		}
	}
	void marca (int[] p, int peca) {
		poe (p[0], p[1], peca); // supoe que p é valido.
	}

	int testa (int[] p) {
		System.out.println (p[0]+","+ p[1]);
		if ((p[0]<0) || (p[0]>=T.length) || 
		    (p[1]<0) || (p[1]>=T[0].length)) return INVALIDO;
		return T[p[0]][p[1]]; // array indexando matriz
	}

	/* criei estes métodos seguindo uma "intuição" de que precisaria 
	 * delas, mas não precisei. */
	void poe (int linha, int coluna, int peca) {
		T[linha][coluna]=peca; // supoe que x e y são validos
	}
	int tira (int linha, int coluna) {
		int r=T[linha][coluna]; // supoe que x e y são válidos
		T[linha][coluna]=VAZIO;
		return r;
	}

}

