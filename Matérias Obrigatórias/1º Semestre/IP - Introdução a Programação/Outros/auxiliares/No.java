public class No{
	
	public static void main (String[] args){
		No no1 = new No();
		no1.setDados(45);
	
		No no2 = new No();
		no2.setDados(-10);
		no1.setProx(no2);
	
		No no3 = new No();
		no3.setDados(13);
		no2.setProx(no3);

		imprime (no1);
		System.out.println ("insere");
		no1=insere (no1, -100);
		imprime (no1);
	}
		
	protected int dados;
	protected No prox;

	static No insere (No aux, int v) {
		No novo = new No();
		novo.setDados(v);
		novo.setProx(aux);
		return novo;
	}
	
	static void imprime (No aux) {
		while (aux!=null) {
			System.out.println (aux.getDados());
			aux=aux.getProx();
		}
	}
	
	public int getDados (){
		return dados;
	}
	
	public void setDados (int dados){
		this.dados = dados;
		
	}
	public No getProx () {
		return prox;
 	}

	public void setProx (No prox) {
		this.prox = prox;
	}
}
