class Moda
{
     
          double[] cole��oDeValores;     //O arranjo de valores digitado pelo usu�rio
     int quantidadeDeItens;             //A quantidade de valores armazenada na cole��o, equivale ao comprimento do arranjo 'colecao'

void carregaValores(double[] valores)
     {
          quantidadeDeItens = valores.length;               //passa o comprimento do arranjo criado pelo usu�rio 
          cole��oDeValores = valores;                   //ser� que funciona 2 vezes???
     }

double moda()    //M�todo exibe uma das modas encontrada, conforme orienta��o do professor.
     {
          double moda = 0.0;                  //Inicializa a moda que ser� calculada
          double[] modamais;
          int n = 0;
          int contadorDeIguais = 0;           //Armazena a quantidade de valores iguais em cada passagem 
          int maiorNumeroDeIguais = 0;        //Guarda a maior quantidade de valores iguais encontrada   
          for (int i = 0; i < quantidadeDeItens; i++)
          {
               for (int j = 0; j < quantidadeDeItens; j++)
               {
                    if (cole��oDeValores[i] == cole��oDeValores[j])  //Testa se os valores contidos na posi��o i e j da 'cole��oDeValores' s�o iguais
                         contadorDeIguais++;                         //Atualiza a contagem dos valores iguais encontrados
               }
               if (contadorDeIguais >= maiorNumeroDeIguais)  //Caso a contagem seja a maior encontrada at� o momento, armazena a respectiva moda.
               {
                    moda = cole��oDeValores[i]; 
                    maiorNumeroDeIguais = contadorDeIguais;  //Atualiza a maior quantidade encontrada.
                    n++;
               }
               contadorDeIguais = 0;
               modamais = new double[n];
          }
          return moda;
     }
}