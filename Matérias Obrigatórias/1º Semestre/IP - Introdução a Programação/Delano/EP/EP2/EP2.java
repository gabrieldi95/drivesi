class Estatistica
{
     double[] colecao;
     double media;
     int n; 
     int x;
     
     void carregaValores(double[] valores)
     {
          n = valores.length;
          colecao = new double[n];
          for (int i = 0; i < n; i++)
          {
          colecao[i] = valores[i];
          }
     }
     
     void imprime()
     {
          
         System.out.println("N�mero de objetos:"+ n);
          for (int i = 0; i < n; i++)
          {
               System.out.println(colecao[i]);
          }
     }
     
     double media()
     {
          double soma = 0;
          for (int i = 0; i < n; i++)
          {
               soma = soma + colecao[i];
          }
          return soma/n;
     }

     void moda()
     {
          double moda = 0.0;
          int maiorNumeroDeIguais = 0;
          int contadorDeIguais = 0;
          
          for (int i = 0; i < n; i++)
          {
               for (int j = 0; j < n; j++)
               {
                    if (colecao[i] == colecao[j])
                    {
                         contadorDeIguais++;
                    }
               }
               
               if (contadorDeIguais >= maiorNumeroDeIguais)
               {
                    moda = colecao[i];
                    maiorNumeroDeIguais = contadorDeIguais;
               }
               contadorDeIguais = 0;
          }
               System.out.println("A moda �: " + moda);
     }
     
     void amplitude() //diferen�a entre o maior e o menor valores
     {
          double menor = colecao[0];
          double maior = colecao[0];
          for (int i = 0; i < n; i++)
          {
               if (colecao[i] < menor)
                    menor = colecao[i];
               if (colecao[i] > maior)
                    maior = colecao[i];
          }
          double amplitude = maior - menor;
          System.out.println(amplitude);
     }
     
     void desvioPadrao()
     {
          double somatorio = 0;
          media = media();
          for (int i = 0; i < n; i++)
          {
               somatorio = somatorio + (colecao[i]-media)*(colecao[i]-media);
          }
          double desvioPadrao = Math.sqrt(somatorio/n);
          System.out.println(desvioPadrao);
     }
}