class Dados {
	/** Matriz de caracteres que representa a grade de
	  * letras onde as palavras devem ser procuradas. 
	  */
	char[][] Grade;
	/** n�mero de linhas da grade de letras */
	int ROWS;
	/** n�mero de colunas da grade de letras */
	int COLS;
	/** Lista de palavras a procurar. */
	String[] Palavra;
	/** Cria uma instancia da classe dados por c�pia das refer�ncias � grade 
	  * e � lista de palavras a procurar.
	  * @param Grade array (matriz) de caracteres que cont�m a grade de letras 
	  * onde procurar as palavras.
	  * @param Palavra array (vetor) de palavras a procurar na grade.
	  */
	Dados (char[][] Grade, String[] Palavra) {
		this.Grade= Grade;
		this.Palavra= Palavra;
		ROWS=Grade.length;
		if (ROWS>0) COLS=Grade[0].length;
	}
	/** Cria uma instancia da classe contendo uma grade de tamanho
	  * (Rows,Cols) com cada posi��o preenchida por um espa�o e
	  * uma lista de palavras a procurar de tamanho NPalavras
	  * com elementos n�o inicializados.
	  * @param Rows n�mero de linhas da grade.
	  * @param Cols n�mero de colunas da grade.
	  * @param NPalavras n�mero de palavras a procurar.
	  */
	Dados (int Rows, int Cols, int NPalavras) {
		ROWS=Rows;
		COLS=Cols;
		Grade=new char[ROWS][COLS];
		Palavra= new String[NPalavras];
		for (int i=0;i<ROWS;i++) {
			for (int j=0; j<COLS;j++) this.Grade[i][j]=' ';
		}
	}
	/** Compara duas inst�ncias da classe Dados. Retorna true
	  * caso os caracteres armazenados nas grades e a lista de
	  * palavras forem iguais.
	  * @param C dados a comparar com os desta inst�ncia.
	  */
	boolean equals (Dados C) {
	    // Testa dimensoes dos arrays
		if ((ROWS!=C.ROWS)||(COLS!=C.COLS)||
		    (Palavra.length!=C.Palavra.length)) return false; 
		// Testa conte�do da Grade.
		for (int i=0;i<ROWS;i++) 
			for (int j=0; j<COLS;j++) 
				if (this.Grade[i][j]!=C.Grade[i][j]) return false;
		// Testa palavras
		for (int i=0;i<Palavra.length;i++)
			if (Palavra[i].equals (C.Palavra[i])==false) return false;
		return true;
	}
	/** Imprime os caracteres contidos na grade e as palavras contidas 
	  * na lista de palavras.
	  */
	void print () {
		for (int i=0;i<ROWS;i++) {
			for (int j=0;j<COLS;j++) {
				System.out.print (Grade[i][j]+ " ");
			}
			System.out.println();
		}
		System.out.println();
		for (int i=0;i<Palavra.length;i++) {
			System.out.println (Palavra[i]);
		}
	}
}
