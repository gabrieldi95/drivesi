/* d� at� para fazer um array tridimensional */
class TestaCacador {
	/** Grade para o teste 1 */
	static char Grade1[][]= {
		{'E', 'G', 'V', 'I', 'G', 'A', 'C', 'K', 'B', 'P', 'H', 'Q', 'Q', 'W'},
		{'M', 'B', 'O', 'L', 'S', 'O', 'P', 'K', 'Q', 'P', 'L', 'F', 'B', 'N'},
		{'Q', 'E', 'U', 'X', 'D', 'P', 'O', 'Q', 'Z', 'N', 'I', 'J', 'Q', 'X'},
		{'O', 'O', 'T', 'N', 'U', 'S', 'A', 'R', 'R', 'A', 'F', 'O', 'Y', 'Z'},
		{'Q', 'S', 'L', 'A', 'C', 'U', 'M', 'E', 'E', 'I', 'R', 'A', 'E', 'W'},
		{'L', 'U', 'N', 'V', 'L', 'E', 'T', 'R', 'F', 'N', 'B', 'S', 'Z', 'C'},
		{'H', 'S', 'A', 'W', 'P', 'I', 'J', 'E', 'T', 'N', 'C', 'L', 'I', 'J'},
		{'M', 'U', 'V', 'D', 'T', 'L', 'C', 'P', 'L', 'E', 'H', 'W', 'N', 'V'},
		{'C', 'A', 'I', 'B', 'R', 'O', 'U', 'A', 'S', 'L', 'L', 'Y', 'C', 'D'},
		{'V', 'R', 'N', 'K', 'A', 'A', 'B', 'L', 'W', 'S', 'R', 'H', 'O', 'O'},
		{'E', 'U', 'Q', 'T', 'A', 'D', 'D', 'B', 'J', 'G', 'P', 'P', 'A', 'W'},
		{'E', 'N', 'N', 'A', 'A', 'I', 'V', 'O', 'P', 'E', 'B', 'M', 'S', 'Y'},
		{'H', 'A', 'S', 'X', 'S', 'N', 'W', 'I', 'X', 'D', 'N', 'S', 'R', 'F'},
		{'J', 'Z', 'F', 'O', 'R', 'R', 'O', 'Z', 'E', 'S', 'A', 'O', 'X', 'Z'}
	};

	/** Palavras para procurar no teste 1 */
	static String Palavra1[]= {
		"CAIBRO",    //(8,0)
		"SARRAFO",   //(3,5)
		"QUADRADO",  //(4,0)
		"VIGA",      //(0,2)
		"TELHA",     //(6,8)
		"ZINCO",     //(5,12)
		"METALICA",  //(1,0)
		"FORRO",     //(13,2)
		"MANTA",     //(7,0)
		"CUMEEIRA",  //(4,4)
		"SUSUARUNA"  //(4,1)
    };

	/** Grade para o teste 2 */
	static char Grade2[][]= {
		{'B', 'T', 'O', 'B', 'F', 'Q', 'Y', 'T', 'V', 'Z'},
		{'G', 'B', 'C', 'E', 'X', 'O', 'V', 'R', 'D', 'K'},
		{'C', 'U', 'M', 'E', 'E', 'I', 'R', 'A', 'E', 'K'},
		{'Q', 'C', 'A', 'I', 'B', 'R', 'O', 'R', 'C', 'V'},
		{'P', 'U', 'M', 'R', 'B', 'C', 'B', 'F', 'O', 'B'},
		{'C', 'S', 'A', 'Y', 'Z', 'I', 'N', 'C', 'O', 'K'},
		{'Y', 'G', 'I', 'D', 'B', 'I', 'Z', 'Z', 'E', 'G'},
		{'A', 'L', 'Z', 'Q', 'R', 'A', 'L', 'I', 'B', 'M'},
		{'A', 'N', 'S', 'X', 'B', 'A', 'K', 'E', 'I', 'T'},
		{'V', 'I', 'G', 'A', 'F', 'V', 'D', 'T', 'J', 'K'},
		{'Z', 'U', 'G', 'Q', 'E', 'E', 'S', 'O', 'O', 'S'},
		{'M', 'T', 'Q', 'O', 'C', 'P', 'A', 'V', 'P', 'U'},
		{'V', 'E', 'A', 'Z', 'X', 'M', 'R', 'I', 'W', 'S'},
		{'K', 'L', 'T', 'G', 'B', 'L', 'R', 'V', 'H', 'U'},
		{'V', 'H', 'J', 'A', 'Z', 'F', 'A', 'L', 'I', 'A'},
		{'O', 'A', 'A', 'H', 'L', 'P', 'F', 'J', 'K', 'R'},
		{'V', 'O', 'B', 'T', 'W', 'I', 'O', 'O', 'I', 'U'},
		{'J', 'P', 'O', 'K', 'Y', 'G', 'C', 'K', 'Q', 'N'},
		{'W', 'F', 'J', 'X', 'M', 'D', 'L', 'A', 'J', 'A'},
		{'G', 'M', 'M', 'A', 'N', 'T', 'A', 'Y', 'L', 'I'}
    };
	
    /** Palavras para procurar no teste 2 */
	static String Palavra2[]= {
		"CAIBRO",    //(3,1)
		"SARRAFO",   //(10,6)
		"QUADRADO",  //(3,0)
		"VIGA",      //(9,0)
		"TELHA",     //(11,1)
		"ZINCO",     //(5,4)
		"METALICA",  //(11,0)
		"FORRO",     //(0,4)
		"MANTA",     //(19,2)
		"CUMEEIRA",  //(2,0)
		"SUSUARUNA"  //(10,9)
    };

	/** Grade para o teste 3 */
	static char Grade3[][]= {
		{'P', 'X', 'Q', 'Q', 'W', 'K', 'P', 'G', 'R', 'M', 'Q', 'I', 'C', 'C', 'V', 'L', 'D', 'U', 'R', 'G'},
		{'F', 'V', 'C', 'P', 'X', 'Y', 'M', 'F', 'N', 'E', 'P', 'L', 'V', 'Y', 'A', 'Y', 'S', 'C', 'I', 'Q'},
		{'M', 'I', 'F', 'A', 'F', 'P', 'C', 'I', 'G', 'W', 'G', 'P', 'M', 'Z', 'Z', 'M', 'A', 'V', 'B', 'H'},
		{'L', 'N', 'M', 'R', 'L', 'Y', 'P', 'S', 'N', 'V', 'Y', 'A', 'G', 'H', 'J', 'U', 'I', 'A', 'L', 'Y'},
		{'J', 'C', 'R', 'S', 'W', 'C', 'A', 'X', 'A', 'S', 'R', 'B', 'T', 'F', 'T', 'T', 'A', 'S', 'P', 'D'},
		{'M', 'W', 'M', 'P', 'W', 'O', 'A', 'E', 'C', 'P', 'A', 'Q', 'D', 'A', 'X', 'E', 'O', 'B', 'A', 'Z'},
		{'B', 'V', 'S', 'H', 'E', 'M', 'F', 'S', 'P', 'H', 'A', 'N', 'U', 'S', 'L', 'R', 'V', 'V', 'B', 'F'},
		{'G', 'I', 'Z', 'Q', 'S', 'B', 'C', 'M', 'P', 'O', 'I', 'T', 'D', 'W', 'M', 'N', 'E', 'D', 'D', 'G'},
		{'V', 'A', 'P', 'M', 'I', 'I', 'S', 'I', 'T', 'Y', 'B', 'N', 'O', 'A', 'O', 'O', 'I', 'W', 'H', 'S'},
		{'P', 'F', 'M', 'L', 'V', 'N', 'U', 'J', 'R', 'I', 'O', 'O', 'E', 'S', 'L', 'L', 'O', 'H', 'O', 'G'},
		{'W', 'Q', 'M', 'G', 'R', 'A', 'V', 'A', 'T', 'A', 'N', 'I', 'L', 'L', 'M', 'I', 'H', 'D', 'Z', 'J'},
		{'V', 'R', 'E', 'G', 'X', 'C', 'I', 'A', 'X', 'A', 'U', 'K', 'R', 'E', 'O', 'Q', 'A', 'Z', 'B', 'H'},
		{'K', 'Y', 'Z', 'J', 'B', 'A', 'X', 'C', 'B', 'L', 'B', 'X', 'S', 'I', 'R', 'S', 'T', 'S', 'M', 'O'},
		{'J', 'C', 'N', 'N', 'Y', 'O', 'Q', 'A', 'A', 'M', 'Z', 'W', 'N', 'C', 'G', 'O', 'F', 'F', 'G', 'T'},
		{'T', 'R', 'U', 'F', 'D', 'L', 'H', 'V', 'Y', 'E', 'C', 'E', 'F', 'B', 'N', 'F', 'A', 'A', 'T', 'O'}
    };
    /** Palavras para procurar no teste 3 */
	static String Palavra3[]= {
		"CALCAS",     //(1,2)
		"CAMISA",     //(0,13)
		"SAIA",       //(1,16)
		"GRAVATA",    //(10,3)
		"TERNO",      //(4,15)
		"COMBINACAO", //(4,5)
		"BOLERO",     //(8,10)
		"SAPATOS",    //(3,7)
		"SANDALIAS",  //(4,9)
		"CHINELOS",   //(5,8)
		"REGATA"      //(0,8)
    };

	/** Grade para o teste 4 */
	static char Grade4[][]= {
		{'K', 'R', 'A', 'O', 'J', 'E', 'F', 'C', 'K', 'C', 'L', 'A', 'S', 'S', 'E', 'T', 'T', 'K'},
		{'Y', 'E', 'N', 'V', 'B', 'B', 'I', 'K', 'Y', 'Q', 'J', 'Y', 'F', 'A', 'Z', 'V', 'E', 'S'},
		{'M', 'F', 'W', 'Z', 'L', 'J', 'W', 'I', 'O', 'K', 'H', 'K', 'M', 'W', 'X', 'W', 'C', 'E'},
		{'B', 'E', 'X', 'I', 'F', 'C', 'E', 'F', 'E', 'A', 'H', 'R', 'O', 'U', 'T', 'C', 'M', 'Z'},
		{'Q', 'R', 'K', 'F', 'Z', 'C', 'V', 'T', 'A', 'G', 'Y', 'V', 'Y', 'F', 'V', 'Y', 'C', 'I'},
		{'D', 'E', 'F', 'C', 'T', 'I', 'V', 'K', 'O', 'R', 'W', 'G', 'F', 'H', 'Z', 'F', 'U', 'M'},
		{'I', 'N', 'S', 'T', 'A', 'N', 'C', 'I', 'A', 'M', 'R', 'R', 'A', 'Q', 'A', 'A', 'O', 'E'},
		{'A', 'C', 'X', 'S', 'Y', 'S', 'O', 'C', 'U', 'L', 'T', 'A', 'M', 'E', 'N', 'T', 'O', 'T'},
		{'X', 'I', 'F', 'U', 'Y', 'X', 'T', 'Y', 'O', 'I', 'G', 'O', 'N', 'O', 'T', 'K', 'K', 'O'},
		{'X', 'A', 'I', 'M', 'V', 'T', 'L', 'X', 'Z', 'Z', 'M', 'O', 'C', 'J', 'V', 'J', 'L', 'D'},
		{'Q', 'M', 'A', 'T', 'R', 'I', 'B', 'U', 'T', 'O', 'Z', 'O', 'W', 'V', 'O', 'C', 'Y', 'O'},
		{'G', 'F', 'J', 'U', 'Y', 'I', 'C', 'V', 'U', 'E', 'D', 'G', 'E', 'S', 'C', 'Y', 'A', 'V'},
		{'S', 'O', 'B', 'R', 'E', 'C', 'A', 'R', 'G', 'A', 'K', 'D', 'T', 'V', 'K', 'H', 'Q', 'R'},
		{'Z', 'J', 'E', 'N', 'C', 'A', 'P', 'S', 'U', 'L', 'A', 'M', 'E', 'N', 'T', 'O', 'J', 'J'},
		{'G', 'H', 'T', 'V', 'P', 'O', 'L', 'I', 'M', 'O', 'R', 'F', 'I', 'S', 'M', 'O', 'B', 'A'}
    };
    /** Palavras para procurar no teste 4 */
	static String Palavra4[]= {
		"ENCAPSULAMENTO", //(13,2)
		"CLASSE",         //(0,9)
		"OBJETO",         //(0,3)
		"INSTANCIA",      //(6,0)
		"OCULTAMENTO",    //(7,6)
		"ARRANJO",        //(4,8)
		"METODO",         //(5,17)
		"ATRIBUTO",       //(10,2)
		"REFERENCIA",     //(0,1)
		"POLIMORFISMO",   //(14,4)
		"SOBRECARGA"      //(12,0)
    };

	/** Grade para o teste 5 */
	static char Grade5[][]= {
		{'U', 'Q', 'Z', 'Z', 'Y', 'C', 'U', 'J', 'S', 'A', 'C', 'T', 'R', 'K', 'N', 'F', 'S', 'S'},
		{'Y', 'B', 'G', 'O', 'Z', 'E', 'L', 'P', 'J', 'M', 'T', 'V', 'C', 'P', 'A', 'B', 'P', 'Q'},
		{'D', 'C', 'L', 'V', 'C', 'P', 'D', 'A', 'M', 'O', 'E', 'R', 'V', 'L', 'J', 'S', 'P', 'E'},
		{'F', 'B', 'W', 'P', 'T', 'U', 'P', 'N', 'S', 'G', 'B', 'D', 'I', 'C', 'L', 'M', 'O', 'K'},
		{'K', 'B', 'F', 'Y', 'X', 'G', 'L', 'U', 'C', 'S', 'O', 'J', 'P', 'B', 'H', 'X', 'L', 'I'},
		{'L', 'F', 'W', 'C', 'H', 'A', 'A', 'T', 'T', 'R', 'E', 'V', 'A', 'W', 'U', 'F', 'I', 'Y'},
		{'D', 'T', 'H', 'S', 'Z', 'G', 'R', 'C', 'A', 'K', 'G', 'V', 'H', 'J', 'K', 'T', 'M', 'N'},
		{'C', 'X', 'A', 'G', 'N', 'P', 'C', 'R', 'G', 'M', 'I', 'S', 'Z', 'C', 'C', 'Y', 'O', 'L'},
		{'E', 'N', 'C', 'A', 'P', 'S', 'U', 'L', 'A', 'M', 'E', 'N', 'T', 'O', 'J', 'M', 'R', 'V'},
		{'P', 'Y', 'G', 'J', 'I', 'E', 'W', 'H', 'V', 'N', 'G', 'N', 'X', 'X', 'T', 'S', 'F', 'Q'},
		{'T', 'C', 'A', 'C', 'L', 'Y', 'C', 'E', 'E', 'C', 'J', 'U', 'T', 'F', 'U', 'A', 'I', 'Z'},
		{'S', 'O', 'B', 'R', 'E', 'C', 'A', 'R', 'G', 'A', 'V', 'O', 'B', 'O', 'S', 'B', 'S', 'G'},
		{'N', 'T', 'V', 'Z', 'E', 'M', 'E', 'T', 'O', 'D', 'O', 'B', 'J', 'E', 'T', 'O', 'M', 'H'},
		{'B', 'R', 'E', 'F', 'E', 'R', 'E', 'N', 'C', 'I', 'A', 'B', 'T', 'M', 'W', 'G', 'O', 'K'},
		{'P', 'F', 'T', 'Z', 'P', 'Q', 'P', 'I', 'N', 'S', 'T', 'A', 'N', 'C', 'I', 'A', 'O', 'X'}
    };
    /** Palavras para procurar no teste 5 */
	static String Palavra5[]= {
		"ENCAPSULAMENTO", //(8,0)
		"CLASSE",         //(0,5)
		"OBJETO",         //(12,10)
		"INSTANCIA",      //(14,7)
		"OCULTAMENTO",    //(1,3)
		"ARRANJO",        //(5,5)
		"METODO",         //(12,5)
		"ATRIBUTO",       //(0,9)
		"REFERENCIA",     //(13,1)
		"POLIMORFISMO",   //(2,16)
		"SOBRECARGA"      //(11,0)
    };

	public static void main (String args[]) {
		CacadorDePalavras Cacador;
		Dados[] Problema;
		Dados Solucao;

		Cacador=new CacadorDePalavras();
		
		/* Instancia os 5 problemas */
		Problema=new Dados[5];
		Problema[0]= new Dados(Grade1, Palavra1);
		Problema[1]= new Dados(Grade2, Palavra2);
		Problema[2]= new Dados(Grade3, Palavra3);
		Problema[3]= new Dados(Grade4, Palavra4);
		Problema[4]= new Dados(Grade5, Palavra5);
		
		/* Resolve os 5 problemas. */
		for (int i=0;i<5;i++) {
			Problema[i].print();
			Cacador.setProblema (Problema[i]);
			Solucao=Cacador.cacaPalavras();
			Solucao.print ();
		}
	}
}