/*********************************************************************/
/** ACH2001 - Introducao aa Ciencia da Computacao I                 **/
/** EACH-USP - Primeiro Semestre de 2011                            **/
/** <turma> - <nome do professor>                                   **/
/**                                                                 **/
/** Primeiro Exercicio-Programa                                     **/
/** Arquivo: <nome do arquivo>                                      **/
/**                                                                 **/
/** <nome do(a) aluno(a)> <numero USP>                              **/
/**                                                                 **/
/** <data de entrega>                                               **/
/*********************************************************************/

/** Voc� deve implementar os m�todos desta classe conforme a especifica��o 
  * apresentada.
  * Os m�todos foram especificados tendo em vista uma certa estrat�gia de 
  * solu��o: Dadas uma palavra e coordenadas na grade do Problema, testar 
  * se iniciando nessas coordenadas e seguindo na horizontal, vertical, 
  * ou diagonal, a palavra � encontrada. Se for encontrada, ent�o copia 
  * os caracteres da grade do Problema, na mesma posi��o em que se encontram,
  * para a grade do Resultado. Fazendo isso para todas as palavras e todas
  * as posi��es, todas as palavras ser�o encontradas. Desta forma,
  * a grade do Resultado conter�, al�m de espa�os, todas as palavras nas 
  * posi��es da grade do Problema.
  */ 
class CacadorDePalavras {
	/** Defini��o do problema a resolver. */
	Dados Problema;
	/** Refer�ncia para objeto que cont�m a solu��o encontrada. */
	Dados Resultado;
	/** Construtor default: Cria o Ca�ador sem problema definido. */
	CacadorDePalavras () {
	// FAZER
	}
 /** Cria o Ca�ador copiando a refer�ncia para os dados.
   * @param D especifica��o do problema a ser resolvido.
   */
	CacadorDePalavras (Dados D) {
	// FAZER
	}
 
 /** Atribui um problema ao Ca�ador copiando a refer�ncia para
   * os dados.
   * @param D especifica��o do problema a ser resolvido.
   */
	void setProblema (Dados D) {
	// FAZER
	}
 /** Busca a k-�sima palavra da lista de palavras do problema
   * na posi��o (i,j) da grade e com orienta��o horizontal.
   * Caso a palavra tenha sido encontrada, copia seus
   * caracteres nas mesmas posi��es em que foram encontrados
   * para a grade do objeto Resultado. 
   * @param k �ndice da palavra na lista de palavras.
   * @param i linha da grade onde a busca come�a.
   * @param j coluna da grade onde a busca come�a.
   * @return retorna true caso a palavra tenha sido encontrada.
   */
	boolean buscaPalavraHorizontal (int k, int i, int j) {
	// FAZER
	}

 /** Busca a k-�sima palavra da lista de palavras do problema
   * na posi��o (i,j) da grade e com orienta��o vertical 
   * Caso a palavra tenha sido encontrada, copia seus
   * caracteres nas mesmas posi��es em que foram encontrados
   * para a grade do objeto Resultado. 
   * @param k �ndice da palavra na lista de palavras.
   * @param i linha da grade onde a busca come�a.
   * @param j coluna da grade onde a busca come�a.
   * @return retorna true caso a palavra tenha sido encontrada.
   */
	boolean buscaPalavraVertical (int k, int i, int j) {
	// FAZER
	}

 /** Busca a k-�sima palavra da lista de palavras do problema
   * na posi��o (i,j) da grade e com orienta��o diagonal
   * Caso a palavra tenha sido encontrada, copia seus
   * caracteres nas mesmas posi��es em que foram encontrados
   * para a grade do objeto Resultado. 
   * @param k �ndice da palavra na lista de palavras.
   * @param i linha da grade onde a busca come�a.
   * @param j coluna da grade onde a busca come�a.
   * @return retorna true caso a palavra tenha sido encontrada.
   */
	boolean buscaPalavraDiagonal (int k, int i, int j) {
	// FAZER
	}

 /** Busca todas as palavras a partir de todas as posi��es e com todas as 
   * orienta��es.
   * @return Retorna a refer�ncia para o objeto Resultado, que cont�m uma 
   * grade em que as palavras encontradas est�o nas posi��es e orienta��es
   * certas e os caracteres que n�o comp�es essas palavras cont�m o
   * caracter espa�o.
   */
	Dados cacaPalavras () {
	// FAZER
	}
}
