class jurosA {
	public static void main (String[] args) {
		int cont = 1; //contador
		int meses = 15;	//periodo em meses
		double mont = 0;//incia montante
        double jurosA = 1.005;//juros	
		double capt = 10000;//investimento inicial
		while(cont < meses){
			jurosA *= 1.005;
			cont++;		
		}
		mont = capt * jurosA;//calculo do montante
		System.out.println("Após 15 meses você terá na poupança: " + mont);
	}
}
/*
19-) Faça um programa que recebe o valor à vista e aplica juros por um certo tempo. Use-o para resolver:

a-) Você tem R$10000 na caderneta de poupança que rende 0.5% ao mês. Após 15 meses, quanto você terá?

*/