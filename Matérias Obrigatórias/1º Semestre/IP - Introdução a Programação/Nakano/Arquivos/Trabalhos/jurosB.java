class jurosB{
	public static void main (String[] args) {
		int cont = 1;//contador
		int	meses = 6;//periodo em meses
		double mont = 0;//inicia montante
		double jurosB = 1.02;//juros
		double	capt = 10000;//capital investido
		while(cont < meses){
			jurosB *= 1.02;
			cont++;		
		}
		mont = capt * jurosB;// calculo do montante
		System.out.println("Após 6 meses você deverá pagar: " + mont);
	}
}
/*
19-) Faça um programa que recebe o valor à vista e aplica juros por um certo tempo. Use-o para resolver:

b-) Você fez um empréstimo de R$10000 em que o valor emprestado rende (ao banco) 2% ao mês. Se você pagar 
de uma só vez, após 6 meses, quanto terá que pagar?

*/