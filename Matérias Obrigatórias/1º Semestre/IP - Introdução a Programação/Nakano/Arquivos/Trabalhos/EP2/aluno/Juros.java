/*********************************************************************/
/** ACH2001 - Introdução a Programação 								**/
/** EACH-USP - Primeiro Semestre de 2014							**/
/** Turma02 - Fábio Nakano	 										**/
/** 																**/
/** Segundo Exercício-Programa 										**/

/**																	**/

/** 																**/

/*********************************************************************/
import lista.ListaDepositos;
import depositos.Deposito;

/*
	VOCÊ PODE IMPORTAR AS CLASSES QUE DESEJAR AQUI
*/

import depositos.Data;
import lista.ListaSimples;
import lista.No;

class Juros {
	
	/*
		VOCÊ PODE INCLUIR OS MÉTODOS E ATRIBUTOS QUE QUISER, DESDE QUE OS JUROS SEJAM CALCULADOS COM UMA ÚNICA CHAMADA A "juros".
	*/
	/**
		Calcula os juros pagos, a partir de uma seqüência de depósitos e um saldo final, com a precisão indicada. Usa o método de Newton-Raphson para tal.
		
		@param depositos Lista de depósitos (não incluindo o saldo final)
		@param saldo Saldo final da aplicação
		@param epsilon Precisão do cálculo (0 < epsilon < 1)
		
		@return Valor dos juros, ou NAN em caso de erro (epsilon fora dos limites, lista de depósitos sem depósitos, saldo null)
	*/
	public static double juros(ListaDepositos depositos, Deposito saldo, double epsilon) {
		// SEU CÓDIGO DEVE VIR AQUI
		try{
			if ((epsilon > 0) && (epsilon < 1) && (saldo != null) && (depositos != null)){
					No dep; 										// Variavel que permite verificar cada deposito
					double vFuncao; 								// Variavel que armazena o valor da função de j -> f(j)
					double vFuncaoD; 								// Variavel que armazena o valor da função de j diferencial-> f'(j)
					double jurAprox = 0.5;							// Primeiro valor atribuido a juros, juros aproximado
					double jurDesej = 0;							// Variavel que armazena a aproximaçao desejada do juros 
					double difJur = 0;								// Variavel que armazena a diferença absoluta entre os juros aproximado e juros desejado
					int difData = 0;								// Variavel que armazena a diferença de meses entre as datas
					do {
						vFuncao = 0;
						vFuncaoD = 0;
						dep =  depositos.getInicio();
						while(dep != null){							// Executa o calculo de f(j) e f'(j) enquanto houver depositos 
							difData = dep.getDeposito().getData().mesesEntre(saldo.getData());
							vFuncao += dep.getDeposito().getValor() *(Math.pow((1 + jurAprox),difData));
							vFuncaoD += difData * (dep.getDeposito().getValor() *(Math.pow((1 + jurAprox),(difData - 1))));
							dep = dep.getProx();
						}
						vFuncao -= saldo.getValor();
						jurDesej = jurAprox - (vFuncao / vFuncaoD);	// Calcula o novo juros  
						difJur = Math.abs(jurDesej - jurAprox);		// Calcula a diferença de juros
						jurAprox = jurDesej;
					} while ( difJur >= epsilon );
					return jurDesej; // essa linha existe apenas para compilar
			} else {
				return Double .NaN;
			}
		} catch (Exception e){
			return Double .NaN;
		}
	}
	
	public static void main(String[] args) {
		// para seus testes
	}
}

