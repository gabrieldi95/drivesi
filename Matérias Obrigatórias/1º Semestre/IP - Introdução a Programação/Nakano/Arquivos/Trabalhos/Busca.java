class Busca{
	public static void main(String[] args){
		int A[] = {1,5,3,2,0,7,9,4,8,6};
		int z = Integer.parseInt(args[0]);// Pega primeiro valor de args e converte pro tipo inteiro
		buscaArray(A, z);//chama o metodo
	}
	
	static void buscaArray(int[] X, int y){//metodo
		int k=0;//variavel de verificação, caso nao encontre o valor no Array
		for(int i=0; i<X.length; i++){
		    if(y == X[i]){
				System.out.println("O valor "+y+" se encontra na posição " + i +" do vetor.");
			}
			else {
				k++;
			}
		}
		if(k == X.length){//Se k tiver mesmo tamanho que o Array, valor nao foi encontrado
			System.out.println("O valor não se encontra no vetor.");
		}
	}
}