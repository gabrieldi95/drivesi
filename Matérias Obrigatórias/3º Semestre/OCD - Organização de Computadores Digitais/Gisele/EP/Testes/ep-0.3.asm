# radix que ordenar apenas um digito

	.data

source:	.word	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0		# entrada (20 palavras)
dest:	.word 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0		# saida (20 palavras)
array:  .word  	0,0,0,0,0,0,0,0,0,0				# distribuicao dos digitos
index:	.word	0,0,0,0,0,0,0,0,0,0				# lista de indices
src_n	.word	0						# nro de elementos


	.text
 	.globl  main

main:   
        li    $t1,0              # init index to 0
        li    $t2,0              # init loop counter
	li    $v0,0
        
########//coloca zero em todas as posicoes da distribuicao
for1:   beq   $t2,10,block1	 # for ( i=0; i < 20 ;i++ )
        sw    $v0,array($t1)
        addi  $t1,$t1,4          #     increment index
        addi  $t2,$t2,1          #     increment counter
        b     for1

block1: li    $v1,0              # zero para ler distribuicao
        li    $t1,0              # init index to 0
        li    $t2,0              # init loop counter

########//le um int e guarda em source
for2:   beq	$t2,20,block2	# for ( i=0; i < 20 ;i++ )
	li	$v0, 5		# 	read_int --> v0 = digito
	syscall 	
	beq   	$v0,-1,block2	#	if ( v0=-1 ) break;
	nop				else
        sw    	$v0,source($t1)	#	   source[i] = v0
        addi  	$t1,$t1,4	#	   increment index
        addi  	$t2,$t2,1	#	   increment counter
#################################	//incrementa distribuicao	
	lw	$v1,array($v0)	#	   v1 = array[digito]
	addi	$v1,$v1,1	#	   increment v1
	sw	$v1,array($v0)	#	   array[digito] = v1
        b     	for2

block2:	
	sw	$t2, src_n	# guarda o nro de elementos na global
	li	$v0,0		# v0 = 0
	la 	$v1,index	# index[i-1]
	addi	$v1,$v1,-1	#
	la	$v2,array	# array[i-1]
	addi	$v2,$v2,-1	#
        li  	$t1,1           # init index to 1
        li	$t2,0           # init loop counter

#################################//contruir indices
for3:	beq	$t2,10,block3	# for ( i=0; i < 10 ;i++ )
	add	$v0,($v1),($v2) # 	v0 = index[i-1]+array[i-1]
	sw	v0,array($t1)
	addi  	$t1,$t1,4	#     increment index
        addi  	$t2,$t2,1	#     increment counter
	addi	$v1,$v1,4	#	increment index[i]
	addi	$v2,$v2,4	#	increment array[i]
        b     	for3

block3: 
	li    	$t1,0		# init index to 0
        li    	$t2,0		# init loop counter
	li    	$v0,0	
################################# copiar e reordenar a bagaca
for4:	beq   	$t2,20,endfor	# for ( i=0; i < 20 ;i++ )
	lw	$v0,source($t1)	# 	v0 = source[i]
	lw	$v1,index($v0)	# 	v1 = index[source[i]
	la	$v2,dest(v1)	# 	v2* = dest[index[source[i]]
	sw	$v0,($v2)	# 	dest[index[source[i]]] = source[i]
	addi	$v2,$v1,1	# 	v2 = index[source[i]+1
	sw	$v2,($v1)	# 	index[source[i]]=index[source[i]]+1;
        addi  	$t1,$t1,4	#     	increment index
        addi  	$t2,$t2,1	#     	increment counter
        b     	for4

endfor:
        li    	$v0,10		# exit
        syscall

# fim de radix.asm
