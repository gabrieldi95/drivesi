	.text

main:	
	## imprime os nomes dos autores do programa na tela.
	la	$a0, autor_msg		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	## imprime a mensagem "Digite 3 numeros e tecle enter, digite -1 para sair".
	la	$a0, instr_1		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	##############################################################################
	## obter os numeros do usuario, guardar na pilha e incrementar o contador
prox:
	la	$t0, vetor		# ponteiro para o inicio do vetor
	la	$t1, vetor		# ponteiro p que percorre o vetor
	li	$t2, 0			# variavel i=0
	
loop:	
	li 	$v0, 5			# c�digo 5 significa "read_int"
	syscall				# le inteiro
	beq	$v0, -1, imprim		# IF (nroLido == -1) sai do loop
	sw	$v0, ($t1)		# coloca o nroLido na posicao p do vetor
	addu	$t1, $t1, 4		# p aponta para a proxima posicao vazia
	add	$t2, $t2, 1		# i++
	beq	$t2, 3, imprim		# IF (i==3) sai do loop
	b	loop			# ELSE le outro digito

imprim:	
	#chamada comparando 0($t0) com 4($t0)
	# armazena em $a0 o maior
	# compara o menor com o terceiro
	#num loop
	
fim:
li	$v0, 10				# codigo de saida
	syscall				#


	## segmento de dados do programa

	.data
autor_msg:	.asciiz "Leilaaaaa\n"
instr_1:		.asciiz "Digite 3 numeros e tecle enter, digite caractere x para sair\n""

vetor:		.word	0,0,0		# reserva 3 word de 32 bits

# fim de radix.asm
