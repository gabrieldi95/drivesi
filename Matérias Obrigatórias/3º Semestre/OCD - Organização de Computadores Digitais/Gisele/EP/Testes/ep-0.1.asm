# Melina Brilhadori e Murilo Honorio -- 28/11/08
# ep-0.1.asm -- programa que le uma serie de digitos especificados pelo usuario em tempo de execucao
#		exibe os digitos quanto ele digitar 'x'
# Registradores utilizados:
#	t0	- armazena o ultimo digito lido
#	t2	- armazena a quantidade de digitos digitados.
#	v0	- parametros de syscall e valores de retorno
#	a0	- parametro de syscall

	.text				# segmento de codigo do programa, onde ficam as instrucoes

main:					# SPIM come�a a execu��o de um programa pelo r�tulo "main"

	## imprime os nomes dos autores do programa na tela.
	la	$a0, autor_msg		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	## imprime a mensagem "Digite numeros e tecle enter, digite -1 para sair".
	la	$a0, instr_1		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	## obter os numeros do usuario, guardar na pilha e incrementar o contador
loop:	
	li 	$v0, 5			# syscall c�digo 5 significa "read_int"
	syscall				# ler integer
	move	$t0, $v0		# move o numero lido para $t0
	beq	$t0, -1, fim		# IF $t0 == 'x', vai para fim, sen�o

	add	$sp, $sp, -4		# atualiza o ponteiro da pilha
	sw	$t0, 4($sp)		# coloca o conteudo de $t0 na pilha
	add	$t2, $t2, 1		# incrementa o contador de digitos
	b	loop			# le outro digito

	# imprime a quantidade de numeros digitados pelo usuario
fim:
	## imprime o texto "Saida:" na tela.
	la	$a0, result_msg		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	move	$a0, $t2		# coloca o contador em $a0
	li	$v0, 1			# imprime na tela o conteudo de $a0
	syscall

li	$v0, 10				# syscall c�digo 10 significa "exit"
	syscall				# olha o c�digo contido no registrador $v0

	## segmento de dados do programa
	.data
autor_msg:	.asciiz "Melina Brilhadori e Murilo Honorio\n"
instr_1:		.asciiz "Digite numeros e tecle enter, digite caractere x para sair\n"
result_msg:	.asciiz "Saida: "

# fim de radix.asm
