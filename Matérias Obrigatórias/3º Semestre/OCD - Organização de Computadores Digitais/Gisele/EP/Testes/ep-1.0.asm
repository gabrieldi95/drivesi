#########################################
#
# Melina Brilhadori e Murilo Honorio -- 04/12/2009
# ep-1.0.asm 
# Descricao: le um inteiro positivo e imprime por extenso (ignorando zeros a esquerda).
# Registradores:
#	$s0	- armazena o inteiro digitado pelo usuario
#	$s1	- numero de digitos
#	$a0	- parametro de syscall
#	$v0	- parametro de syscall
#	$a1	- argumento de numtexto
#	$t0	- armazena o inteiro temporariamente nas divisoes sucessivas
#	$t1	- armazena o endereco do inicio do armazenamento
#	$t2	- armazena um digito isolado
#	$sp	- topo da pilha
#	$ra	- endereco de retorno
#
#########################################
# segmento de dados

	.data				
aut_msg:	.asciiz "Exercico A.8 de OCD,\npor Melina Brilhadori e Murilo Honorio.\n"
ins_msg:	.asciiz "Digite um numero inteiro: "
inv_msg:	.asciiz "Entrada invalida"
espaco: 	.asciiz " "
n_zero:		.asciiz "Zero"
n_ummm:		.asciiz "Um"	
n_dois:		.asciiz "Dois"
n_tres:		.asciiz "Tres"
n_quat:		.asciiz "Quatro"
n_cinc:		.asciiz "Cinco"
n_seis:		.asciiz "Seis"
n_sete:		.asciiz "Sete"
n_oito:		.asciiz "Oito"
n_nove:		.asciiz "Nove"

#off: sera que da pra implementar com vetores?


#########################################
# segmento de instrucoes

	.text				

numtexto:				# imprime por extenso o numero no argumento $a1
	beq	$a1, 0, zero		# vai para zero se ($a1 == 0)
	beq	$a1, 1, ummm		# vai para ummm se ($a1 == 1)
	beq	$a1, 2, dois		# vai para dois se ($a1 == 2)
	beq	$a1, 3, tres		# vai para tres se ($a1 == 3)
	beq	$a1, 4, quat		# vai para quat se ($a1 == 4)
	beq	$a1, 5, cinc		# vai para cinc se ($a1 == 5)
	beq	$a1, 6, seis		# vai para seis se ($a1 == 6)
	beq	$a1, 7, sete		# vai para sete se ($a1 == 7)
	beq	$a1, 8, oito		# vai para oito se ($a1 == 8)
	beq	$a1, 9, nove		# vai para nove se ($a1 == 9)
zero:	la	$a0, n_zero
	li	$v0, 4
	syscall				# "Zero"
	jr	$ra			# retorna
ummm:	la	$a0, n_ummm
	li	$v0, 4
	syscall				# "Um"
	jr	$ra			# retorna
dois:	la	$a0, n_dois
	li	$v0, 4
	syscall				# "Dois"
	jr	$ra			# retorna
tres:	la	$a0, n_tres
	li	$v0, 4
	syscall				# "Tres"
	jr	$ra			# retorna
quat:	la	$a0, n_quat
	li	$v0, 4
	syscall				# "Quatro"
	jr	$ra			# retorna
cinc:	la	$a0, n_cinc
	li	$v0, 4
	syscall				# "Cinco"
	jr	$ra			# retorna
seis:	la	$a0, n_seis
	li	$v0, 4
	syscall				# "Seis"
	jr	$ra			# retorna
sete:	la	$a0, n_sete
	li	$v0, 4
	syscall				# "Sete"
	jr	$ra			# retorna
oito:	la	$a0, n_oito
	li	$v0, 4
	syscall				# "Oito"
	jr	$ra			# retorna
nove:	la	$a0, n_nove
	li	$v0, 4
	syscall				# "Nove"
	jr	$ra			# retorna


inval:					# imprime mensagem para inteiros negativos
	la	$a0, inv_msg		# coloca o endereco de inv_msg em $a0
	li	$v0, 4			# syscall = 4: "print_string"
	syscall				# mensagem: "Entrada invalida"
	b	fim			# desvia para o fim


separa:					# isola cada digito, colocando na pilha
					# na ordem: unidade, dezena, centena...
	li	$t0, 0			# inicializa $t0 = 0
	add	$t0, $t0, $s0		# $t0 = $s0
repete:	rem	$t2, $t0, 10		# $t2 = ($t0 % 10), separa o digito
	addi 	$sp, $sp, -4		# push: ajusta $sp
	sw 	$t2, 0($sp)		# push: empilha digito
	addi	$s1, $s1, 1		# digitos++
	div	$t0, $t0, 10		# $t0 = ($t0 / 10), exclui o digito
	bgtz	$t0, repete		# se ($t0 > 0) repete
	jr	$ra			# retorna


main:	
	la	$a0, aut_msg		# coloca o endereco de aut_msg em $a0
	li	$v0, 4			# syscall = 4: "print_string"
	syscall				# mensagem: "Exercicio e autores"

	la	$a0, ins_msg		# coloca o endereco de ins_msg em $a0
	li	$v0, 4			# syscall = 4: "print_string"
	syscall				# mensagem: "Digite um numero inteiro:"

	li 	$v0, 5			# syscall = 5: "read_int"
	syscall				# le inteiro
	move	$s0, $v0		# move o inteiro lido para $s0

	blez 	$s0, inval		# se ($s0 < 0), vai para inval
	jal	separa			# desvia para 'separa'

print:	lw	$a1, ($sp)		# pop: desempilha digito para argumento $a1
	addi	$sp, $sp, 4		# pop: ajusta $sp
	jal	numtexto		# desvia para numtexto
	addi	$s1, $s1, -1		# digitos--
	beq	$s1, 0, fim		# se ($s1 == 0) fim
	la	$a0, espaco		
	li	$v0, 4			# syscall = 4: "print_string" 
	syscall				# " "
	b	print			# repete print


fim:	li	$v0, 10			# syscall = 10: "exit"
	syscall				# sair

# fim de radix.asm
