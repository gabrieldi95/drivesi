################################################################################
# COMP 273: Assignment 2
# Author: anonymous
# Winter 2005, Prof. M. Langer
################################################################################
# The merge-sort algorithm
################################################################################

################################################################################
# data - note that str_blank and str_newline are defined for convenience.
################################################################################
		.data
		.align 2
A:		.space 80				# The main working array.
B:		.space 80				# The temporary array.
str_input:	.asciiz "Enter number: "
str_output:	.asciiz "Sorted array: "
str_blank:	.asciiz " "
str_newline:	.asciiz "\n"

		.text
		.globl main

################################################################################
# merge - merges two subarrays of array A. $a0 is the index of the first element
# of the left part (p). $a1 is the index of the first element of the right part
# (q). $a2 is the index of the element immediately following the end of the
# array (r). this function assumes that p <= q <= r.
################################################################################ 
merge:		addi	$sp, $sp, -20			# first, we back up $s0 to $s4, since we'll be using them.
		sw	$s0, 16($sp)			# we won't back up any other registers here, since merge
		sw	$s1, 12($sp)			# is not called recursively. indeed, no other functions
		sw	$s2, 8($sp)			# are called from merge. so $ra or any other registers are
		sw	$s3, 4($sp)			# not tampered with during one call to merge.
		sw	$s4, 0($sp)

		la	$s0, A				# $s0 is always the beginning address of A.
		la	$s1, B				# $s1 is always the beginning address of B.

		sll	$s2, $a0, 2			# $s2 is always the byte offset of p from the beginning.
		sll	$s3, $a1, 2			# $s3 is always the byte offset of q from the beginning.
		sll	$s4, $a2, 2			# $s4 the byte offset of r from the beginning.
							# nb: multiplying the indices by 4 gives us the byte offsets.

		add	$t0, $s2, $s0			# $t0 is a pointer to move along the A subarray...
		add	$t1, $s2, $s1			# $t1 is a pointer to move along the B subarray...
		add	$t2, $s4, $s0			# and $t2 is the address after the end of the A subarray.

							# the merge_l_1 loop just copies array A to array B.
merge_l_1:	lw	$t3, 0($t0)			# take the next word from the A subarray...
		sw	$t3, 0($t1)			# and put it in the corresponding slot in the B subarray...
		addi	$t0, 4				# then increment one offset...
		addi	$t1, 4				# then the other.
		beq	$t0, $t2, merge_l_1_done	# if we've moved past the end of the subarray in A, break.
		j	merge_l_1			# loop.

							# after merge_l_1, we prepare to merge. this is done in the merge_l_2 loop.
merge_l_1_done:	add	$t0, $s2, $s1			# let $t0 be our pointer in the first part of the B subarray...
		add	$t1, $s3, $s1			# let $t1 be our pointer in the second part of the B subarray...
		add	$t2, $s2, $s0			# let $t2 be our pointer in the A subarray, where we write things...
		add	$t3, $s4, $s0			# let $t3 be the end-address-pointer for the A subarray...
		add	$t4, $s3, $s1			# let $t4 be the end-address-pointer for the first part of the A subarray...
		add	$t5, $s4, $s1			# and let $t5 be the end-address-pointer for the second part of the B subarray.

merge_l_2:	beq	$t2, $t3, merge_l_2_done	# if the entire A subarray's been filled, we're done, break.
		lw	$t6, 0($t0)			# load the next entry in the first part of the B subarray...
		lw	$t7, 0($t1)			# and the next entry in the second....
		beq	$t0, $t4, case_2		# if the first part of the B subarray's been done, try the second.
		beq	$t1, $t5, case_1		# if the second part of the B subarray's been done, try the first.
							# nb: these ``full'' conditions override any comparisons.
		ble	$t6, $t7, case_1		# if that of the first part's smaller, jump to case_1...
		bgt	$t6, $t7, case_2		# otherwise, jump to case_2.
		j	merge_l_2			# loop.

							# in case_1, we take the next entry in A from the first B subarray.
case_1:		sw	$t6, 0($t2)			# write the entry from the first B subarray into the new A subarray...
		addi	$t2,4				# increment both entry pointers...
		addi	$t0,4
		j	merge_l_2			# and jump back.

							# in case_2, we take the next entry in A from the second B subarray.
case_2:		sw	$t7, 0($t2)			# write the entry from the second B subarray into the new A subarray...
		addi	$t2,4				# increment both entry pointers...
		addi	$t1,4
		j	merge_l_2			# and jump back.

merge_l_2_done:	
		lw	$s0, 16($sp)			# restore the $s arrays from the stack.
		lw	$s1, 12($sp)
		lw	$s2, 8($sp)
		lw	$s3, 4($sp)
		lw	$s4, 0($sp)
		addi	$sp, $sp, 20			# push the stack pointer back up.
		jr	$ra				# return.

################################################################################
# merge sort - recursively sorts a portion of an array. $a0 is the left index
# and $a1 is the right index, where 0 denotes the first entry in the array.
################################################################################
merge_sort:	addi	$sp, $sp, -32			# first, we back up the $s registers.
		sw	$s0, 28($sp)			# this time, we'll also do the $a and $ra registers.
		sw	$s1, 24($sp)			# after all, we'll certainly be calling merge-sort
		sw	$s2, 20($sp)			# recursively. i'll preserve all the $a registers,
		sw	$ra, 16($sp)			# even though we don't need all of them.
		sw	$a0, 12($sp)
		sw	$a1, 8($sp)
		sw	$a2, 4($sp)
		sw	$a3, 0($sp)
		

		sub	$s0, $a1, $a0			# get the difference from $a0 to $a1.
		ble	$s0, 1, merge_sort_ret		# if it's 1, we're at the base case, so we break.
		add	$s0, $a0, $a1			# if not, we'll call merge sort recursively...
		srl	$s0, $s0, 1			# first obtaining the midpoint of the arguments.
		move	$s1, $s0			# we shift the arguments around such that they're
		move	$s0, $a0			# always conveniently accessible in the $s0 to $s2
		move	$s2, $a1			# registers (specifically, $a0 is in $s0, $a1 in
		move	$a1, $s1			# $s1 and $a2 in $s2).
		jal	merge_sort			# first call, on the first half of the subarray.

		move	$a0, $s1			# set the arguments for the second call...
		move	$a1, $s2
		jal	merge_sort			# second call, on the second half of the subarray.

		move	$a0, $s0			# set the arguments for the call to merge...
		move	$a1, $s1
		move	$a2, $s2
		jal	merge				# merging the results of the above calls.
		
merge_sort_ret:	lw	$s0, 28($sp)			# restore all the registers from the stack.
		lw	$s1, 24($sp)
		lw	$s2, 20($sp)
		lw	$ra, 16($sp)
		lw	$a0, 12($sp)
		lw	$a1, 8($sp)
		lw	$a2, 4($sp)
		lw	$a3, 0($sp)
		addi	$sp, $sp, 32			# remember to push the stack pointer back to where it belongs.
		jr	$ra				# done.

################################################################################
# main - takes integer input from the user up to 20 numbers, then sorts them
# using merge sort and returns the sorted list to the console.
################################################################################
main:		la	$s0, A				# $s0 will always contain the beginning of A.
		move	$s1, $s0			# $s1 will be a moving pointer, used to write the input into A.
		
input_loop:	li	$v0, 4				# prints out a string through syscall.
		la	$a0, str_input			# the string we want to write goes into $a0.
		syscall					# print.
		li	$v0, 5				# takes integer input through syscall.
		syscall					# read.
		beq	$v0, $zero, input_loop_end	# if a zero is entered, we stop collecting numbers and sort...
		sw	$v0, 0($s1)			# otherwise, we keep collecting
		add	$s1, $s1, 4			# increment the moving pointer.
		j	input_loop			# loop.

input_loop_end:	beq	$s1, $s0, input_loop		# cheeky! someone entered only a zero. this won't do. jump back into the collecting loop.
		li	$a0,0				# the first p value is always 0.
		move	$s2, $s1			# we'll want to know how the last address in our array later. $s2 will remember it.
		sub	$s1, $s1, $s0			# computing the byte size from the memory addresses (last address - beginning address = size in bytes).
		srl	$a1, $s1, 2			# computing the max index from the byte size (divide by 4).

		jal	merge_sort			# and off we go.

		li	$v0, 4				# prints out a string through syscall.
		la	$a0, str_output			# we want to print str_output. it goes into $a0.
		syscall					# print.

		move	$s1, $s0			# $s1 will be the moving pointer again. we init it to the beginning of A.
print_loop:	li	$v0, 1				# tell syscall to print an integer.
		lw	$a0, 0($s1)			# get the next entry in the new A subarray, indexed by $s1.
		syscall					# print.
		li	$v0, 4				# we'll print blanks between the sorted integers.
		la	$a0, str_blank			# puts the blank into $a0.
		syscall					# blank.
		add	$s1, $s1, 4			# increment $s1 to next entry.
		beq	$s1, $s2, print_loop_end	# if we've moved past the end, break.
		j	print_loop			# loop

print_loop_end:	la	$a0, str_newline		# let's tack on two newlines at the end of the output. this makes the program's output look
		syscall					# better if we run it several times in one console window, in one spim session.
		syscall
		exit: