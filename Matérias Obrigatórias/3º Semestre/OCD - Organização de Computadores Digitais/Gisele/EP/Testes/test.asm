# Melina Brilhadori e Murilo Honorio -- 28/11/08
# radix.asm -- 	programa que computa e imprime a soma de dois numeros
#		especificados pelo usuario em tempo de execucao
# Registradores utilizados:
#	t0	- utilizado para armazenar o primeiro numero.
#	t1	- utilizado para armazenar o segundo numero
#	t2	- utilizado para armazenar a soma de $t1 e $t2.
#	v0	- parametros de syscall e valores de retorno
#	a0	- parametro de syscall

	.text				# segmento de codigo do programa, onde ficam as instrucoes

main:					# SPIM come�a a execu��o de um programa pelo r�tulo "main"

	## imprime os nomes dos autores do programa na tela.
	la	$a0, autor_msg		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	## imprime a mensagem "insira dois numeros inteiros".
	la	$a0, instr_1		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	## obter o primeiro numero do usuario, guardar em $t0
	li 	$v0, 5			# syscall c�digo 5 significa "read_int"
	syscall				# ler integer
	move	$t0, $v0		# mover o numero lido para $t0

	## obter o segundo numero do usuario, guardar em $1.
	li	$v0, 5			# syscall 5 -> "read_int"
	syscall				# ler integer
	move	$t1, $v0		# mover o numero lido para $t1

	## condicional IF(t0 > t1) ELSE.
	bgt	$t0, $t1, if	# IF $t0 > $t1, vai para if_t0_maior
	move	$t2, $t1		# ELSE move $t1 para $t2
	b	endif			# branch para endif ou termina o IF-ELSE

if:					# codigo do IF
	move	$t2, $t0		# move $t0 para $t2
endif:

	## imprime o texto "Resposta:" na tela.
	la	$a0, result_msg		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	## imprimir $t2 na tela
	move	$a0, $t2		# mover o numero que sera impresso para $a0
	li	$v0, 1			# syscall c�digo 1 significa "print_int"
	syscall				# imprime na tela o conteudo de $a0

	li	$v0, 10			# syscall c�digo 10 significa "exit"
	syscall				# olha o c�digo contido no registrador $v0


	## segmento de dados do programa
	.data
autor_msg:	.asciiz "Melina Brilhadori e Murilo Honorio\n"
instr_1:		.asciiz "Entre com dois inteiros\n"
result_msg:	.asciiz "Resposta: "

# fim de radix.asm
