## Algoritmo "Counting Sort" implementado em assembly MIPS
## Referencia: http://en.wikipedia.org/wiki/Counting_sort
## Testado no simulador PCSpim 7.0 para Windows rodando via Wine 0.9.43 no Ubuntu 7.04 - PCSpim: http://pages.cs.wisc.edu/~larus/spim.html

## Marco Santos Souza - sms.cpp@gmail.com - http://www.inf.ufsc.br/~sms/

.data
	_space: .asciiz " "													# string usada pela rotina de impressao
	_array: .word 2, 5, 5, -1, 10, -9, 3, 4, 4, 34, 11, -2, -1			# array que sera ordenado
	_array_size: .word 13												# tamanho do array



.text
.globl main

# int main(int argc, char **argv)
main:
	addi $sp, $sp, -4				# abre espaco na pilha
	sw $ra, 0($sp)					# salva $ra na pilha

	#la $a0, _array					# primeiro argumento para rotina de impressao
	#lw $a1, _array_size			# segundo argumento para rotina de impressao
	#jal printArray					# chama rotina para imprimir array inicial (altera $ra) # OBS: rotina chamada nao preservara os valores nos registradores $a0 e $a1

	la $a0, _array					# primeiro argumento para rotina de ordenacao
	lw $a1, _array_size				# segundo argumento para rotina de ordenacao
	jal countingSort				# chama rotina de ordenacao
	
	move $a0, $v0					# primeiro argumento para rotina de impressao # $v0 == retorno de countingSort
	lw $a1, _array_size				# segundo argumento para rotina de impressao
	jal printArray					# chama rotina para imprimir array ordenado
	
	lw $ra, 0($sp)					# restaura valor de $ra
	addi $sp, $sp, 4				# restaura pilha

	move $v0, $zero					# return 0 (convencao em C)
	jr $ra							# retorna ao caller
### fim main



# void countingSort(const int A[], const int numOfElements)
## argumentos:
## $a0 -> endereco base do array a ordenar (nao altera array)
## $a1 -> tamanho do array
## retorno -> $v0 == endereco base de um novo array ordenado
countingSort:
	### se array tem tamanho menor que dois entao vai para fim da rotina e retorna
		li $t0, 2						# carrega constante 2 em $t0
		slt $t1, $a1, $t0				# se tamanho do array maior ou igual a 2 (o esperado) entao $t1 igual a 0
		bne $t1, $zero, cs_end			# se nao for o esperado ($t1 != 0) entao vai para fim e retorna
	###

	addi $sp, $sp, -20					# abre espaco na pilha
	sw $s0, 0($sp)						# salva $s0
	sw $s1, 4($sp)						# salva $s1
	sw $s2, 8($sp)						# salva $s2
	sw $s3, 12($sp)						# salva $s3
	sw $ra, 16($sp)						# salva $ra

	move $s0, $a0						# $s0 = $a0 # $s0 = &A[0]
	sll $s1, $a1, 2						# $s1 = tamanho do array em bytes
	add $s1, $s1, $s0					# $s1 = endereco do final do array

	### procurar pelo menor e pelo maior valor no array
		lw $t8, 0($s0)						# $t8 = A[0] ($t8 == min)
		move $t9, $t8						# $t9 = $t8 (max = min -- $t9 == max)

		move $t0, $s0						# $t0 = copia do endereco base do array
		addi $t0, $a0, 4					# incrementa endereco base e guarda em $t0 # $t0 = &A[1]

		cs_firstLoopBegin:
			lw $t1, 0($t0)					# $t1 = A[i] (ou algo como "$t1 = *$t0")

			slt $t2, $t1, $t8				# se $t1 < $t8 entao $t2 == 1 # se A[i] < min entao $t2 == 1
			beq $t2, $zero, cs_firstElse	# se $t5 == 0 ( ou !(A[i] < min) ) pular para cs_firstElse
			move $t8, $t1					# min = A[i] ($t8 = $t1)

			cs_firstElse:
				slt $t2, $t9, $t1				# se $t8 < $t1 entao $t2 == 1 # se A[i] > max entao $t2 == 1
				beq $t2, $zero, cs_endElseIf	# se $t5 == 0 ( ou !(A[i] > max) ) pular para cs_endElseIf
				move $t9, $t1					# max = A[i] ($t9 = $t1)
				
			cs_endElseIf:

			addi $t0, $t0, 4					# incrementa endereco para pegar endereco do proximo elemento do array
			bne $t0, $s1, cs_firstLoopBegin		# se ainda nao eh o fim do array, reinicia loop

	###	resultado -> $t8 == min e $t9 == max

	sub $s3, $t9, $t8							# $s3 = max - min
	addi $s3, $s3, 1							# $s3 = max - min + 1

	sll $s3, $s3, 2								# $s3 = $s3 * 4 # tamanho em bytes para um novo array temporario (arrayTemp)

	li $v0, 9									# codigo syscall para sbrk em $v0
	move $a0, $s3								# quantidade de bytes que deseja alocar no heap colocada em $a0
	syscall										# $v0 = new int[$a0]									

	move $s2, $v0								# $s2 = endereco base do novo array temporario (arrayTemp)

	### zerar elementos de arrayTemp
		move $t0, $s2							# copia endereco base de arrayTemp em $t2
		add $t1, $s2, $s3						# $t1 = endereco do final de arrayTemp
			
		cs_secondLoopBegin:
			sw $zero, 0($t0)					# zera elemento de arrayTemp
			addi $t0, $t0, 4					# incrementa endereco para pegar endereco do proximo elemento do array
			bne $t0, $t1, cs_secondLoopBegin	# se ainda nao eh o fim do array, reinicia loop
	###

	### for (int i = 0; i < tamanho do array A; ++i) ++arrarTemporario[ A[i] - min ];
		move $t0, $s0							# copia endereco base do array A em $t0

		cs_thirdLoopBegin:
			lw $t1, 0($t0)						# $t1 = A[i]
			sub $t1, $t1, $t8					# $t1 = A[i] - min
			sll $t1, $t1, 2						# $t1 = $t1 * 4
			add $t2, $s2, $t1					# $t2 = &arrarTemporario[ A[i] - min ]
			lw $t3, 0($t2)						# $t3 = *$t2
			addi $t3, $t3, 1					# ++$t3
			sw $t3, 0($t2)						# arrarTemporario[ A[i] - min ] = $t3

			addi $t0, $t0, 4					# incrementa endereco para pegar endereco do proximo elemento do array
			bne $t0, $s1, cs_thirdLoopBegin		# se ainda nao eh o fim do array, reinicia loop
	###

	### for (int i = 1; i < tamanho de arrayTemp; ++i) arrayTemp[i] += arrayTemp[ i - 1 ];
		add $t1, $s2, $s3						# $t1 = endereco final de arrayTemp
		addi $t0, $s2, 4						# $t0 = &arrayTemp[1]

		cs_fourthLoopBegin:
			addi $t3, $t0, -4					# $t3 = &A[i-1]
			lw $t3, 0($t3)						# $t3 = A[i-1]
			lw $t2, 0($t0)						# $t2 = A[i]

			add $t2, $t2, $t3					# $t2 = arrayTemp[i] + arrayTemp[ i - 1 ];
			sw $t2, 0($t0)						# arrayTemp[i] = $t2;

			addi $t0, $t0, 4					# incrementa endereco para pegar endereco do proximo elemento do array
			bne $t0, $t1, cs_fourthLoopBegin	# se ainda nao eh o fim do array, reinicia loop
	###

	### criar no heap array que sera retornado
		li $v0, 9										# codigo syscall para sbrk em $v0
		sub $a0, $s1, $s0								# tamanho em bytes do array A em $a0
		syscall											# $v0 = new int[$a0]
	###

	### for (int i = tamanho do array A - 1; i >= 0; --i)
		addi $t0, $s1, -4								# $t0 = &A[tamanho de A - 1]
		addi $t1, $s0, -4								# $t1 = (A-1)

		cs_fifthLoopBegin:
			lw $t2, 0($t0)							# $t2 = A[i]
			sub $t3, $t2, $t8						# $t3 = A[i] - min
			sll $t3, $t3, 2							# $t3 = $t3 * 4
			
			add $t3, $s2, $t3						# $t3 = &arrayTemp[ A[i] - min ] ]
			lw $t4, 0($t3)							# $t4 = arrayTemp[ A[i] - min ] ]
			addi $t4, $t4, -1						# $t4 = $t4 - 1
			sw $t4, 0($t3)							# salva $t4
			sll $t4, $t4, 2							# $t4 = $t4 * 4

			add $t3, $v0, $t4						# $t3 = &sortedArray[ --arrayTemp[ A[i] - min ] ]
			sw $t2, 0($t3)							# sortedArray[ --arrayTemp[ A[i] - min ] ] = A[i]

			addi $t0, $t0, -4
			bne $t0, $t1, cs_fifthLoopBegin
	###

	lw $s0, 0($sp)									# restaura $s0
	lw $s1, 4($sp)									# restaura $s1
	lw $s2, 8($sp)									# restaura $s2
	lw $s3, 12($sp)									# restaura $s3
	lw $ra, 16($sp)									# restaura $ra
	addi $sp, $sp, 20								# restaura $sp

	cs_end:
	jr $ra											# retorna ao caller
## fim countingSort



# void printArray(const int A[], const int numOfElements)
## argumentos:
## $a0 -> endereco base do array a imprimir (nao altera array)
## $a1 -> tamanho do array
printArray: 						# rotina de debug para imprimir _array
	beq $a1, $zero, pa_exit			# se array tem tamanho igual a zero entao pular loop e retornar

	move $t0, $a0					# copia endereco base do array em $t0
	sll $t1, $a1, 2					# tamanho do array multiplicado por 4 (para obter tamanho em bytes)
	add $t1, $t0, $t1				# endereco do final do array em $t1 -- baseArray + tamanhoEmBytes

	la $t2, _space					# endereco da string rotulada por _space (" ") em $t2

	pa_loopBegin:
		li $v0, 1					# codigo syscall para print_int
		lw $a0, 0($t0)				# argumento para print_int em $a0
		syscall						# chama print_int

		li $v0, 4					# codigo syscall para print_string
		move $a0, $t2				# endereco da string passado como argumento para print_string
		syscall						# chama print_string

		addi $t0, $t0, 4			# incrementa endereco para pegar endereco do proximo elemento do array
		bne $t0, $t1, pa_loopBegin	# se ainda nao eh o fim do array, reinicia loop

	pa_exit:
 	jr $ra							# retorna ao caller
## fim printArray
