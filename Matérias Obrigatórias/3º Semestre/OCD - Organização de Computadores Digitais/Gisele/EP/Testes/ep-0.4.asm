# Melina Brilhadori e Murilo Honorio -- 04/12/2009
# ep-0.5.asm 
# Descricao: le um inteiro positivo
# Registradores:
#	$s0	- armazena o inteiro digitado pelo usuario
#	$s1	- numero de digitos
#	t1	- armazena o endereco do inicio do armazenamento
#	t2	- armazena a quantidade de vetor digitados.
#	v0	- parametros de syscall e valores de retorno
#	a0	- parametro de syscall
	

##########################################
###   SEGMENTO DE DADOS DO PROGRAMA    ###
##########################################

	.data
aut_msg:	.asciiz "Melina Brilhadori e Murilo Honorio\n"
ins_msg:	.asciiz "Digite um numero inteiro: "
inv_msg:	.asciiz "Entrada invalida"

index:		.word	0,0,0,0,0,0,0,0,0,0

vetor:		.space	160		# reserva 40 palavras (160 bytes) a 1a. eh o contador
tam_vetor:	.word	160

str_1: 		.asciiz " "
palavra: 	.space 	40


##########################################
### SEGMENTO DE INSTRUCOES DO PROGRAMA ###
##########################################
	.text
	.globl main


inval:	la	$a0, inv_msg		# coloca o endereco de inv_msg em $a0
	li	$v0, 4			# syscall = 4: "print_string"
	syscall				# mensagem: "Entrada invalida"
	b	fim			# desvia para o fim

separa:	
	li	$t0, 0			# reinicializa $t0 = 0
	add	$t0, $t0, $s0		# $t0 = $s0
	li	$t1, 10			# $t1 = 10
loop:	rem	$t2, $s0, $t0		# $t2 = ($s0 % 10), separa o ultimo digito
	addi 	$sp, $sp, -4		# atualiza a pilha
	sw 	$t2, 0($sp)		# coloca digito na pilha
	addi	$s1, $s1, 1		# digitos++
	div	$t0, $t0, $t1		# $t0 = ($t0 / 10), exclui ultimo digito
	bgtz	$t0, loop		# se ($t0 > 0) loop
	jr	$ra
	
main:	
	la	$a0,aut_msg		# coloca o endereco de aut_msg em $a0
	li	$v0,4			# syscall = 4: "print_string"
	syscall				# mensagem: "Melina Brilhadori e Murilo Honorio\n"

	la	$a0,ins_msg		# coloca o endereco de ins_msg em $a0
	li	$v0,4			# syscall = 4: "print_string"
	syscall				# mensagem: "Digite um numero inteiro: "

	li 	$v0, 5			# syscall = 5: "read_int"
	syscall				# le inteiro
	move	$s0, $v0		# move o inteiro lido para $s0

	blez 	$s0, inval		# se ($s0 < 0), vai para inval
	jal	separa			# devia para 'separa'

print:
	lw	$a0, ($sp)		# coloca o conteudo apontado por $sp em $a0
	addi	$sp, $sp, 4		# avan�a o ponteiro
	li	$v0, 1			# imprime na tela o conteudo de $a0
	syscall
	subi	$s1, $s1, 1		# digitos--
	beq	$s1, 0, fim		# se ($s1 == 0) fim
	b	print			

fim:	li	$v0,10			# syscall = 10: "exit"
	syscall				# sair

# fim de radix.asm
