# Melina Brilhadori e Murilo Honorio -- 28/11/08
# ep-0.1.asm -- cria 10 vetores, uma para cada digito, le os vetor do usuario e armazena no vetor apropriado
# Registradores utilizados (atualizar):
#	t0	- armazena o ultimo digito lido
#	t1	- armazena o endereco do inicio do armazenamento
#	t2	- armazena a quantidade de vetor digitados.
#	v0	- parametros de syscall e valores de retorno
#	a0	- parametro de syscall
	

	.text

main:	
	## imprime os nomes dos autores do programa na tela.
	la	$a0, autor_msg		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	## imprime a mensagem "Digite numeros e tecle enter, digite -1 para sair".
	la	$a0, instr_1		# coloca o endereco de hello_msg em $a0
	li	$v0, 4			# syscall c�digo 4 significa "print_string"
	syscall

	##############################################################################
	### Inicializar o vetor de palavras com -1

	la	$t0, vetor		# ponteiro para o inicio do vetor
	sub	$t1, $t1, 1		# coloca -1 na constante $t1
loop0:
	sw	$t1, ($t0)		# coloca -1 na posicao apontada por p
	addu	$t0, $t0, 4		# proxima posicao
	beq	$t0, 160, prox		# encerra quando $t0==160

	##############################################################################
	## obter os numeros do usuario, guardar na pilha e incrementar o contador
prox:
	add	$t0, $zero, $zero	# ponteiro para a posicao adequada
	la	$t1, vetor		# ponteiro para o inicio do vetor
	la	$t2, vetor		# ponteiro p que percorre o vetor
	add	$t3, $zero, $zero	# variavel i=0
	lw	$t4, tam_vetor		# parametro tam_vetor
	
loop:	
	li 	$v0, 5			# c�digo 5 significa "read_int"
	syscall				# le inteiro
	beq	$v0, -1, imprim		# IF (nroLido == -1) sai do loop
	nop				# ELSE
	sw	$v0, ($t2)		# coloca o nroLido na posicao p do vetor
	addu	$t2, $t2, 4		# p aponta para a proxima posicao vazia
	add	$t3, $t3, 1		# i++
	beq	$t3, $t4, imprim	# IF (i==tam_vetor) sai do loop
	b	loop			# ELSE le outro digito

	##############################################################################

imprim:	
	beq	$t1, $t2, fim		# se $t1 == $t2 encerra o programa
	nop				# sen�o
	lw	$a0, ($t1)		# coloca o conteudo apontado por $t1 em $a0
	add	$t1, $t1, 4		# avan�a o ponteiro
	li	$v0, 1			# imprime na tela o conteudo de $a0
	syscall
	b	imprim

fim:
li	$v0, 10				# syscall c�digo 10 significa "exit"
	syscall				# olha o c�digo contido no registrador $v0


	## segmento de dados do programa

	.data
autor_msg:	.asciiz "Melina Brilhadori e Murilo Honorio\n"
instr_1:		.asciiz "Digite numeros e tecle enter, digite caractere x para sair\n"
result_msg:	.asciiz "Saida: "

vetor:		.space	160		# reserva 40 palavras (160 bytes) a 1a. eh o contador
tam_vetor:	.word	160

# fim de radix.asm
