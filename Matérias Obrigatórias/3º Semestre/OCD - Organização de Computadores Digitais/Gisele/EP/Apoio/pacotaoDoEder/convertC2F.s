##
## 	The program --- convertC2F.s 
##
##		- will ask the user for a temperature in Celsius,
##		- convert it to Fahrenheit, and
##		- print the result.
##
##		Here is the formula of the conversion:
##              F = (9*C/5)+32
##
##		v0 - reads in Celsius
##		t0 - holds Fahrenheit result
##      	a0 - points to output strings
##

#################################################
#                                               #
#               text segment                    #
#                                               #
#################################################

        .text
        .globl __start
__start:                # execution starts here

        la $a0,prompt   # print prompt on terminal
        li $v0,4        # system call to print
        syscall         # out a string

        li $v0,5	# syscall 5 reads an integer
        syscall         

	mul $t0,$v0,9 	# to convert, multiply by 9,
	div $t0,$t0,5 	# divide by 5, then
	addi $t0,$t0,32  # add 32

	la $a0,ans1	# print string before result
	li $v0,4
	syscall 

	move $a0,$t0	# print result
	li $v0,1
	syscall

	la $a0,endl	# syscal to print out
	li $v0,4	# a new line
	syscall 

	li $v0,10
	syscall		# Bye!


#################################################
#                                               #
#               data segment                    #
#                                               #
#################################################

        .data
	prompt:	.asciiz "Enter temperature (Celsius): "
	ans1:	.asciiz "The temperature in Fahrenheit is "
	endl:	.asciiz "\n"	   

##
## 	end of file convertC2F.s
