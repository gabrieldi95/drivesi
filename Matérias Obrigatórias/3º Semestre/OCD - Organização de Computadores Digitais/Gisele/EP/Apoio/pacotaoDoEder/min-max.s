##
## 	Nome do Programa: min-max.s  
##
##		- Ir� imprimir min o valor m�nimo 
##		- E o m�ximo valor m�ximo de uma matriz. 
##		
##		- Suponha que a matriz tem pelo menos dois elementos (a [0] e a [1]).  
##		- Ele inicializa ambos min e max para a [0] e ent�o
##		- Atravessa a contagem de loop - 1 vezes. 
##		- Este programa ir� usar ponteiros. 
##		
##
##		t0 - apontam para elementos da matriz, por sua vez
##		t1 - cont�m contagem dos elementos 
##      	t2 - cont�m min
##		t3 - cont�m max
##		t4 -  cada palavra da matriz, por sua vez
##

#################################################
#                                               #
#                Segmento do texto              #
#                                               #
#################################################

        .text
        .globl __start
__start:           	   	# execu��o come�a aqui

	la $t0,array		# $t0 ir� apontar para os elementos 
	lw $t1,count		# contagem de loop de sa�da quando $t1 == 0 
	lw $t2,($t0)		# inicializa min = a[0]
	lw $t3,($t0)		# inicializa max = a[0]
	add $t0,$t0,4		# ponteiro para iniciar em a[1]
	add $t1,$t1,-1		# e vai contar rodada - 1 vezes
	
        
loop:	lw $t4,($t0)		# carga pr�xima palavra da matriz 
	bge $t4,$t2,notMin	# saltar se a[i]>=min 
	move $t2,$t4		# copiar a[i] para min

notMin:	ble $t4,$t3,notMax	# saltar se a[i] <= max 
	move $t3,$t4		# copiar a[i] para max


notMax:	add $t1,$t1,-1		# decrementa contador
	add $t0,$t0,4		# ponteiro incremento por palavra
	bnez $t1, loop		# continue se contador > 0


	la $a0,ans1   		# imprime no prompt do terminal
	li $v0,4		# chamada de sistema para imprimir 
	syscall			# saida "min = "

               
	move $a0,$t2		# resultado da impressao min
	li $v0,1
	syscall
	
	
	la $a0,ans2		# imprime saida "max = "
	li $v0,4
	syscall 

	move $a0,$t3		# resultado da impressao max
	li $v0,1
	syscall

	la $a0,endl		# chamada de sistema para imprimir saida
	li $v0,4		# uma nova linha
	syscall 

	li $v0,10		# Saida
	syscall			# Chau!


#################################################
#                                               #
#               data segment                    #
#                                               #
#################################################

        .data
	array:	.word 3,4,2,6,12,7,18,26,2,14,19,7,8,12,13
	count:	.word 15
	ans1:	.asciiz "min = "
	ans2:	.asciiz "\nmax = "
	endl:	.asciiz "\n"	   

##
## 	end of file min-max.s
