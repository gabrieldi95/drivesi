import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.*;



class Principal {
	static int contador = 0;	// variavel global usada para atribuição de valores no metodo coordenada
	public static void main(String[] args) {
		
		int numVertices = 0;		// guarda o número de vértices
		int numArestas = 0;			// guarda o número de arestas
		String chave;				// variável usada para armazenar temporariamente cada linha da entrada
		FileReader f;				//
		BufferedReader arquivo;		//
		try {
			f = new FileReader( "input.txt" );					// lê o arquivo
			arquivo = new BufferedReader( f );
			chave = arquivo.readLine();							// lê a primeira linha da entrada
			String[] array = chave.split(" ");					// armazena os valores separados em um array
			numVertices = Integer.parseInt( array[0] );			// guarda o primeiro valor na variável de vertices
			Grafo g = new Grafo( numVertices );					// Cria grafo de tamanho numVertices
			numArestas = Integer.parseInt( array[1] );			// guarda o segundo valor na variável de arestas
			chave = arquivo.readLine();							// "Pula" 2 linhas
			chave = arquivo.readLine();
			
			for ( int i = 0; i < numVertices; i++) {
				String[] coordenadas = chave.split( " " );
				// cria um vértice com os valores passados
				Vertice v = new Vertice( Integer.parseInt( coordenadas[0] ), Integer.parseInt( coordenadas[1] ), Integer.parseInt( coordenadas[2] ) );
				g.vertices.add( v ); 								// adiciona o vértice

				chave = arquivo.readLine();

			}
			Grafo auxiliar = new Grafo(0);
			for ( int i = 0; i < numArestas; i++) {					//
				String[] arestas = chave.split( " " );				//
				chave = arquivo.readLine();
				Vertice v1 = new Vertice(Integer.parseInt( arestas[0] ),0,0);
				Vertice v2 = new Vertice(Integer.parseInt( arestas[1] ),0,0);
				Aresta ar = new Aresta( v1, v2);
				auxiliar.inserirAresta(Integer.parseInt( arestas[0] ), Integer.parseInt( arestas[1] ));						
			}
			chave = arquivo.readLine(); 							//
			String[] origem_destino = chave.split( " " );			//
			int origem = Integer.parseInt( origem_destino[0] );		//
			int destino = Integer.parseInt( origem_destino[1] );	//
			Dijkstra d = new Dijkstra();
			
			BufferedWriter out = new BufferedWriter(new FileWriter("saida.txt"));
//			int numVerticesUsados = 0;
			Dijkstra d_auxiliar = new Dijkstra();
			ListIterator iterad = d_auxiliar.caminho.listIterator(d_auxiliar.caminho.size());
			Vertice aux = new Vertice (0,0,0);
			while (iterad.hasPrevious()){
				aux=(Vertice) iterad.previous();
				out.write(aux.indice+ " " + aux.antecessor+"\r\n"); // Imprimir vertices
				aux=(Vertice)iterad.previous();
			}
			Vertice fi_nal = d.buscaVertice(g, destino);
			out.write(fi_nal.peso+"\r\n"); // Imprimir vertices
			// Escrever linha em branco (Enter)
			
			out.close();
			
		}
		catch (FileNotFoundException exc){
			System.out.println ("Arquivo nao encontrado");
		}
		catch ( IOException e ) {									// Captura exceção
			e.printStackTrace();									// Imprime lista de erros
		}
		
		
	} 
}