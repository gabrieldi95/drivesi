class Aresta {
	double peso;
	Vertice	v_origem;
	Vertice v_destino;
	
	Aresta (Vertice inicio, Vertice fim){
		v_origem=inicio;
		v_destino=fim;
		peso=calculaPeso(v_origem,v_destino);
	}
	double calculaPeso(Vertice a, Vertice b){
		return Math.sqrt((b.x-a.x)*(b.x-a.x)+(b.y-a.y)*(b.y-a.y));
	}
	
}