import java.util.*;


class Dijkstra 
{
	int [] antecessores;
	//	double [] customin;
	List <Integer> caminho = new LinkedList <Integer>();
	void metodo_dijkstra (Grafo g , int origem, int destino)
	{
		
		Iterator it = g.vertices.iterator();
		Vertice v=new Vertice (0,0,0);
		while( it.hasNext() )
		{
			v.visitado = false;
			if ( v.indice == origem )
			v.peso = 0;
			else {
				v.peso = Double.MAX_VALUE;
				v.antecessor = -1;
			}
		}
		Collections.sort( g.vertices ); // Ordena os vertices - PODE ESTAR ERRADO
		Vertice u = Collections.min( g.vertices );
		it = g.vertices.iterator ();
		while (it.hasNext()){ // Percorre todos os vertices
			it = g.vertices.iterator ();
			v = (Vertice)it.next();
			if(v.visitado){
				v=(Vertice)it.next();
			}
			else {
				Iterator adj = v.lista_adj.iterator();
				while (adj.hasNext()) { // Percorre a Lista de Adjacente de v
					adj.next();
				
					if (v.peso > u.peso + g.buscaAresta (g, v.indice, u.indice)) {
						v.peso = u.peso + g.buscaAresta (g, v.indice, u.indice);
						v.antecessor = u.indice;
					}
				}
			v.visitado=true;
			Collections.sort( g.vertices ); // Ordena os vertices - PODE ESTAR ERRADO
			}
		}

		//		List <Integer> caminho = new List <Integer>();
		caminho.add( destino );
		Vertice x = buscaVertice( g, destino );
		while ( x.antecessor != -1 ) {
			caminho.add( x.antecessor );
			x = buscaVertice( g, x.antecessor );
		}
	}
	//Collections.reverse( caminho );
	Vertice buscaVertice( Grafo g, int indice )
	{
		Iterator it = g.vertices.iterator(); // REVISAR
		Vertice aux;
		while ( it.hasNext() )
		{
			aux = (Vertice)it.next();
			if ( aux.indice == indice )
				return aux;
		}
		return null;
	} // fim metodo_dijkstra
}