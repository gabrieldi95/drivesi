import java.util.ListIterator;
import java.util.List;
import java.util.LinkedList;

class Grafo {
	List <Vertice> vertices;
	int tamanho;
	Grafo ( int n ){
		tamanho=n;
		vertices = new LinkedList<Vertice>();
	}
	int tamanho_grafo (){
		return tamanho;
	}
	
	// Retorna o Peso
	double buscaAresta ( Grafo g, int a, int b){
		ListIterator it = g.vertices.listIterator();
		Aresta aresta;
		while (it.hasNext()){
			aresta=(Aresta)it.next();
			if (aresta.v_destino.indice==b){
				return aresta.peso;
			}
		}
		return -1.0;
	}
	void inserirAresta( int origem, int destino ){
		Vertice v1 = new Vertice (origem, 0, 0);
		Vertice v2 = new Vertice (destino, 0, 0);
		Aresta aresta = new Aresta( v1, v2 );
		// Procurar vertice de origem na lista g ( grafo )
		ListIterator it = vertices.listIterator();
		Vertice v;
		while ( it.hasNext() ) {
			v = (Vertice)it.next();
			if ( v.indice == origem )
				v.lista_adj.add(aresta);
		}
		it = vertices.listIterator();
		while ( it.hasNext() ) {
			v = (Vertice)it.next();
			if ( v.indice == destino )
				v.lista_adj.add(aresta);
		}
		// Acessar a lista de adjacência
		// Criar nova aresta na lista
		// Procurar o vértice de destino na lista g
		// Acessar a lista de adjacência
		// Inserir aresta de retorno

	}
	// Criar lista vertice c coordenadas e colocar na lista de vertice
	// Criar arestas e colocar na lista de adj
}