import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.FileWriter;
import java.util.BufferedWriter;


class Principal {
	static int contador = 0;	// variavel global usada para atribuição de valores no metodo coordenada
	public static void main(String[] args) {
		
		int numVertices = 0;		// guarda o número de vértices
		int numArestas = 0;			// guarda o número de arestas
		String chave;				// variável usada para armazenar temporariamente cada linha da entrada
		FileReader f;				//
		BufferedReader arquivo;		//
		double [][] m_ = new double [numVertices][numVertices];	//
		int [][] m_coordenadas = new int [numVertices][3];		//
		for ( int a = 0; a < numVertices; a++ ){				//
			for ( int b = 0;b < numVertices; b++ ){				//
				m_[a][b] = 0;									// atribui zero a todos os elemetos
			}
		}
		try {
			f = new FileReader( "input.txt" );					// lê o arquivo
			arquivo = new BufferedReader( f );
			chave = arquivo.readLine();							// lê a primeira linha da entrada
			String[] array = chave.split(" ");					// armazena os valores separados em um array
			numVertices = Integer.parseInt( array[0] );			// guarda o primeiro valor na variável de vertices
			Grafo g = new Grafo( numVertices );					// Cria grafo de tamanho numVertices
			numArestas = Integer.parseInt( array[1] );			// guarda o segundo valor na variável de arestas
			m_ = new double [numVertices][numVertices];			// instancia uma matriz (de adjacência?)
			m_coordenadas = new int [numVertices][3];			//
			chave = arquivo.readLine();							// "Pula" 2 linhas
			chave = arquivo.readLine();
			
			for ( int i = 0; i < numVertices; i++) {
				String[] coordenadas = chave.split( " " );
				// cria um vértice com os valores passados
				Vertice v = new Vertice( Integer.parseInt( coordenadas[0] ), Integer.parseInt( coordenadas[1] ), Integer.parseInt( coordenadas[2] ) );
				g.vertices.add( v ); 					// adiciona o vértice

				chave = arquivo.readLine();

			}
			for ( int i = 0; i < numArestas; i++) {					//
				String[] arestas = chave.split( " " );				//
				chave = arquivo.readLine();	
				insereAresta( Integer.parseInt( arestas[0] ), Integer.parseInt( arestas[1] ) );						
			}
			chave = arquivo.readLine(); 							//
			String[] origem_destino = chave.split( " " );			//
			int origem = Integer.parseInt( origem_destino[0] );		//
			int destino = Integer.parseInt( origem_destino[1] );	//
		}
		catch ( IOException e ) {									// Captura exceção
			e.printStackTrace();									// Imprime lista de erros
		}
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("saida.txt"));
			
			for (int a=0;a<caminhos;a++){
				out.write("aString\n"); // Imprimir vertices
			}
			// Escrever linha em branco (Enter)
			out.write("\n"); // Imprimir custo
			out.close();
		} 
		catch (IOException e) {
		}
	} 
}