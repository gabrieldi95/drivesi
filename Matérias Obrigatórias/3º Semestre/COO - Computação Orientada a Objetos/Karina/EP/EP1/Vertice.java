import java.util.LinkedList;
import java.util.List;

class Vertice implements Comparable<Vertice> {
	int indice;
	int x;
	int y;
	double peso;
	boolean visitado;
	int antecessor;
	List <Aresta> lista_adj = new LinkedList<Aresta>();	//

	
	Vertice(int valor, int a, int b) {					//	
		indice = valor;									//
		x = a;											//
		y = b;											//
	}
	int getX (){
		return x;
	}
	int getY (){										//
		return y;										//
	}
	int getIndice (){									//
		return indice;									//
	}
	public int compareTo( Vertice v ) {					//
        if (this.peso < v.peso) {						//
            return -1;									//
        }
        if (this.peso > v.peso) {						//
            return 1;									//
        }
        return 0;										//
    }
}