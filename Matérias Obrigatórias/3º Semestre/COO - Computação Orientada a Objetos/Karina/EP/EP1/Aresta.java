class Aresta {
	double peso;
	int v_origem;
	int v_destino;
	
	Aresta (int inicio, int fim){
		v_origem=inicio;
		v_destino=fim;
	}
	double calculaPeso(Vertice a, Vertice b){
//		double peso;
//		peso = sqrt((b.x-a.x)*(b.x-a.x)+(b.y-a.y)*(b.y-a.y));
//		return peso;
		return Math.sqrt((b.x-a.x)*(b.x-a.x)+(b.y-a.y)*(b.y-a.y));
	}
	Aresta buscaAresta (int a, int b){
		ListIterator it = g[a].listIterator();
		Aresta aresta;
		while (it.hasNext()){
			aresta=(Aresta)it.next();
			if (aresta.v_destino==b){
				return aresta;
			}
			return NULL;
		}
	}
}